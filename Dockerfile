FROM r-base:4.4.1
## Installing lagun dependencies (java jdk )
RUN apt-get -y update
RUN apt-get -y install libcurl4-openssl-dev libv8-dev libnode-dev libgsl-dev default-jre
## Installing cmake for R packages
RUN apt install -y cmake 
## Getting sources
COPY lagun /lagun/lagun
## Setting custom renv cache path
RUN mkdir -p /lagun/renv/cache
ENV RENV_PATHS_CACHE="/lagun/renv/cache"
## Setting working directory
WORKDIR /lagun/lagun
## Starting Lagun
RUN R -e "install.packages('renv')"
RUN R -e "options(timeout = 600); options(renv.config.connect.timeout = 600); renv::load(); renv::restore()"
CMD R -e "renv::load(); shiny::runApp('app.R', port=6023, host = '0.0.0.0')"
EXPOSE 6023
