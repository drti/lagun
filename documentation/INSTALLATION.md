## Install & run

### Machine without internet access

- Requirements: R (4.4.1) available from [CRAN](https://cran.r-project.org/), git, ([Rtools44](https://cran.r-project.org/bin/windows/Rtools/) for windows)
- First install Lagun on a machine with internet access and the same OS (mac/linux/windows) as the isolated machine, following the initial [instructions](../README.md)
- Compress lagun folder: `tar -cvzhf lagun.tar.gz lagun`
- Download renv source `renv_1.0.0.tar.gz` from the [CRAN](https://cran.r-project.org/web/packages/renv/index.html)
- Transfer the files `renv_1.0.0.tar.gz` and `lagun.tar.gz` to the isolated machine
- Extract lagun files: `tar -xvf lagun.tar.gz`
- Go in the main directory of Lagun Shiny app: `cd lagun/lagun`
- To execute the following R commands, make sure that R is added to the PATH variable or use R console.
- Install renv from the imported source: `R -e "install.packages('../../renv_1.0.0.tar.gz', repos = NULL, type = 'source')"`
- Install all the required packages from imported sources: `R -e "renv::load(); renv::restore()"`
- Start Lagun app with `R -e "renv::load(); shiny::runApp('app.R', port=6023)"`
- Open your browser at returned local adress: http://127.0.0.1:6023
