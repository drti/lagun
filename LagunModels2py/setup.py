from setuptools import setup

setup(
    name="lagunmodels2py", 
    version="0.1.0", 
    packages=["lagunmodels2py"],
    python_requires = ">=3.6",
    required_python_version = (3, 6)
)