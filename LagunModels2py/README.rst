LagunModels2py
##############

LagunModels2py has been developped to easily build and use surrogate models 
extracted from the software `Lagun <https://gitlab.com/drti/lagun>`_ (developped in R), 
in a Python environment, in order to compute predictions on new data.

No optimization is done in LagunModels2py, models are built using the hyperparameters 
optimized by Lagun, and no installation of R is needed.


Requirements
************

Python >= 3.6


Scikit-Learn >= 0.24


Installation
************

LagunModels2py can be installed locally via `pip <https://pypi.org/project/pip/>`_

.. code-block:: console

    $ pip install path/to/LagunModels2py


Try it out - tiny tutorial
**************************

Once the installation is complete, you can try it out with the 
`provided JSON file <json_example/surrogate_export_test-case-2.json>`_

Import JSON file
================

The following code imports the JSON file and builds the surrogate models

.. code-block:: python

    from lagunmodels2py import ExportedSurrogateModels
    j = "path/to/LagunModels2py/json_example/surrogate_export_test-case-2.json"
    esm = ExportedSurrogateModels(j)

The object ``esm`` instanciated previously roughly contains the information available in the JSON file.

Compute predictions
===================

For each target, the built surrogate model and the Q2 score are stored in the attribute ``models``.

For example, you can check the Q2 score of the target *Deviation* with the following piece of code

.. code-block:: python

    esm.models["Deviation"]["Q2"]
    # 0.994

To compute predictions, use the function ``predict(data, targets_to_predict=None)`` 
where *data* is an array containing new data, and *targets_to_predict* the targets for which the predictions will be computed.
It can be represented by a string for a single target, a list of strings for one or multiple targets. If not given, it will
compute the prediction for each target. 

.. code-block:: python

    import numpy as np

    X = np.array([[600, 14400000, 259.3, 398]])

    esm.predict(X)
    # {'Deviation': array([0.6084])}

    esm.predict(X, "Deviation")
    # {'Deviation': array([0.6084])}

    esm.predict(X, ["Deviation"])
    # {'Deviation': array([0.6084])}