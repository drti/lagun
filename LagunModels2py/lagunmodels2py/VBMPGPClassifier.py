from sklearn.gaussian_process import GaussianProcessRegressor
from scipy.stats import norm
import numpy as np


class VBMPGPClassifier(object):

    def __init__(self, kernel, classes):
        self.gp = GaussianProcessRegressor(
            kernel=kernel, optimizer=None, alpha=0)
        self.classes = classes

    def fit(self, X, y):
        self.gp.fit(X, y)
        return(self)

    def predict_proba(self, X, n_samps=1000):
        y_mean, y_std = self.gp.predict(X, return_std=True)
        y_std2 = y_std ** 2
        n_classes = len(self.classes)
        n_obs = len(X)
        p_test = np.ones((n_obs, n_classes))
        p_var = np.ones((n_obs, n_classes))

        if(n_classes == 2):
            p = y_mean[:, 1] / (np.sqrt(1 + y_std2))
            p = VBMPGPClassifier.safe_norm_cdf(p)
            p_test = np.array([p, 1-p]).T
            p_var = np.tile(np.prod(p_test, axis=1), (2, 1)).T
            # for i in range(n_obs):
            #     v = y_mean[i, 1] / (np.sqrt(1 + y_std2[i]))
            #     p_test[i, 0] = VBMPGPClassifier.safe_norm_cdf(v)
            #     p_test[i, 1] = 1 - p_test[i, 0]
            #     p_var[i, 0] = p_var[i, 1] = p_test[i, 0] * p_test[i, 1]

        elif(n_classes > 2):
            u = norm.rvs(size=n_samps)
            for i in range(n_obs):
                for j in range(n_classes):
                    pp = np.ones(n_samps)
                    for k in range(n_classes):
                        if k == j:
                            continue
                        else:
                            pp = pp * VBMPGPClassifier.safe_norm_cdf(
                                u + (y_mean[i, j] - y_mean[i, k])/(np.sqrt(1.+y_std2[i])))
                    p_test[i, j] = np.mean(pp)
                    p_var[i, j] = np.var(pp)

        return p_test, p_var

    def predict(self, X):
        y_proba, y_var = self.predict_proba(X)
        max_probas_idx = np.argmax(y_proba, axis=1)
        return([self.classes[i] for i in max_probas_idx])

    @staticmethod
    def safe_norm_cdf(x):
        return(norm.cdf(np.where(x < -10, -10, x)))
