from sklearn.linear_model import LinearRegression
import numpy as np
from numbers import Number


class FittedLinearRegression(LinearRegression):

    def __init__(self, coef_=None, intercept_=None):

        if not isinstance(intercept_, Number):
            raise TypeError("intercept must be a number")
        elif not isinstance(coef_, list):
            raise TypeError("coef must be a list")
        elif not all(isinstance(x, Number) for x in coef_):
            raise TypeError("elements in coef must be numbers")

        self.coef_ = np.array(coef_)
        self.intercept_ = np.array(intercept_)

    def predict(self, X):
        # modify X if categorical data ? (dummy variables)
        return super().predict(X)

    def fit(self, X, y):
        raise NotImplementedError("fit not available, prediction only")
