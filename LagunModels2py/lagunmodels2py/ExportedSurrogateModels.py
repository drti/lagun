import json
import numpy as np
from .FittedLinearRegression import FittedLinearRegression
from .FittedLogisticRegression import FittedLogisticRegression
from .VBMPGPClassifier import VBMPGPClassifier
from sklearn.gaussian_process.kernels import Matern, RBF, WhiteKernel
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.preprocessing import OneHotEncoder


class ExportedSurrogateModels(object):

    def __init__(self, path_to_json):
        self.path = path_to_json

        json_data = self._read_json()

        self.features = {item["name"]: item["type"]
                         for item in json_data["data"]["features"]}
        self.targets = {item["name"]: item["type"]
                        for item in json_data["data"]["targets"]}
        self.features_data = np.array(json_data["data"]["features_data"])
        self.targets_data = np.array(json_data["data"]["targets_data"])
        self.features_encoders = None
        self.models = None

        self._fit_encoders()
        self._build_models(json_data["models"])

    def _read_json(self):
        with open(self.path, "r") as json_file:
            json_data = json.load(json_file)

        # TODO validate json file

        return(json_data)

    def _fit_encoders(self):
        enc_dict = dict()
        cat_features = self._get_categorical_features_index()

        for f_name_idx in cat_features:
            f_name = list(self.features)[f_name_idx]
            encoder = OneHotEncoder(drop="first", dtype="int")
            category = self.features_data[:, f_name_idx]
            category = category.reshape(-1, 1).astype("object")
            encoder.fit(category)
            enc_dict[f_name] = encoder

        self.features_encoders = enc_dict if enc_dict else None

    def _get_feature_index(self, feature_name):
        index = list(self.features.keys()).index(feature_name)
        return(index)

    def _get_target_index(self, target_name):
        index = list(self.targets.keys()).index(target_name)
        return(index)

    def _get_numeric_features_index(self):
        types_list = list(self.features.values())
        return([i for i, x in enumerate(types_list) if x == "numeric"])

    def _get_categorical_features_index(self):
        types_list = list(self.features.values())
        return([i for i, x in enumerate(types_list) if x == "categorical"])

    def _build_models(self, models_info):
        models = dict()

        for m in models_info:
            params = m["parameters"]
            q2 = m["Q2_LOO"]

            if m["type"] == "Lasso":
                models[m["target_name"]] = self._build_lasso_model(
                    m["target_name"], params, q2)
            elif m["type"] == "Kriging":
                models[m["target_name"]] = self._build_kriging_model(
                    params, m["target_name"], q2)
            else:
                models[m["target_name"]] = None

        self.models = models

    def _build_lasso_model(self, target, params, q2):
        target_type = self.targets[target]
        coefficients = params["coefficients"]
        intercept = params["intercept"]

        if target_type == "numeric":
            mdl = {"model": FittedLinearRegression(
                coefficients, intercept), "Q2": q2}
        
        elif target_type == "categorical":
            classes = params["classes"]
            mdl = {"model": FittedLogisticRegression(
                coefficients, intercept, classes), 
                "Q2": q2}

        return(mdl)

    def _build_kriging_model(self, params, target, q2):
        target_type = self.targets[target]
        target_idx = self._get_target_index(target)
        x = self._preprocess_kriging(self.features_data)

        if target_type == "numeric":
            y = self.targets_data[:, target_idx].astype("float64")
            alpha = params["nugget"] * params["sigma2_hat"]
            kernel = ExportedSurrogateModels._select_kriging_kernel(params)

            gp = GaussianProcessRegressor(kernel=kernel,
                                          optimizer=None,
                                          alpha=alpha)

            mdl = {"model": gp.fit(x, y), "Q2": q2}
        elif target_type == "categorical":
            y = params["y_tild"]
            theta = params["theta"]
            classes = params["classes"]
            sigma2 = 1
            kernel = sigma2 * \
                RBF(length_scale=theta, length_scale_bounds="fixed") + \
                WhiteKernel(noise_level=1)

            gp = VBMPGPClassifier(kernel=kernel, classes=classes)

            mdl = {"model": gp.fit(x, y), "Q2": q2}

        return(mdl)

    @staticmethod
    def _select_kriging_kernel(params):

        kernel_type = params["kernel"]
        range_parameters = params["range_parameters"]
        theta_hat = params["theta_hat"]
        sigma2_hat = params["sigma2_hat"]

        if kernel_type == "matern_3_2":
            kernel = theta_hat + sigma2_hat * \
                Matern(length_scale=range_parameters,
                       length_scale_bounds="fixed",
                       nu=1.5)

        elif kernel_type == "matern_5_2":
            kernel = theta_hat + sigma2_hat * \
                Matern(length_scale=range_parameters,
                       length_scale_bounds="fixed",
                       nu=2.5)

        elif kernel_type == "pow_exp":
            kernel = theta_hat + sigma2_hat * \
                RBF(length_scale=range_parameters,
                    length_scale_bounds="fixed")

        else:
            raise ValueError(kernel_type + " kernel is not supported")

        return(kernel)

    def _preprocess_kriging(self, data):
        # Select only numeric features
        cat_idx = self._get_categorical_features_index()
        data = np.delete(data, cat_idx, axis=1)
        return(data.astype("float"))

    def _preprocess_lasso(self, data):
        # One Hot Encoding for categorical features, replace feature by encoded features
        # at the same index to match coefficients
        idx_shift = 0
        cat_features = self._get_categorical_features_index()

        for f_idx in cat_features:
            f_name = list(self.features)[f_idx]
            f_name_idx = f_idx + idx_shift
            category = data[:, f_name_idx]
            category = category.reshape(-1, 1).astype("object")
            encoded = self.features_encoders[f_name].transform(category)
            data = np.delete(data, f_name_idx, axis=1)
            data = np.insert(data, 
                            [f_name_idx] * encoded.shape[1], 
                            encoded.toarray(), 
                            axis=1)
            idx_shift += encoded.shape[1] - 1

        return(data.astype("float"))

    def predict(self, data, targets_to_predict=None):

        if(not targets_to_predict):
            targets_to_predict = list(self.targets.keys())

        if(not isinstance(targets_to_predict, list)):
            targets_to_predict = [targets_to_predict]

        predictions = dict()
        for t in targets_to_predict:
            t_model = self.models[t]["model"]

            if(isinstance(t_model, FittedLinearRegression) or
               isinstance(t_model, FittedLogisticRegression)):
                preprocessed_data = self._preprocess_lasso(data)
            elif(isinstance(t_model, GaussianProcessRegressor) or
                 isinstance(t_model, VBMPGPClassifier)):
                preprocessed_data = self._preprocess_kriging(data)
            else:
                raise ValueError(str(t_model)+" not supported")

            predictions[t] = t_model.predict(preprocessed_data)

        return(predictions)
