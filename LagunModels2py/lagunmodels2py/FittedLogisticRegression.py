import numpy as np


class FittedLogisticRegression(object):

    def __init__(self, coef=None, intercept=None, classes=None):
        self.coefficients = np.array(coef)
        self.intercept = np.array(intercept)
        self.classes = classes

    def predict_proba(self, X):
        probas = None
        n_classes = self._get_n_classes()
        i = self.intercept
        c = self.coefficients

        if n_classes == 2:
            p = 1/(1 + np.exp(-(i + np.sum(c * X, axis=1))))
            probas = np.column_stack((p, 1 - p))

        elif n_classes > 2:
            e_beta_x = np.column_stack(
                [np.exp(i[x] + np.sum(c[x] * X, axis=1)) for x in range(len(i))])
            deno = np.matrix(1 + np.sum(e_beta_x, axis=1))
            p = e_beta_x / np.tile(deno.T, (1, len(i)))
            probas = np.column_stack((p, 1 - np.sum(p, axis=1)))

        return(np.array(probas))

    def predict(self, X):
        y_proba = self.predict_proba(X)
        max_probas_idx = np.argmax(y_proba, axis=1)
        return(np.array([self.classes[i] for i in max_probas_idx]))

    def _get_n_classes(self):
        return(len(self.classes))
