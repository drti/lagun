#!/bin/bash


nparam=$((`head -1 paramFile_$1.txt | grep -c ';'`+1))
for n in seq 1 $nparam;
do
  value=`head -1 paramFile_$1.txt | cut -d';' -f$n`
  sed -i 's/__X'$n'__/'$value'/g' $1
done


if [ ! ${1: -4} == ".mos" ]; then
  model=`grep "model" $1 | awk '{print $2}'`
  cat > $1.mos <<- EOM
loadModel(Modelica);
loadFile("$1");
simulate($model, stopTime=1,tolerance=0.001,outputFormat="csv");
EOM
  omc $1.mos
else
  omc $1
fi

echo "\"T\"" > outputFile.csv
tail -1 NewtonCooling_res.csv | cut -d, -f2 >> outputFile.csv


