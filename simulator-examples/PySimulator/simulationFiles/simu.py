
import sys
import csv
import math

def analytical_fct(doe):
    A = 7
    B = 0.1
    y = []
    for x in doe:
        y += [[math.sin(x[0]) + A * (math.sin(x[1])**2) + B * (x[2]**4) * math.sin(x[0])]]
    return y

doe = []
with open(sys.argv[1], newline='') as csvfile:
     spamreader = csv.reader(csvfile, delimiter=';', quotechar='|')
     for row in spamreader:
         doe += [[float(x) for x in row]]

output = analytical_fct(doe)
print(output)

with open('outputFile.csv', mode='w', newline = '') as outfile:
    out_writer = csv.writer(outfile, delimiter=';')
    for x in output:
        out_writer.writerow(x)

