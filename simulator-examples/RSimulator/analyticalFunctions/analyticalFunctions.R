testcase.alias.UQ.name <- c("Diving board","Flood model")
testcase.alias.UQ <- c("dive","flood")
testcase.alias.GSA.name <- c("Ishigami function (Slow)","Ishigami function","G-Sobol function","Flood model")
testcase.alias.GSA <- c("ishigami.slow","ishigami","gsobol","flood")
testcase.alias.CO.name <- c("Welded beam constrained optim","G5mod function","G7 function (Slow)","G7 function","G9 function","G10 function","G18 function")
testcase.alias.CO <- c("wbeam","g5mod","g7.slow","g7","g9","g10","g18")
testcase.alias.MO.name <- c("Beam bi-objective optim","Flood model")
testcase.alias.MO <- c("beam_2obj","flood")

testcase.alias.name <- c(testcase.alias.UQ.name,testcase.alias.GSA.name,testcase.alias.CO.name,testcase.alias.MO.name)
testcase.alias <- c(testcase.alias.UQ,testcase.alias.GSA,testcase.alias.CO,testcase.alias.MO)

testcase.info <- function(testcase.alias){
  info <- list()
  
  # Ishigami (Slow)
  if (testcase.alias=="ishigami.slow"){
    info$alias <- "ishigami.slow"
    info$name <- "Ishigami (Slow)"
    info$description <- "Slow version of Ishigami for tests."
    info$class <- "GSA"
    info$dX <- 3
    info$dY <- 1
    info$lb <- rep(-pi,3)
    info$ub <- rep(pi,3)
    info$fun <- function(x){
      if (is.null(dim(x))) x <- matrix(x,1)
      A <- 7
      B <- 0.1
      y <- sin(x[, 1]) + A * sin(x[, 2])^2 + B * x[, 3]^4 * sin(x[,1])
      Sys.sleep(5)
      return(y)
    }
  }
  
  # Ishigami
  if (testcase.alias=="ishigami"){
    info$alias <- "ishigami"
    info$name <- "Ishigami"
    info$description <- "3 inputs and 1 output. A classical function for sensitivity analysis."
    info$class <- "GSA"
    info$dX <- 3
    info$dY <- 1
    info$lb <- rep(-pi,3)
    info$ub <- rep(pi,3)
    info$fun <- function(x){
      if (is.null(dim(x))) x <- matrix(x,1)
      A <- 7
      B <- 0.1
      y <- sin(x[, 1]) + A * sin(x[, 2])^2 + B * x[, 3]^4 * sin(x[,1])
      return(y)
    }
  }
  
  # G-Sobol
  if (testcase.alias=="gsobol"){
    info$alias <- "gsobol"
    info$name <- "G-Sobol"
    info$description <- "8 inputs and 1 output. A classical function for sensitivity analysis."
    info$class <- "GSA"
    info$dX <- 8
    info$dY <- 1
    info$lb <- rep(0,8)
    info$ub <- rep(1,8)
    info$fun <- function(x){
      if (is.null(dim(x))) x <- matrix(x,1)
      a <- c(0, 1, 4.5, 9, 99, 99, 99, 99)
      y <- 1
      for (j in 1:8) {
        y <- y * (abs(4 * x[, j] - 2) + a[j])/(1 + a[j])
      }
      return(y)
    }
  }
  
  # Beam bi-objective optim
  if (testcase.alias=="beam_2obj"){
    info$alias <- "beam_2obj"
    info$name <- "Beam for bi-objective optimization"
    info$description <- "2 inputs and 2 outputs. A simple example of bi-objective optimization."
    info$class <- "MO"
    info$dX <- 2
    info$dY <- 2
    info$lb <- c(30,200)
    info$ub <- c(50,500)
    info$fun <- function(x){
      if (is.null(dim(x))) x <- matrix(x,1)
      P = 1000; E = 210000; rho = 7850
      d <- x[,1]; L <- x[,2]
      delta <- 64 * P * L^3/(3*pi*E*d^4)
      mass <- 0.25*pi*rho*d^2*L*1e-9
      return(cbind(delta,mass))
    }
  }
  
  # Flood model
  if (testcase.alias=="flood"){
    info$alias <- "flood"
    info$name <- "Flood model"
    info$description <- "8 inputs and 2 outputs. A classical function for sensitivity analysis."
    info$class <- "GSA"
    info$dX <- 8
    info$dY <- 2
    info$lb <- c(0,10,49,54,5,55,4092,295)
    info$ub <- c(20,50,51,56,7,56,5003,305)
    info$fun <- function(x){
      if (is.null(dim(x))) x <- matrix(x,1)
      n <- nrow(x)
      H = (x[,1] / (x[,2]*x[,8]*sqrt((x[,4] - x[,3])/x[,7])))^(0.6)
      S = x[,3] + H - x[,5] - x[,6]
      Cp = matrix(NA,n,1)
      idSneg <- (S<=0)
      Cp[idSneg,1] <- 0.2 + 0.8 * (1 - exp(-1000 / S^4))
      Cp[!idSneg,1] <- 1
      idHdless8 <- (x[,5]<=8)
      Cp[idHdless8,1] <- Cp[idHdless8,1] + 8/20
      Cp[!idHdless8,1] <- Cp[!idHdless8,1] + x[,5]/20
      return(cbind(S,Cp))
    }
  }
  
  # Welded beam constrained optim
  if (testcase.alias=="wbeam"){
    info$alias <- "wbeam"
    info$name <- "Welded beam for constrained optimization"
    info$description <- "4 inputs and 5 outputs. A simple example of constrained optimization."
    info$class <- "CO"
    info$dX <- 4
    info$dY <- 5
    info$lb <- c(0.125,0.1,0.1,5)
    info$ub <- c(5,10,10,10)
    info$fun <- function(x){
      if (is.null(dim(x))) x <- matrix(x,1)
      P = 6000; L=14; E=30*10^6; G=12*10^6
      M = P*(L+ x[,2]/2)
      R = sqrt(0.25*(x[,2]^2 +(x[,1] +x[,3])^2))
      J = sqrt(2)*x[,1]*x[,2]*(x[,2]^2/12 +0.25*(x[,1] +x[,3])^2)
      Pc = 4.013*E/(6*L^2) *x[,3]*x[,4]^3*(1 - 0.25*x[,3]*sqrt(E/G)/L)
      t1 = P/(sqrt(2)*x[,1]*x[,2])
      t2 = M*R/J
      tt = sqrt(t1^2+t1*t2*x[,2]/R+t2^2)
      s = 6*P*L/(x[,4]*x[,3]^2)
      d = 4*P*L^3/(E*x[,4]*x[,3]^3)
      f <- 1.10471*x[,1]^2*x[,2] + 0.04811*x[,3]*x[,4]*(14.0 + x[,2])
      g1 <- tt
      g2 <- s
      g5 <- d
      g6 <- Pc
      return(cbind(f,log(g1),log(g2),log(g5),g6))
    }
  }
  
  # G5mod function
  if (testcase.alias=="g5mod"){
    info$alias <- "g5mod"
    info$name <- "G5mod function for constrained optimization"
    info$description <- "4 inputs and 6 outputs. A classical example of constrained optimization."
    info$class <- "CO"
    info$dX <- 4
    info$dY <- 6
    info$lb <- c(rep(0,2),rep(-0.55,2))
    info$ub <- c(rep(1200,2),rep(0.55,2))
    info$fun <- function(x){
      if (is.null(dim(x))) x <- matrix(x,1)
      f <- 3*x[,1] + 0.000001 * x[,1]^3 + 2*x[,2] + 0.000002/3 * x[,2]^3
      g1 <- -x[,4]+x[,3]-0.55
      g2 <- -x[,3]+x[,4]-0.55
      g3 <- 1000 * sin(-x[,3] - 0.25) + 1000 * sin(-x[,4] - 0.25) + 894.8 - x[,1]
      g4 <- 1000 * sin(x[,3]-0.25)+1000*sin(x[,3]-x[,4]-0.25)+894.8-x[,2]
      g5 <- 1000 * sin(x[,4] - 0.25) + 1000 * sin(x[,4] - x[,3] - 0.25) + 1294.8
      return(cbind(f,g1,g2,g3,g4,g5))
    }
  }
  
  # G7 function
  if (testcase.alias=="g7"){
    info$alias <- "g7"
    info$name <- "G7 function for constrained optimization"
    info$description <- "10 inputs and 9 outputs. A classical example of constrained optimization."
    info$class <- "CO"
    info$dX <- 10
    info$dY <- 9
    info$lb <- rep(-10,10)
    info$ub <- rep(10,10)
    info$fun <- function(x){
      if (is.null(dim(x))) x <- matrix(x,1)
      f <- x[,1]^2 +x[,2]^2 +x[,1]*x[,2] -14*x[,1] -16*x[,2] +(x[,3] -10)^2 +4*(x[,4] -5)^2 +(x[,5] -3)^2 + 2*(x[,6] -1)^2 + 5*x[,7]^2 +7*(x[,8] -11)^2 +2*(x[,9] -10)^2 +(x[,10] -7)^2 +45
      g1 <- (-105+4*x[,1] +5*x[,2] -3*x[,7] +9*x[,8])/105
      g2 <- (10*x[,1] -8*x[,2] -17*x[,7] + 2*x[,8])/370
      g3 <- (-8*x[,1] +2*x[,2] +5*x[,9] -2*x[,10] -12)/158
      g4 <- (3*(x[,1] -2)^2 +4*(x[,2] -3)^2 + 2*x[,3]^2 - 7*x[,4] -120)/1258
      g5 <- (5*x[,1]^2 + 8*x[,2] +(x[,3] -6)^2 -2*x[,4] -40)/816
      g6 <- (x[,1]^2 +2*(x[,2] -2)^2 -2*x[,1]*x[,2] +14*x[,5] -6*x[,6])/788
      g7 <- (0.5*(x[,1] -8)^2 +2*(x[,2] -4)^2 +3*x[,5]^2 -x[,6] -30)/834
      g8 <- (-3*x[,1] +6*x[,2] +12*(x[,9]-8)^2 -7*x[,10])/4048
      return(cbind(f,g1,g2,g3,g4,g5,g6,g7,g8))
    }
  }
  
  # G7 function (Slow)
  if (testcase.alias=="g7.slow"){
    info$alias <- "g7.slow"
    info$name <- "G7 (Slow)"
    info$description <- "G7 for tests between 0 and 1."
    info$class <- "CO"
    info$dX <- 10
    info$dY <- 9
    info$lb <- rep(0,10)
    info$ub <- rep(1,10)
    info$fun <- function(x){
      if (is.null(dim(x))) x <- matrix(x,1)
      x <- -10+20*x
      f <- x[,1]^2 +x[,2]^2 +x[,1]*x[,2] -14*x[,1] -16*x[,2] +(x[,3] -10)^2 +4*(x[,4] -5)^2 +(x[,5] -3)^2 + 2*(x[,6] -1)^2 + 5*x[,7]^2 +7*(x[,8] -11)^2 +2*(x[,9] -10)^2 +(x[,10] -7)^2 +45
      g1 <- (-105+4*x[,1] +5*x[,2] -3*x[,7] +9*x[,8])/105
      g2 <- (10*x[,1] -8*x[,2] -17*x[,7] + 2*x[,8])/370
      g3 <- (-8*x[,1] +2*x[,2] +5*x[,9] -2*x[,10] -12)/158
      g4 <- (3*(x[,1] -2)^2 +4*(x[,2] -3)^2 + 2*x[,3]^2 - 7*x[,4] -120)/1258
      g5 <- (5*x[,1]^2 + 8*x[,2] +(x[,3] -6)^2 -2*x[,4] -40)/816
      g6 <- (x[,1]^2 +2*(x[,2] -2)^2 -2*x[,1]*x[,2] +14*x[,5] -6*x[,6])/788
      g7 <- (0.5*(x[,1] -8)^2 +2*(x[,2] -4)^2 +3*x[,5]^2 -x[,6] -30)/834
      g8 <- (-3*x[,1] +6*x[,2] +12*(x[,9]-8)^2 -7*x[,10])/4048
      return(cbind(f,g1,g2,g3,g4,g5,g6,g7,g8))
    }
  }
  
  # G9 function
  if (testcase.alias=="g9"){
    info$alias <- "g9"
    info$name <- "G9 function for constrained optimization"
    info$description <- "7 inputs and 5 outputs. A classical example of constrained optimization."
    info$class <- "CO"
    info$dX <- 7
    info$dY <- 5
    info$lb <- rep(-10,7)
    info$ub <- rep(10,7)
    info$fun <- function(x){
      if (is.null(dim(x))) x <- matrix(x,1)
      f <- (x[,1] - 10)^2 + 5*(x[,2] - 12)^2 + x[,3]^4 +3*(x[,4] - 11)^2 + 10*x[,5]^6 + 7*x[,6]^2 + x[,7]^4 - 4*x[,6]*x[,7] - 10*x[,6] - 8*x[,7]
      g1 <- (2*x[,1]^2 + 3*x[,2]^4 + x[,3] + 4*x[,4]^2 + 5*x[,5] - 127)/127
      g2 <- (7*x[,1] + 3*x[,2] + 10*x[,3]^2 + x[,4] - x[,5] - 282)/282
      g3 <- (23*x[,1] + x[,2]^2 + 6*x[,6]^2 - 8*x[,7] - 196)/196
      g4 <- 4*x[,1]^2 + x[,2]^2 - 3*x[,1]*x[,2] + 2*x[,3]^2 + 5*x[,6] - 11*x[,7]
      return(cbind(f,g1,g2,g3,g4))
    }
  }
  
  # G10 function
  if (testcase.alias=="g10"){
    info$alias <- "g10"
    info$name <- "G10 function for constrained optimization"
    info$description <- "8 inputs and 7 outputs. A classical example of constrained optimization."
    info$class <- "CO"
    info$dX <- 8
    info$dY <- 7
    info$lb <- c(100,1000,1000,rep(10,5))
    info$ub <- c(10000,10000,10000,rep(1000,5))
    info$fun <- function(x){
      if (is.null(dim(x))) x <- matrix(x,1)
      plog <- function(x){
        if (x >= 0){
          y <- log(1 + x)
        } else {
          y <- - log(1 - x)
        }
        return(y)
      }
      f <- x[,1] + x[,2] + x[,3]-1+0.0025*(x[,4]+x[,6])
      g1 <- -1+0.0025*(x[,4]+x[,6])
      g2 <- -1+0.0025*(-x[,4]+x[,5]+x[,7])
      g3 <- -1+0.01*(-x[,5]+x[,8])
      g4 <- apply(matrix(100*x[,1]-x[,1]*x[,6]+833.33252*x[,4]-83333.333), 1, plog)
      g5 <- apply(matrix(x[,2]*x[,4]-x[,2]*x[,7]-1250*x[,4]+1250*x[,5]), 1, plog)
      g6 <- apply(matrix(x[,3]*x[,5]-x[,3]*x[,8]-2500*x[,5]+1250000), 1, plog)
      return(cbind(f,g1,g2,g3,g4,g5,g6))
    }
  }
  
  # G18 function
  if (testcase.alias=="g18"){
    info$alias <- "g18"
    info$name <- "G18 function for constrained optimization"
    info$description <- "9 inputs and 14 outputs. A classical example of constrained optimization."
    info$class <- "CO"
    info$dX <- 9
    info$dY <- 14
    info$lb <- c(rep(-10,8),0)
    info$ub <- c(rep(10,8),20)
    info$fun <- function(x){
      if (is.null(dim(x))) x <- matrix(x,1)
      f <- -0.5*(x[,1]*x[,4] - x[,2]*x[,3] + x[,3]*x[,9] - x[,5]*x[,9] + x[,5]*x[,8] - x[,6]*x[,7])
      g1 <- x[,3]^2 + x[,4]^2 - 1
      g2 <- x[,9]^2 - 1
      g3 <- x[,5]^2 + x[,6]^2 - 1
      g4 <- x[,1]^2 + (x[,2] - x[,9])^2 - 1
      g5 <- (x[,1] - x[,5])^2 + (x[,2] - x[,6])^2 - 1
      g6 <- (x[,1] - x[,7])^2 + (x[,2] - x[,8])^2 - 1
      g7 <- (x[,3] - x[,5])^2 + (x[,4] - x[,6])^2 - 1
      g8 <- (x[,3] - x[,7])^2 + (x[,4] - x[,8])^2 - 1
      g9 <- x[,7]^2 + (x[,8] - x[,9])^2 - 1
      g10 <-  x[,2]*x[,3] - x[,1]*x[,4]
      g11 <-  -x[,3]*x[,9]
      g12 <-  x[,5]*x[,9]
      g13 <-  x[,6]*x[,7] - x[,5]*x[,8]
      return(cbind(f,g1,g2,g3,g4,g5,g6,g7,g8,g9,g10,g11,g12,g13))
    }
  }
  
  # Diving board UQ
  if (testcase.alias=="dive"){
    info$alias <- "dive"
    info$name <- "Diving board for uncertainty propagation"
    info$description <- "4 inputs and 1 output. A simple example of uncertainty quantification."
    info$class <- "UQ"
    info$dX <- 4
    info$dY <- 1
    info$lb <- c(0,10000000,250,300)
    info$ub <- c(60000,50000000,260,500)
    info$fun <- function(x){
      if (is.null(dim(x))) x <- matrix(x,1)
      result <- (x[,1]*x[,3]^3)/(3*x[,2]*x[,4])
      return(result)
    }
  }
  
  
  return(info)
}