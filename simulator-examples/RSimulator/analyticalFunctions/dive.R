analytical.fct <- function(x){
  if (is.null(dim(x))) x <- matrix(x,1)
  result <- (x[,1,drop=F]*x[,3,drop=F]^3)/(3*x[,2,drop=F]*x[,4,drop=F])
  return(result)
}