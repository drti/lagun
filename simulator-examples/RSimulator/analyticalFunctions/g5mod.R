analytical.fct <- function(x){
  if (is.null(dim(x))) x <- matrix(x,1)
  f <- 3*x[,1,drop=F] + 0.000001 * x[,1,drop=F]^3 + 2*x[,2,drop=F] + 0.000002/3 * x[,2,drop=F]^3
  g1 <- -x[,4,drop=F]+x[,3,drop=F]-0.55
  g2 <- -x[,3,drop=F]+x[,4,drop=F]-0.55
  g3 <- 1000 * sin(-x[,3,drop=F] - 0.25) + 1000 * sin(-x[,4,drop=F] - 0.25) + 894.8 - x[,1,drop=F]
  g4 <- 1000 * sin(x[,3,drop=F]-0.25)+1000*sin(x[,3,drop=F]-x[,4,drop=F]-0.25)+894.8-x[,2,drop=F]
  g5 <- 1000 * sin(x[,4,drop=F] - 0.25) + 1000 * sin(x[,4,drop=F] - x[,3,drop=F] - 0.25) + 1294.8
  y <- cbind(f, g1, g2, g3, g4, g5)
  colnames(y) <- c("f", "g1", "g2", "g3", "g4", "g5")
  return(y)
}