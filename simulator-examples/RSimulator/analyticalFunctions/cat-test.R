analytical.fct <- function(x){
  if (is.null(dim(x))) x <- matrix(x,1)

  f <- switch( x[,5],
		"one" = { 3*x[,1,drop=F] + 0.000001 * x[,1,drop=F]^3 + 2*x[,2,drop=F] + 0.000002/3 * x[,2,drop=F]^3 },
		"two" = { -x[,4,drop=F]+x[,3,drop=F]-0.55 },
		"three" = { -x[,3,drop=F]+x[,4,drop=F]-0.55 },
		"four" = { 1000 * sin(-x[,3,drop=F] - 0.25) + 1000 * sin(-x[,4,drop=F] - 0.25) + 894.8 - x[,1,drop=F] },
		"five" = {  1000 * sin(x[,3,drop=F]-0.25)+1000*sin(x[,3,drop=F]-x[,4,drop=F]-0.25)+894.8-x[,2,drop=F] },
		"six" = { 1000 * sin(x[,4,drop=F] - 0.25) + 1000 * sin(x[,4,drop=F] - x[,3,drop=F] - 0.25) + 1294.8 }
  )
  return(f)
}