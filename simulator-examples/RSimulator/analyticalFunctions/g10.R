analytical.fct <- function(x){
  if (is.null(dim(x))) x <- matrix(x,1)
  plog <- function(x){
    if (x >= 0){
      y <- log(1 + x)
    } else {
      y <- - log(1 - x)
    }
    return(y)
  }
  f <- x[,1] + x[,2] + x[,3]-1+0.0025*(x[,4]+x[,6])
  g1 <- -1+0.0025*(x[,4]+x[,6])
  g2 <- -1+0.0025*(-x[,4]+x[,5]+x[,7])
  g3 <- -1+0.01*(-x[,5]+x[,8])
  g4 <- apply(matrix(100*x[,1]-x[,1]*x[,6]+833.33252*x[,4]-83333.333), 1, plog)
  g5 <- apply(matrix(x[,2]*x[,4]-x[,2]*x[,7]-1250*x[,4]+1250*x[,5]), 1, plog)
  g6 <- apply(matrix(x[,3]*x[,5]-x[,3]*x[,8]-2500*x[,5]+1250000), 1, plog)
  return(cbind(f,g1,g2,g3,g4,g5,g6))
}