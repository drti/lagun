# HubOpt optimization library
# NOMAD : Derivative Free optimizer with constraints
 
nomad.run <- function(PbDefinition, ParametersValues, AdvancedParametersValues) {
  library(HubOpt4R) #IFPEN library
  source("optimizers/HubOpt/CreateDefaultXML.R")
  
  ParametersValues$solver = "NOMAD"
  CreateDefaultXML(ParametersValues,AdvancedParametersValues,HOpath=PbDefinition$savepath)
  
  nd <- PbDefinition$nd
  ncons <- PbDefinition$ncons
  
  # HubOpt4R package requires separated OF and constraints computations: 
  # call twice the simulationLauncher with the same x (simulationLauncher detects duplicated points -> only one simulation !)
  ObjFunc <- function(x) { nsim <- length(x)/nd ; x = matrix(x,nrow=nsim,ncol=nd,byrow=TRUE) ; 
                           Y <- PbDefinition$ObjFunc(x) ; return(matrix(t(Y[,1,drop=F]),nrow=nsim,ncol=1,byrow=F))}
  
  if (ncons > 0) ConsFunc <- function(x) { 
     nsim <- length(x)/nd ; x = matrix(x,nrow=nsim,ncol=nd,byrow=TRUE) ; 
     Y <- PbDefinition$ObjFunc(x, FALSE) 
     return(matrix(t(Y[ , 2:(1+ncons), drop=F]),nrow=nsim,ncol=ncons,byrow=F)) 
     } else ConsFunc = NULL
  
  indCat = NULL
  if (PbDefinition$tags$categorical) {
    #identify categorical/integer variables by their indices
    indint = which( PbDefinition$inputflag == "I")
    indCat = which( PbDefinition$inputflag == "Cat")
    ny = length(c(indint,indCat))
    ycategorical = 0 #no categorical adapted poll step for categorical (except when coupled with EGO)
    yindex = c(indint,indCat)-1
  }else{
    ny=0
	yindex=NULL
	ycategorical=0
  }
  
  print(paste0("nomad with ",ny," integer variables of index "))
  print(yindex)
  
  
  resHO <- HubOpt(list(ObjFunc), PbDefinition$x0, nd, PbDefinition$lb, PbDefinition$ub, nx0=1, nobj=1,
                  ny=ny, yindex=yindex, ycategorical=ycategorical,
                  HOPATH=PbDefinition$savepath)
  
  #resHO<- HubOpt(list(func), x0, nx, lb, ub, nx0=1, 
  #               XDOEfixed=NULL, nDOEfixed=0, 
  #               ny=0, yindex=NULL, ycategorical=0,
  #               nobj=1, A=NULL, nil=0, B=NULL, Aeq=NULL, nel=0, Beq=NULL, 
  #               NLCON="", ninl=0, nenl=0, 
  #               DFCON="", nid=0, ld=NULL, ud=NULL,  
  #               xn=NULL, XMLFILE='./HO.xml', HOPATH='./')
  
  Outputs <- list(xsol=resHO[[2]])
  return(Outputs)
}

nomad.description <- function(){

	DisplayName="HubOpt NOMAD"

	Description="NOMAD performs global derivative-free constrained optimization with a Mesh Adaptive Direct Search algorithm (MADS)."

	OptimTags=list(constraints=TRUE, categorical=TRUE, monoobj=TRUE, multiobj=FALSE, derivative=FALSE)

	Warnings=list()

	Parameters=list(
	dx_init=list(default=1,lower=0,upper=Inf,type="float",description="Initial mesh size."),
	dx_min=list(default=1.e-6,lower=0,upper=Inf,type="float",description="Minimal mesh size."),
	maxeval=list(default=100,lower=0,upper=Inf,type="integer",description="Stop when the number of function evaluations exceeds maxeval.")
	)

	AdvancedParameters=list(
	print_level=list(default=0,enum=c(0,10,20,30),type="integer",description="Display level: 0 mute, 1 display error, 10 display warnings, 20 display info, 30 display debug.")
	)

	return(list(dispname=DisplayName, descr=Description, tags=OptimTags, warn=Warnings, param=Parameters, advparam=AdvancedParameters))
}
  