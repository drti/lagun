#### Advanced Settings

You will find here an access to some parameters that are used internally in the platform
for some of the tasks. You are free to change them, however it is recommended that you are
familiar with the concepts. Indeed increasing one or some of these parameters can have a large impact
on the computation time of the tasks.

#### About

Information on the releases and versions
