In each tab, the primary tasks to be performed in the workflow will be indicated by blue buttons:

<button class="btn btn-primary">Primary Task</button>

Tasks that are not explictly in the workflow but are highly recommended will appear in red:

<button class="btn btn-danger">Highly Recommended Task</button>

Tasks that may bring relevant additional information are colored in lightblue:

<button class="btn btn-info">Additional Information</button>

Finally, information on what is loaded in memory and which surrogate model is active is available at all times in the 'Info' panel (found in the navigation bar).
