<h5> 
These tools are commonly used in the numerical uncertainty community (GDR Mascot-Num for example) but are also widely applicable to experimental problems. The main functionalities are the following:

1. Optimized design of experiments <br>
If you have control on the inputs/parameters on the system which will generate the dataset (numerical simulations, settings of the experiments, ...), you can benefit for a better spatial repartition of the experiments.

2. Visual exploration tools <br>
When the complete dataset with inputs/parameters and outputs/responses is available, you can load it to perform insightful visual analyses and identify the main trends and the most influential parameters.

3. Going further with surrogate models <br>
A next common step is to use the dataset to infer a predictive relationship between the inputs/parameters and the outputs. This estimated relationship, the surrogate model, can help push forward the analysis with its ability to predict the responses for any new combination of the inputs. In particular it can be extensively used for uncertainty quantification, sensitivity analysis, deterministic optimization, optimization under uncertainty (robust and reliability based) or more intensive graphical studies.

4. Numerical simulations <br>
In the special case of numerical simulations, you can benefit from a direct connection between LAGUN and your simulation scripts to perform automatic and sequential optimizations with the surrogate models.

<br>
<br>
For a general introduction, please see the following tutorial:
