<div class="jumbotron">
  <h1 align="center"> LAGUN </h1>
  <h4 align="center"> LAGUN is a R/Shiny platform providing a user-friendly interface to methods and algorithms dedicated to the exploration and the analysis of datasets.<br>
Guided workflows are provided to help non-expert users to apply safely the proposed methodologies. </h4>
</div>
