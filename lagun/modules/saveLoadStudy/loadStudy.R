#
# This file is subject to the terms and conditions defined in
# the file 'LICENSE', which is part of this source code.
#

source("modules/saveLoadStudy/saveStudy.R", local = TRUE)

SAVE_MODULE_MIN_VER <- 0.1

PROGRESS_STEPS <- c(
  "defineObjective-clean",
  "generateDOE-nX",
  "generateDOE-xInfos",
  "generateDOE-results",
  "importDOE",
  "uploadDOE",
  "confSimulator",
  "importExperimentalData-menuImport",
  "defineObjective-menuImport",
  "surrogate-listmodel",
  "buildsurrogate-surrogateMode",
  "uqParamsChange-uncertaintyDefinition",
  "uqParamsDependence-uncertaintyDefinition",
  "uncertaintyPropagation",
  "sensitivityAnalysis",
  "unconstrained-xinfos",
  "unconstrained-results",
  "constrainedDefine-constrained-initialXinfos",
  "constrainedDefine-constrained-COformulation",
  "constrainedSolve",
  "directOptim-simulator",
  "importExperimentalData-directoptim",
  "defineObjective-directOptim",
  "constrainedDefine-directOptim-initialXinfos",
  "constrainedDefine-directOptim-COformulation",
  "constrainedDefine-directOptim-x0",
  "directOptim-optimArgs",
  "directOptim-results"
)

progressToNextStep <- function(loadedStudy) {
  progressIndex <- which(loadedStudy$updatingStep == loadedStudy$updatingSteps)
  if (length(progressIndex) == 1) {
    if (progressIndex == length(loadedStudy$updatingSteps)) {
      loadedStudy$updatingStep <- "OFF"
    }
    else {
      loadedStudy$updatingStep <- loadedStudy$updatingSteps[progressIndex + 1]
    }
  }
  else if (length(progressIndex) == 0) {
    print(paste0(
      "Unknown updating step: '", loadedStudy$updatingStep, "'. Known: ",
      paste(loadedStudy$updatingSteps, collapse = ",")
    ))
  }
  else if (length(progressIndex) > 1) {
    print(paste0(
      "Several updating steps have the same name: '", loadedStudy$updatingStep, "' among: ",
      paste(loadedStudy$updatingSteps, collapse = ",")
    ))
  }
}

loadStudyUI <- function(id) {
  ns <- NS(id)
  tagList(
    useShinyjs(),
    useShinyFeedback(),
    radioGroupButtons(
      inputId = ns("loadMethod"),
      choiceNames = c("Load from app", "Upload file"),
      choiceValues = c("app", "file"),
      justified = TRUE,
      size = "sm",
      status = "primary"
    ),
    
    hidden(tags$div(id=ns("fromApp"),
                    fluidRow(
                      column(8, pickerInput(inputId = ns("selectedStudy"),
                                            label = "Select study",
                                            choices = filesInfo()$names,
                                            options = pickerOptions(
                                              style = "btn-primary"
                                            ),
                                            width = "100%")),
                      column(4, style = "padding-top: 25px;", 
                             loadingButton(ns("loadFromApp"), 
                                           "Load", 
                                           loadingSpinner = "circle-notch",
                                           loadingLabel = "Please wait...",
                                           style = "width: 100%;"))
                    ))
    ),
    
    hidden(
      tags$div(id=ns("fromFile"),
               
               fluidRow(
                 column(8, fileInput(ns("uploadedFile"), 
                                     "Upload study", 
                                     accept = ".bz2")),
                 column(4, style = "padding-top: 25px;", 
                        disabled(
                          loadingButton(ns("loadFromFile"), 
                                        "Load", 
                                        loadingSpinner = "circle-notch",
                                        loadingLabel = "Please wait...",
                                        style = "width: 100%;")))
               )
               
               
      )
    
    
    )
  )
  
}


loadStudyServer <- function(id) {
  moduleServer(
    id,
    function(input, output, session) {
      
      loadedStudy <- reactiveValues(
        content = NULL, # Content of the currently loaded study
        updatingSteps = c(), # List of the updating steps to proceed
        updatingStep = "OFF", # Current updating step to proceed
        report = c() # List of messages to display at the end of the updating process
      )
      
      observeEvent(input$loadMethod, {
        
        if (input$loadMethod=="file"){
          showElement("fromFile", time = 0.5, anim = TRUE, animType = "slide")
          hideElement("fromApp", time = 0.5, anim = TRUE, animType = "slide")
          
        }else{
          hideElement("fromFile", time = 0.5, anim = TRUE, animType = "slide")
          showElement("fromApp", time = 0.5, anim = TRUE, animType = "slide")
          
        }
      })
      
      refhook <- function(e){
        if (e == "environment") {
          return(globalenv())
        }
        return(NULL)
      }  

      observeEvent(input$loadFromApp, {
        
        hideFeedback("selectedStudy")
        savedStudies <- filesInfo()
        path <- savedStudies[savedStudies$names==input$selectedStudy, "path"]
        path <- ifelse(length(path)>0, path, "")
        
        if (file.exists(path)){
          showFeedbackSuccess(
            inputId = "selectedStudy",
            text = "Study found!"
          )

          if (grepl('RDS.bz2$', path)) {
            study <- readRDS(bzfile(path), refhook = refhook)
          }
          else {
            load(bzfile(path)) #variable name is "study"
          }
          
          if (study$version < SAVE_MODULE_MIN_VER){
            hideFeedback("selectedStudy")
            showFeedbackDanger(
              inputId = "selectedStudy",
              text = paste0("File version must be >= ", SAVE_MODULE_MIN_VER, ", yours is ", study$version)
            )
          }else{
            loadedStudy$content <- study
            loadedStudy$updatingSteps <- PROGRESS_STEPS
            loadedStudy$updatingStep <- PROGRESS_STEPS[1]
            loadedStudy$report <- c()
          }
          
          resetLoadingButton("loadFromApp")
          
        }else{
          
          updatePickerInput(
            session = session,
            inputId = "selectedStudy",
            choices = filesInfo()$names
          )
          
          showFeedback(
            inputId = "selectedStudy",
            text = "Study not found, the list has been updated. Please try again"
          )
          
          resetLoadingButton("loadFromApp")
        }
      })
      

      observeEvent(input$loadFromFile, {
        path <- input$uploadedFile$datapath
        
        study <- tryCatch({
          readRDS(bzfile(path), refhook = refhook)
        },
        error = function(err_msg){
          message(paste("Failed:", err_msg))
        })
        
        # If 'RDS' loading failed, try 'RData' loading
        out <- "study"
        if (is.null(study)) {
          out <- tryCatch({
            load(bzfile(path)) #variable name is "study"
          },
          error = function(err_msg){
            message(paste("Failed:", err_msg))
          })
        }

        hideFeedback("uploadedFile")
        if (is.null(study) && is.null(out)){
          showFeedbackDanger(
            inputId = "uploadedFile",
            text = "Unable to load file!"
          )
        }else if (out != "study"){
          showFeedbackDanger(
            inputId = "uploadedFile",
            text = "File not supported!"
          )
        }else if (study$version < SAVE_MODULE_MIN_VER){
          showFeedbackDanger(
            inputId = "uploadedFile",
            text = paste0("File version must be >= ", SAVE_MODULE_MIN_VER, ", yours is ", study$version)
          )
        }else{
          loadedStudy$content <- study
          loadedStudy$updatingSteps <- PROGRESS_STEPS
          loadedStudy$updatingStep <- PROGRESS_STEPS[1]
          loadedStudy$report <- c()
        }
        
        resetLoadingButton("loadFromFile")
      })
      
      observeEvent(loadedStudy$updatingStep, {
        if (!is.null(loadedStudy$content)) {
          if (loadedStudy$updatingStep == "OFF") {
            html("selectedStudy-text", "")
            html("uploadedFile_progress", "")
          }
          else {
            html("selectedStudy-text", paste("Updating", loadedStudy$updatingStep, "..."))
            html("uploadedFile_progress", paste("Updating", loadedStudy$updatingStep, "..."))
          }
        }
      })
      
      observeEvent(loadedStudy$showReport, {
        if (loadedStudy$showReport) {
          showModal(modalDialog(HTML(paste(loadedStudy$report, collapse = '<br/>')), title = "Loading Report", size = 'l'))
          loadedStudy$showReport <- FALSE
        }
      })
      
      observeEvent(input$uploadedFile, {
        
        ext <- tools::file_ext(input$uploadedFile$datapath)
        hideFeedback("uploadedFile")
        
        if (ext == "bz2"){
          
          showFeedbackSuccess(
            inputId = "uploadedFile",
            text = "Upload complete, click on load"
          )
          shinyjs::enable("loadFromFile")
          
        }else{
          
          showFeedbackDanger(
            inputId = "uploadedFile",
            text = "Please upload a bz2 file"
          )
          
          shinyjs::disable("loadFromFile")
        }
      })
      
      return(loadedStudy)
    }
  )
}