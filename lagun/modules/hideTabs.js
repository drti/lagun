//
// This file is subject to the terms and conditions defined in
// the file 'LICENSE', which is part of this source code.
//

shinyjs.init = function(){
	$('a[data-value=\"nav-tabsurrogateModel\"]').hide();
	$('a[data-value=\"nav-menuImport-tabprelimExplo\"]').hide();
	$('a[data-value=\"Explore\"]').hide();
	$('a[data-value=\"Optimize\"]').hide();
}
