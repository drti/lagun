//
// This file is subject to the terms and conditions defined in
// the file 'LICENSE', which is part of this source code.
//

$(document).ready(function(){
  $("a[data-value=\'Info\']").attr({
    "href":"#", 
    "data-toggle":"modal",
    "data-target":"#modalw"
  });
})