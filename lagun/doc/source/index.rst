.. Lagun documentation master file, created by
   sphinx-quickstart on Tue Oct 27 10:04:48 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Lagun's documentation!
=================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:
   :glob:
   
   introduction.rst
   main_objects.rst
   utility_functions.rst
   modules/*

..
   Indices and tables
   ==================

   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`
