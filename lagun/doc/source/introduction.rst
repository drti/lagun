Introduction
############

Tutorial
********

.. |pdf| image:: _images/icons/pdf64.png
   :target: https://gitlab.com/drti/lagun/-/raw/master/documentation/tutorials/Lagun_Tutorial0_Introduction.pdf

.. list-table::
   :widths: 80 20

   * - Introduction
     - |pdf|

Architecture
************

.. image:: _images/archi/modules_diag.svg
   :target: _images/modules_diag.svg