menuMore
************************


menuMore.R
==========


.. py:function:: menuMore.ui(id)

   :param add_type id: add description

.. py:function:: menuMore.server(input, output, session, NULL)

   :param add_type input: add description
   :param add_type output: add description
   :param add_type session: add description
   :param add_type NULL: add description


about.R
=======


.. py:function:: about.ui(id)

   :param add_type id: add description

.. py:function:: about.server(input, output, session)

   :param add_type input: add description
   :param add_type output: add description
   :param add_type session: add description


settings.R
==========


.. py:function:: collapsible.ui(title, ui, NULL)

   :param add_type title: add description
   :param add_type ui: add description
   :param add_type NULL: add description

.. py:function:: settings.ui(id)

   :param add_type id: add description

.. py:function:: settings.server(input, output, session, listmodels)

   :param add_type input: add description
   :param add_type output: add description
   :param add_type session: add description
   :param add_type listmodels: add description


settingsDOE.R
=============


.. py:function:: settingsDOE.ui(id)

   :param add_type id: add description

.. py:function:: settingsDOE.server(input, output, session, settings)

   :param add_type input: add description
   :param add_type output: add description
   :param add_type session: add description
   :param add_type settings: add description


settingsDownload.R
==================


.. py:function:: settingsDownload.ui(id)

   :param add_type id: add description

.. py:function:: settingsDownload.server(input, output, session, listmodels, writefile)

   :param add_type input: add description
   :param add_type output: add description
   :param add_type session: add description
   :param add_type listmodels: add description
   :param add_type writefile: add description


settingsExploration.R
=====================


.. py:function:: settingsExploration.ui(id)

   :param add_type id: add description

.. py:function:: settingsExploration.server(input, output, session, settings)

   :param add_type input: add description
   :param add_type output: add description
   :param add_type session: add description
   :param add_type settings: add description


settingsGSA.R
=============


.. py:function:: settingsGSA.ui(id)

   :param add_type id: add description

.. py:function:: settingsGSA.server(input, output, session, settings)

   :param add_type input: add description
   :param add_type output: add description
   :param add_type session: add description
   :param add_type settings: add description


settingsOptimization.R
======================


.. py:function:: settingsOptimization.ui(id)

   :param add_type id: add description

.. py:function:: settingsOptimization.server(input, output, session, settings)

   :param add_type input: add description
   :param add_type output: add description
   :param add_type session: add description
   :param add_type settings: add description


settingsRobust.R
================


.. py:function:: settingsRobust.ui(id)

   :param add_type id: add description

.. py:function:: settingsRobust.server(input, output, session, settings)

   :param add_type input: add description
   :param add_type output: add description
   :param add_type session: add description
   :param add_type settings: add description


settingsSurrogate.R
===================


.. py:function:: settingsSurrogate.ui(id)

   :param add_type id: add description

.. py:function:: settingsSurrogate.server(input, output, session, settings)

   :param add_type input: add description
   :param add_type output: add description
   :param add_type session: add description
   :param add_type settings: add description


settingsUQ.R
============


.. py:function:: settingsUQ.ui(id)

   :param add_type id: add description

.. py:function:: settingsUQ.server(input, output, session, settings)

   :param add_type input: add description
   :param add_type output: add description
   :param add_type session: add description
   :param add_type settings: add description
