#
# This file is subject to the terms and conditions defined in
# the file 'LICENSE', which is part of this source code.
#

testthat::test_that("{shinytest2} calibration", {
  
  # input names to ignore in comparison
  input_ignore <- c("nav-menuImport-importDOE-runSet",
                    "nav-menuImport-importDOE-loadStudy-selectedStudy",
                    "nav-saveStudy-filename",
                    "plotly_relayout-A",
                    "plotly_afterplot-A"
  )
  expect <- function(){
    val <- app$get_values()
    input_names <- names(val$input)
    input_names <- input_names[!input_names %in% input_ignore]
    app$expect_values(input=input_names, output=names(val$output), export=names(val$export))
  }
  
  ## Initialization ##
  
  # Launch app
  app <- AppDriver$new(variant = platform_variant(), name = "calibration", seed = 1, height = 922, 
                       width = 1611, load_timeout= 100000, timeout = 30000)
  app$wait_for_idle()
  
  ## Import DOE simu ##
  
  # Begin study modal
  app$set_inputs(`nav-clickedtab` = "nav-menuImport-tabimportDOE")
  app$wait_for_idle()
  
  # Choose import DOE
  app$set_inputs(`nav-menuImport-importDOE-confirm.import.mode` = "click")
  app$wait_for_value(output="nav-menuImport-importDOE-uploadDOE-import.dynui")
  app$wait_for_idle()
  
  # Upload DOE from test case 7
  path_to_doe <- paste0(LAGUN_PATH,
                        "/../documentation/test-cases/Lagun_TestCase7_Calibration/DOEXY_simulated.csv")
  app$upload_file(`nav-menuImport-importDOE-uploadDOE-file` = path_to_doe)
  app$wait_for_idle()
  
  # Confirm import
  app$set_inputs(`nav-menuImport-importDOE-uploadDOE-importconfirm` = "click")
  app$wait_for_idle()
  
  # 3 inputs
  app$set_inputs(`nav-menuImport-importDOE-uploadDOE-nX` = 3)
  app$wait_for_idle()
  
  # Change input bounds
  app$set_inputs(`nav-menuImport-importDOE-uploadDOE-bounds-change` = "click")
  app$wait_for_idle()
  
  # Upload bound file
  path_to_xinfos <- paste0(LAGUN_PATH,
                           "/../documentation/test-cases/Lagun_TestCase7_Calibration/Xinfos.txt")
  app$upload_file(`nav-menuImport-importDOE-uploadDOE-bounds-file` = path_to_xinfos)
  app$wait_for_idle()
  
  # Save bounds
  app$set_inputs(`nav-menuImport-importDOE-uploadDOE-bounds-save` = "click")
  app$wait_for_idle()
  
  # Confirm input definition
  app$set_inputs(`nav-menuImport-importDOE-uploadDOE-inputsconfirm` = "click")
  app$wait_for_idle()
  
  # Confirm output definition
  app$set_inputs(`nav-menuImport-importDOE-uploadDOE-outputsconfirm` = "click")
  app$wait_for_idle()
  
  # Final confirmation
  app$set_inputs(`nav-menuImport-importDOE-uploadDOE-finishconfirm` = "click")
  app$wait_for_idle()
  expect()
  
  ## Import experiment ##
  
  # Panel to import experiment
  app$set_inputs(`nav-clickedtab` = "nav-menuImport-tabdefineCalib")
  app$wait_for_idle()
  expect()
  
  # Upload experiment
  path_to_xp <- paste0(LAGUN_PATH,
                       "/../documentation/test-cases/Lagun_TestCase7_Calibration/data_experimental.csv")
  app$upload_file(`nav-menuImport-defineCalibration-importXPdata-file` = path_to_xp)
  app$wait_for_idle()
  expect()
    
  # Panel to define objective function for calibration
  app$set_inputs(`nav-menuImport-defineCalibration-closeCollapseCalibration` = "Import Experimental Data")
  app$wait_for_idle()
  app$set_inputs(`nav-menuImport-defineCalibration-openCollapseCalibration` = "Define Objective Function")
  app$wait_for_idle()
  expect()
  
  ## Preliminary exploration ##
  
  # Panel for preliminary exploration
  app$set_inputs(`nav-clickedtab` = "nav-menuImport-tabprelimExplo")
  app$wait_for_idle()
  expect()
  
  ## Surrogate model ##
  
  # Surrogate panel
  app$set_inputs(`nav-clickedtab` = "nav-tabsurrogateModel")
  app$wait_for_idle()
  expect()
  
  # Build lasso
  app$set_inputs(`nav-surrogate-buildsurrogate-buildlasso` = "click")
  app$wait_for_idle()
  
  # Acosso 1 settings
  app$set_inputs(`nav-surrogate-buildsurrogate-acosso1settings` = "click")
  app$wait_for_idle()
  
  # Build acosso 1
  app$set_inputs(`nav-surrogate-buildsurrogate-buildacosso1` = "click")
  app$wait_for_idle()
  
  # Acosso 2 settings
  app$set_inputs(`nav-surrogate-buildsurrogate-acosso2settings` = "click")
  app$wait_for_idle()
  
  # Build acosso 2
  app$set_inputs(`nav-surrogate-buildsurrogate-buildacosso2` = "click")
  app$wait_for_idle()
  
  # Kriging settings
  app$set_inputs(`nav-surrogate-buildsurrogate-krigingsettings` = "click")
  app$wait_for_idle()
  
  # Build kriging
  app$set_inputs(`nav-surrogate-buildsurrogate-buildkriging` = "click")
  app$wait_for_idle()
  expect()
    
  ## Optimization ##
  
  # Optimization panel
  app$set_inputs(`nav-clickedtab` = "nav-menuOptimize-taboptimSurrogate")
  app$wait_for_idle()
  
  # Unconstrained Optimization panel
  app$set_inputs(`nav-menuOptimize-openCollapseOptim` = "Unconstrained Optimization")
  app$wait_for_idle()
  
  # Choose OFtotal as first objective
  app$set_inputs(`nav-menuOptimize-unconstrained-chooseY1-choice` = "OFtotal")
  app$wait_for_idle()
  expect()
  
  # Launch optimization
  app$set_inputs(`nav-menuOptimize-unconstrained-goOptim` = "click")
  app$wait_for_idle()
  expect()
  
  
})