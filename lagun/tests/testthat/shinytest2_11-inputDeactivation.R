#
# This file is subject to the terms and conditions defined in
# the file 'LICENSE', which is part of this source code.
#

testthat::test_that("{shinytest2} input deactivation", {
  
  # input names to ignore in comparison
  input_ignore <- c("nav-menuImport-importDOE-runSet",
                    "nav-menuImport-importDOE-loadStudy-selectedStudy",
                    "nav-saveStudy-filename",
                    "plotly_relayout-A",
                    "plotly_afterplot-A"
  )
  expect <- function(){
    val <- app$get_values()
    input_names <- names(val$input)
    input_names <- input_names[!input_names %in% input_ignore]
    app$expect_values(input=input_names, output=names(val$output), export=names(val$export))
  }
  
  ## Initialization ##
  
  # Launch app
  app <- AppDriver$new(variant = platform_variant(), name = "inputDeactivation", 
                       seed = 1, height = 922, width = 1611, 
                       load_timeout= 100000, timeout = 30000)
  app$wait_for_idle()
  
  # Load study
  app$set_inputs(`nav-clickedtab` = "nav-menuImport-tabimportDOE")
  app$wait_for_idle()
  app$set_inputs(`nav-menuImport-importDOE-mode` = "load")
  app$wait_for_idle()
  app$set_inputs(`nav-menuImport-importDOE-loadStudy-loadMethod` = "file")
  app$wait_for_idle()
  path_to_study <- paste0(LAGUN_PATH, 
                          "/tests/testthat/shinytest2_studies/study_surrogateModels-testcase5.RDS.bz2")
  app$upload_file(`nav-menuImport-importDOE-loadStudy-uploadedFile` = path_to_study)
  app$wait_for_idle()
  app$run_js("$('#nav-menuImport-importDOE-loadStudy-loadFromFile').trigger('click');")
  app$wait_for_value(output="nav-menuImport-importDOE-uploadDOE-import.dynui")
  app$wait_for_idle()
  
  ## Surrogate Model
  app$set_inputs(`nav-clickedtab` = "nav-tabsurrogateModel")
  app$wait_for_idle()
  expect()
  
  ## UQ & GSA
  app$set_inputs(`nav-clickedtab` = "nav-menuExplore-tabUQGSASurrogate")
  app$wait_for_idle()
  expect()
  
  ## Import DOE
  # Input activation, deactivate inputs 2 and 4
  app$set_inputs(`nav-clickedtab` = "nav-menuImport-tabimportDOE")
  app$wait_for_idle()
  app$set_inputs(`nav-menuImport-importDOE-uploadDOE-inputactivate2-change` = "click")
  app$wait_for_idle()
  expect()
  app$set_inputs(`nav-menuImport-importDOE-uploadDOE-inputactivate2-inputsactive2` = FALSE)
  app$set_inputs(`nav-menuImport-importDOE-uploadDOE-inputactivate2-inputsactive4` = FALSE)
  app$wait_for_idle()
  expect()
  app$set_inputs(`nav-menuImport-importDOE-uploadDOE-inputactivate2-save` = "click")
  
  ## Surrogate Model
  app$set_inputs(`nav-clickedtab` = "nav-tabsurrogateModel")
  app$wait_for_idle()
  expect()
  app$set_inputs(`nav-surrogate-buildsurrogate-buildlasso` = "click")
  app$wait_for_idle()
  expect()
  
  ## UQ & GSA
  app$set_inputs(`nav-clickedtab` = "nav-menuExplore-tabUQGSASurrogate")
  app$wait_for_idle()
  expect()
  
})