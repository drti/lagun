#
# This file is subject to the terms and conditions defined in
# the file 'LICENSE', which is part of this source code.
#

testthat::test_that("{shinytest2} output groups", {
  
  # input names to ignore in comparison
  input_ignore <- c("nav-menuImport-importDOE-runSet",
                    "nav-menuImport-importDOE-loadStudy-selectedStudy",
                    "nav-saveStudy-filename",
                    "plotly_relayout-A",
                    "plotly_afterplot-A",
                    ".clientValue-default-plotlyCrosstalkOpts"
  )
  expect <- function(){
    val <- app$get_values()
    input_names <- names(val$input)
    input_names <- input_names[!input_names %in% input_ignore]
    app$expect_values(input=input_names, output=names(val$output), export=names(val$export))
  }
  
  ## Initialization ##
  
  # Launch app
  app <- AppDriver$new(variant = platform_variant(), name = "outputGroups", seed = 1, height = 922, 
                       width = 1611, load_timeout= 100000, timeout = 30000)
  app$wait_for_idle()
  
  # Load study
  app$set_inputs(`nav-clickedtab` = "nav-menuImport-tabimportDOE")
  app$wait_for_idle()
  app$set_inputs(`nav-menuImport-importDOE-mode` = "load")
  app$wait_for_idle()
  app$set_inputs(`nav-menuImport-importDOE-loadStudy-loadMethod` = "file")
  app$wait_for_idle()
  path_to_study <- paste0(LAGUN_PATH, 
                          "/tests/testthat/shinytest2_studies/study_uncertaintyDefinition-testcase3.RDS.bz2")
  app$upload_file(`nav-menuImport-importDOE-loadStudy-uploadedFile` = path_to_study)
  app$wait_for_idle()
  app$run_js("$('#nav-menuImport-importDOE-loadStudy-loadFromFile').trigger('click');")
  app$wait_for_value(output="nav-menuImport-importDOE-uploadDOE-import.dynui")
  app$wait_for_idle()
  
  app$set_inputs(`nav-menuImport-importDOE-uploadDOE-outtype2-change` = "click")
  app$wait_for_idle()
  app$set_inputs(`nav-menuImport-importDOE-uploadDOE-outtype2-choose.out.status` = "Surverse")
  app$wait_for_idle()
  expect()
  
  # Change output groups
  app$set_inputs(`nav-menuImport-importDOE-uploadDOE-outtype2-save` = "click")
  app$wait_for_idle()
  expect()
  
  ## Preliminary Exploration
  # Qualitative Exploration
  # Regression Plot
  app$set_inputs(`nav-clickedtab` = "nav-menuImport-tabprelimExplo")
  app$wait_for_value(output = "nav-menuImport-exploreDOE-qualitativeExploration-regression-plot-tab-regression")
  expect()
  
  # Parallel Plot
  app$set_inputs(`nav-menuImport-exploreDOE-qualitativeExploration-tabs` = "nav-menuImport-exploreDOE-qualitativeExploration-pcp")
  app$wait_for_idle()
  expect()
  
  # Regression Plot all in one
  app$set_inputs(`nav-menuImport-exploreDOE-qualitativeExploration-tabs` = "nav-menuImport-exploreDOE-qualitativeExploration-regression-plot-all")
  app$wait_for_value(output = "nav-menuImport-exploreDOE-qualitativeExploration-regression-plot-all-tab-plotallinone.treillis")
  app$wait_for_idle()
  expect()
  
  ## Surrogate Model
  # Lasso
  app$set_inputs(`nav-clickedtab` = "nav-tabsurrogateModel")
  app$wait_for_idle()
  app$set_inputs(`nav-surrogate-buildsurrogate-buildlasso` = "click")
  app$wait_for_idle()
  expect()
  
  ## Explore - Qualitative Exploration
  app$set_inputs(`nav-clickedtab` = "nav-menuExplore-tabexploreSurrogate")
  app$wait_for_idle()
  expect()
  
  ## Explore - Quantitative Exploration
  # Regression plot - One by One
  app$set_inputs(`nav-menuExplore-exploreModel-closePanelExploreModel` = "Qualitative Exploration")
  app$wait_for_idle()
  app$set_inputs(`nav-menuExplore-exploreModel-openPanelExploreModel` = "Quantitative Exploration")
  app$wait_for_value(output = "nav-menuExplore-exploreModel-modelQuantitativeExploration-plotExplo")
  expect()
  
  # Regression plot with smoothing - All in One
  app$set_inputs(`nav-menuExplore-exploreModel-modelQuantitativeExploration-tabs` = "<h4>Regression plot with smoothing - All in One</h4>")
  app$wait_for_value(input="nav-menuExplore-exploreModel-modelQuantitativeExploration-goDown")
  app$wait_for_value(output = "nav-menuExplore-exploreModel-modelQuantitativeExploration-plot.smooth")
  expect()
  
  # Regression plot with smoothing - All in One (Summary)
  app$set_inputs(`nav-menuExplore-exploreModel-modelQuantitativeExploration-tabs` = "<h4>Regression plot with smoothing - All in One (Summary)</h4>")
  app$wait_for_value(output = "nav-menuExplore-exploreModel-modelQuantitativeExploration-plot.smooth.summary")
  expect()
  
})
