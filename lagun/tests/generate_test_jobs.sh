#!/bin/bash

cat <<EOF

image: $CI_REGISTRY_IMAGE/lagun-ci
 
stages:
  - checks
  - testServer
  - shinytest2
EOF

cat <<EOF
before_script:
  - cd lagun
  - ln -s /lagun_packages/renv/library renv/library
  - R -e "renv::load(); renv::restore()"

check1-cran-https:
  stage: checks
  tags:
    - lagun-ci
  before_script:
    - ''
  script:
    - wget https://cloud.r-project.org/src/contrib/PACKAGES
  allow_failure: true

check2-renv:
  stage: checks
  tags:
    - lagun-ci
  script:
    - pwd

EOF

for test in tests/testthat/testServer_*.R
do 

test_name=$(cut -d '/' -f3 <<< "$test")
job_name=${test_name%.*}
cat <<EOF
$job_name:
  stage: testServer
  tags:
    - lagun-ci
  script: 
    - Rscript --vanilla tests/run_testServer.R $test_name
  needs:
    - check2-renv

EOF
done

for test in tests/testthat/shinytest2_*.R
do 

test_name=$(cut -d '/' -f3 <<< "$test")
job_name=${test_name%.*}
cat <<EOF
$job_name:
  stage: shinytest2
  tags:
    - lagun-ci
  script: 
    - unset https_proxy
    - unset http_proxy
    - Rscript --vanilla tests/run_shinytest2.R $test_name
  retry: 2
  allow_failure: false
  needs:
    - check2-renv
  artifacts:
    when: on_failure
    paths:
      - lagun/tests/testthat/_snaps/linux*/$job_name/*
      - lagun/tests/testthat/shinytest2_diff/*
    expire_in: 1 day

EOF
done
