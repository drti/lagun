# Tests

## Test types

Two types of tests are available: 
- **Server function tests**, performed via the `testServer()` function of the package Shiny 
- **Snapshot-based tests**, performed using the package shinytest2

Learn more about these tests here: [Shiny testing overview](https://shiny.posit.co/r/articles/improve/testing-overview/)


## Run tests


### testServer

```console
Rscript tests/run_testServer.R <testServer_testName.R>
```

### shinytest2

#### Prerequisites

- Set the environment variable `NOT_CRAN=false`
- If needed, set the `CHROMOTE_CHROME` environment variable as the path to your chrome based web browser.

```console
Rscript tests/run_shinytest2.R <shinytest2_testName.R>
```

## Gitlab CI

A Gitlab CI pipeline is used to perform the tests. The file `.gitlab-ci.yml` at the root of the project triggers a child pipeline using the script `generate_test_jobs.sh`. The latter creates jobs for every test file contained in `tests/testthat` prefixed with "shinytest2" or "testServer".

### Docker lagun-ci

A docker container named `lagun-ci`is used in the tests, it has been built with all the necessary system packages and R packages for Lagun to run, avoiding the step of building the environment at each run.

The Dockerfile used to build this image is located in `tests/Dockerfile_lagun-ci`. The docker image is built during the ci. The building is started automatically if the R packages have been updates or can be started manually.

### Docker lagun
A docker image of lagun is also built during the ci. It is the one that is used for the hugging face demonstrator : https://lagun-saf-ifp-lagun.hf.space. The building is started is there is a new version tag or can be started manually.

### Pipeline: structure

The triggered pipeline is in three stages:

- **checks**: to ensure that 
    - https is funtional, 
    - the renv library can be restored (if the restore fails, the pipeline will be cancelled)
- **testServer**: contains all testServer tests
- **shinytest2**: contains all shinytest2 tests

