import * as fs from "fs";

import { LocalRun, RunsSet, LauncherServer, Logger } from "../../dist/indexForTests.js";

import * as assert from "assert";

// *********************
// Properties definition
// *********************

const LAUNCHER_SERVER_JSON = "./test/data/launcherServer.json";

const io = buildIO();

/** 
 * Object containing data defining the simulations launcher.
 * @type {import('../../src/typescript/src/launcherServer').LauncherConfig}
 **/ 
const launcherConfig = buildLauncherConfig();

/** 
 * @type {import('../../src/typescript/src/launcherServer').Doe}
 **/ 
const doe = buildDOE();

// ************************
// Initialization functions
// ************************

function buildIO() {
    return {logger: new Logger("testing-logger")};
}

/**
 * @returns {import('../../src/typescript/src/launcherServer').Doe}
 */
function buildDOE() {
    return {
        mat: [
            ["42.1427861793326", "41.4933829622114"],
            ["36.0626836301063", "34.7626408148178"],
            ["37.4461222514968", "30.2408525103672"],
            ["30.76454995088", "29.3740592980246"],
            ["37.907270962267", "36.0252390858637"],
            ["36.3312112668002", "29.5457125552482"],
            ["38.2854369012844", "34.782474928572"],
            ["36.3699504484868", "39.0485249039115"],
            ["39.451964150284", "45.6487016380772"],
            ["34.5975059479062", "36.2664544736421"]
        ],
        paramNames: ["X1", "X2"]
    };
}

/**
 * @returns {import('../../src/typescript/src/launcherServer').LauncherConfig}
 */
function buildLauncherConfig() {
    const fileContent = fs.readFileSync(LAUNCHER_SERVER_JSON, "utf8");
    const config = JSON.parse(fileContent);
    for (let i = 0; i < config.simulator_configs.length; i++) {
        config.simulator_configs[i].id = i;
    }
    return config;
}

// ****************
// Test Definitions
// ****************

describe("Simulations-Launcher", function () {
    describe("LocalRun", function () {
        describe("#templateContext()", templateContextTest);
    });

    describe("LauncherServer", function () {
        describe("#mustachesInParamFiles()", mustachesInParamFilesTest);
    });

    describe("RunsSet", function () {
        describe("#initFromStoringDir()", initFromStoringDirTest);
    });
});

function templateContextTest() {
    for (let i = 0; i < doe.mat.length; i++) {
        it("should have same properties and values than doe for run " + (i + 1), function () {
            /** @type {RunsSet} **/ const runsSet = new RunsSet(io, launcherConfig);
            runsSet.initRuns(doe);

            const localRun = new LocalRun(runsSet, i, launcherConfig.simulator_configs[0]);

            const templateContext = localRun.templateContext();
            Object.getOwnPropertyNames(templateContext);

            assert.equal(
                JSON.stringify(Object.getOwnPropertyNames(templateContext)), 
                JSON.stringify(doe.paramNames));
            assert.equal(
                JSON.stringify(Object.values(templateContext)), 
                JSON.stringify(doe.mat[i]));
        });
    }
}

function mustachesInParamFilesTest() {
    it("should find parameter names", async function () {
        const foundInputs = await LauncherServer.mustachesInParamFiles(launcherConfig.simulator_configs[0]);
        assert.equal(
            JSON.stringify(Object.values(foundInputs)), 
            JSON.stringify(doe.paramNames));
    });
}

function initFromStoringDirTest() {
    /** @type {RunsSet} **/ const runsSet = new RunsSet(io, launcherConfig);
    const initDone = runsSet.initFromStoringDir();
    
    for (const run of runsSet.runs.values()) {
        it("should extract expected info from 'storingDir' for run " + run.id, async function () {
            await initDone;
            assert.equal(
                JSON.stringify(run.paramNames), 
                JSON.stringify(doe.paramNames));
            assert.equal(
                JSON.stringify(run.paramValues), 
                JSON.stringify(doe.mat[run.id].map(v => +v)));
            assert.equal(run.simulatorId, launcherConfig.simulator_configs[0].id)
        });
    }

    assert.ok(
        runsSet.runs.size > 0, 
        "no imported run as expected"
    );

    assert.equal(runsSet.nextRunId, runsSet.runs.size);
}