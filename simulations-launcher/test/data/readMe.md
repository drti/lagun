# Data for testing

## `fakeSimulator` directory

It contains a file `FakeSimulator.bat` which represents a simulator doing little :
- it waits 2 seconds;
- then it generates a `fakeGeneratedFile.txt` file, but it does not contain any results (about the results, see `simulationFile.csv` in the `simulationFiles directory` chapter), it just serves to test that the simulator runs in the correct directory.

It also contains a `FakeSimulator.sh` file , the equivalent of `FakeSimulator.bat` , which can be used under linux.

## `simulationFiles` directory

It contains 4 files which represent the input files of the simulator ("fake" files, in fact, not used by `FakeSimulator.bat`):
- `fakeParamFile1.txt` represents a "parameterized" input file (ie, which involves `X1` and `X2`), the parameters to be injected being indicated by `{{paramName}}` (injection made by the template engine <https://mozilla.github.io/nunjucks>);
- `simulationFile.csv` represents a second "parameterized" input file demonstrating the possibility of combining parameter values ​​by applying complex formulas; it is this file that contains results to return to `Lagun` (trick allowing `FakeSimulator.bat` not to generate the results);
- `fakeDataFile1.txt` and `fakeDataFile2.txt` represent "not parameterized" input files.

## `launcherServer.json` file

This file contains a configuration of the simulations launcher. It is used in mocha testing.