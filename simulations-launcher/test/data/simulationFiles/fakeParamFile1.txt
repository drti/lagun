This file is just used to test that several parameter files can be used.
{# About templating engine: https://mozilla.github.io/nunjucks/templating.htm #}
X1 = {{ X1 }}
X2 = {{ X2 }}
X1 + X2 = {{ X1|float + X2|float }}

{% if (X1 < 0.5) %}X1 is smaller than 0.5{% else  %}X1 is greater than 0.5{% endif %}
{{ "X2 is smaller than 0.5" if (X2 < 0.5) else "X2 is greater than 0.5" }}