# Test using a fake Lagun

This directory contains `Shiny` applications that allows you to test the launcher outside `Lagun`. 

## `fakeLagun` directory

This directory contains `FakeLagun.R`, a `Shiny` application that allows you to test the launcher outside `Lagun`. This mini application consists of only 5 components:
- the `Sample Size` text field allows you to vary the number of experiments to send to the simulations launcher;
- the `Generate DOE & Submit Simulations` button allows to randomly generate experimental points for two parameters X1 and X2, then to submit these experiments to the simulations launcher (the number of parameters and their names are hard-coded, but can be modified for testing);
- a text box is used to display events received from the simulations launcher;
- a `simulatorsView` htmlwidget which allows to configure simulators;
- a `simulationsView` htmlwidget which allows to launch simulations.

Other `Shiny` applications (called  `FakeLagun-xxx`) allows to test other aspects of the launcher.

Note that the communication between the `Shiny` server (which is in R, see [fakeLagun/fakeLagun.R](./fakeLagun/fakeLagun.R)) and the client part (which is in javascript , see [fakeLagun/www/launcher/shiny2LauncherHandler.js](./fakeLagun/www/launcher/shiny2LauncherHandler.js)) uses a technique presented in the article <https://shiny.rstudio.com/articles/communicating-with-js.html>.

## Installation

### Prerequisites
- retrieve the `RStudio` installer and run it (see instructions in web site <https://www.rstudio.com/products/rstudio/download>);
- launch `RStudio` and install `Shiny` with the command `install.packages('shiny')`.
- install `devtools` (in `RStudio`, exécute command `install.packages('devtools')`);
- install `simulatorsView` (in `RStudio`, execute commands `setwd("htmlwidget/simulatorsView")` then `devtools::install()`);
- install `simulationsView` (in `RStudio`, execute commands `setwd("htmlwidget/simulationsView")` then `devtools::install()`);
- Install some optimization packages for tests (tested on Windows only):
  - install IFPEN HubOpt optimization library : `install.packages("../HubOpt-package/HubOpt4R_4.0.tar.gz", repos = NULL, type = "source")`
  - install nloptr package : `install.packages("nloptr")`
  - install rCMA package for CMAES optimizer : `install.packages("rCMA"); install.packages("rJava")`
  - install reticulate package for python optimizers : `install.packages("reticulate");` A recent version of python (tested with version 3.8) should also be installed (+ numpy + scipy) 

### Preliminary deployment

It is necessary to execute the task `npm run build`.

## Fake `Lagun`

After performing `Preliminary deployment` (if relevant):

- start the server part (run `node main.js`, more info in [here](../../readMe.md));
- in `RStudio`, open the [fakeLagun/fakeLagun.R](./fakeLagun/fakeLagun.R);
- in the editor tab showing `FakeLagun.R`, click the `Run App` button (in the toolbar, on the top right) => a `Shiny` server simulating `Lagun` is now available on port 7768 and an html client must be open.
