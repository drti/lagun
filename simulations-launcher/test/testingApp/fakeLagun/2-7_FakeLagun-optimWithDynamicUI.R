#
# This file is subject to the terms and conditions defined in
# the file 'LICENSE', which is part of this source code.
#
###############################
# This testing application shows how to make available optimizers (provided by the optimizers plugins system, cf. 'getOptimDescr.R') remotely (shiny side) or locally (simulations launcher side).
###############################

library(shiny)
library(shinyBS)
library(shinyWidgets)
library(plotly)
library(promises)
library(future)
plan(multisession)
source("modules/asktell_fun.R", local = TRUE)
source("modules/launcherEnv.R", local = TRUE)
source("modules/simTable.R", local = TRUE)

###############################
#  Define client user interface
###############################

LAGUN_WF <- "LagunWf"
LAUNCHER_WF <- "LauncherWf"
LAUNCHER_WF_ASKY_BY_LAGUN <- "LauncherWfAskyByLagun"

ui <- fluidPage(
  # Populate with some widgets
  titlePanel("Fake Lagun"),
  fluidRow(
      column(3,
          wellPanel(
              radioButtons(
                "workflowType",
                label = "Optimizer workflow",
                choices = list(
                  "Optimization by Lagun server" = LAGUN_WF,
                  "Optimization by Simulations Launcher" = LAUNCHER_WF,
                  "Optimization by Simulations Launcher (ask Y to Lagun Server)" = LAUNCHER_WF_ASKY_BY_LAGUN
                ),
                inline = TRUE,
                selected = LAGUN_WF
              ),
              selectInput("simulator", "Simulator:", character(0)),
              fluidRow(
                column(4, pickerInput("toMinimizeIndex", "Response(s) to minimize:", character(0), multiple=TRUE)),
                column(4, pickerInput("toMaximizeIndex", "Response(s) to maximize:", character(0), multiple=TRUE)),
                column(4, pickerInput("toConstrainIndex", "Response(s) to constrain:", character(0), multiple=TRUE))
              ),
              checkboxGroupInput(
                inputId = "pbDefGroup", 
                label = "Problem definition", 
                choices = list(
                  #"Constraints" = "Constraints", 
                  "Categorical" = "Categorical", 
                  #"Mono-Objective" = "Mono-Objective", 
                  #"Multi-Objective" = "Multi-Objective", 
                  "Derivative" = "Derivative"
                ),
                inline = TRUE,
                selected = "Mono-Objective"
              ),
              selectInput(
                inputId = "optimizer", 
                label = "Select optimizer", 
                choices = character(0)
              ),
              # 'addPopover' (called in server part) seems to fail if this tooltip is not created
              bsTooltip(
                id = "optimizer", 
                title = "Available methods depends of problem definition",
                placement = "right", 
                options = list(container = "body")
              ),
              actionButton("optimAction", "Launch Optim"),
              actionButton("stopAction", "Stop")
          )
      ),
      column(9, 
        textOutput("launchStatus"),
        plotlyOutput("monitoringPlot"),
        tableOutput("result"),
        simTable.ui(id = "huboptLocal")
      )
  ),

  # Extend Shiny app with custom JavaScript code
  includeScript(path = "www/launcher/socket.io.js"),
  includeScript(path = "www/launcher/shiny2LauncherHandler-optimLocal.js")
)

#####################
# Define server logic
#####################

# Number of milliseconds to wait between checks of the file by 'reactiveFileReader'
INTERVAL_MILLIS <- 100

# Prefix for a temp file used to send progression from optim process
PROGRESS_FILE_PREFIX <- "progress"

# Prefix for a temp file used to send interrupt message to optim process
INTERRUPT_FILE_PREFIX <- "interrupt"

TMP_DIR <- tempdir()

server <- function(input, output, session) {
  
  launcherEnv <- buildLauncherEnv(input, output, session)
  launcherEnv$retrieveExistingRuns()
  launcherEnv$initSimulatorSelector()
  
  observeEvent(input$simulator, {
    launcherEnv$simulatorHasChanged()
  })

  optimEnv <- buildOptimEnv(input, output, session, launcherEnv)
  
  source("getOptimDescr.R")

  observeEvent(input$workflowType, {
      optimEnv$wfTypeHasChanged()
    }
  )
  
  observeEvent(input$pbDefGroup, {
      optimEnv$pbDefHasChanged()
    },
    ignoreNULL = FALSE
  )
  
  observeEvent(list(input$toMinimizeIndex, input$toMaximizeIndex, input$toConstrainIndex), {
    optimEnv$pbDefHasChanged()
  })

  observeEvent(input$optimizer, {
      optimEnv$optimizerHasChanged()
    }
  )

  observeEvent(input$optimAction, {
    return(optimEnv$optimActionClicked())
  })
  
  observeEvent(input$stopAction, {
    optimEnv$stopActionClicked()
  })
  
  output$result <- renderTable({
    req(optimEnv$optimResult())
  })
  
  output$monitoringPlot <- renderPlotly({
    return(optimEnv$monitoringPlot())
  })

  output$launchStatus <- renderText({
    return(optimEnv$launchStatusText())
  })
  
  observeEvent(reactiveValuesToList(launcherEnv$runs), {
    optimEnv$runsChanged()
  })

  callModule(simTable.server, "huboptLocal", launcherEnv = launcherEnv, optimEnv = optimEnv)
}

###############################
#' Build a kind of object dealing with optimisation execution.
###############################

buildOptimEnv <- function(input, output, session, launcherEnv) {
  optimEnv <- list()
  
  # Unique id used to control relevance of events during inter-process exchanges
  optimEnv$id <- reactiveVal(0)
  # Used to store the optim result (which is display in a table)
  optimEnv$optimResult <- reactiveVal()
  # Selected optim workflow type when 'optimAction' has been clicked
  optimEnv$workflowType <- reactiveVal(LAGUN_WF)
  # Selected simulator when 'optimAction' has been clicked
  optimEnv$simulatorId <- reactiveVal()
  # Values to illustrate monitoring
  optimEnv$monitoringValues <- reactiveVal()
  # Indexes of runs used by this optim
  optimEnv$runIndexes <- reactiveVal()
  # Indexes of runs used by the current iteration
  optimEnv$iterationIndexes <- reactiveVal(c())
  
  # Retrieve optimizers descriptions (which are known by the simulations launcher) through reactive 'input$optimDescrs'
  observeEvent(input$optimDescrs, {
    optimEnv$optimDescrs <<- jsonlite::fromJSON(input$optimDescrs$results, simplifyDataFrame = FALSE)
    if (input$optimDescrs$success) {
      optimEnv$updateOptimizersInput()
    }
    else {
      optimEnv$optimResult(paste("optimizers descriptions retrieval failed:", optimEnv$optimDescrs))
    }
  })
  
  # 'reactiveFileReader' used to retrieve progression from optim process
  optimEnv$progressFile <- reactiveFileReader(
    INTERVAL_MILLIS, 
    session, 
    function() {
      return(tmp.file(optimEnv$id(), PROGRESS_FILE_PREFIX, TMP_DIR))
    },
    function(file) {
      if (file.exists(file)) {
        return(get.object(file))
      }
      return(NULL)
    }
  )

  # 'reactiveFileReader' used to retrieve X from optim process
  xTodoFile <- reactivePoll(INTERVAL_MILLIS, session,
    checkFunc = function() {
      xFile <- X.file(id = optimEnv$id(), dev.path = TMP_DIR)
      if (file.exists(xFile))
        file.info(xFile)$mtime[1]
      else
        ""
    },
    valueFunc = function() {
      xFile <- X.file(id = optimEnv$id(), dev.path = TMP_DIR)
      if (file.exists(xFile)) {
        return(read.io(xFile))
      }
      return(NULL)
    }
  )
  
  # If 'xTodoFile' or 'input$askYEvent' have been changed ...
  observeEvent(c(xTodoFile(), input$askYEvent), {
    optimEnv$computeY()
  })
  
  # When client-side javascript changes the reactive input 'optimResults' ...
  observeEvent(input$optimResults, {
    optimEnv$optimResultsOccured()
  })
  
  # Delete com files after the app has finished running
  onStop(function() {
    del.file(tmp.file(isolate(optimEnv$id()), INTERRUPT_FILE_PREFIX, TMP_DIR))
    del.file(tmp.file(isolate(optimEnv$id()), PROGRESS_FILE_PREFIX, TMP_DIR))
  })
  
  ###############################
  #' Deduced from 'pbDefGroup' checkboxes:
  #' returns the 'tags' part of the problem definition (which is required by 'getOptimDescr' function).
  ###############################

  optimEnv$optimTags <- function() {
    return(list(
        constraints = "Constraints" %in% input$pbDefGroup, 
        categorical = "Categorical" %in% input$pbDefGroup, 
        monoobj = "Mono-Objective" %in% input$pbDefGroup, 
        multiobj = "Multi-Objective" %in% input$pbDefGroup, 
        derivative = "Derivative" %in% input$pbDefGroup
    ))
  }

  ###############################
  #' Callback associated to the 'workflowType' radio-buttons.
  ###############################

  optimEnv$wfTypeHasChanged <- function() {
    # Problem definition needs to be updated
    optimEnv$pbDefHasChanged()
  }

  ###############################
  #' Callback associated to the 'pbDefGroup' checkboxes.
  ###############################

  optimEnv$pbDefHasChanged <- function() {
    pbDefinition <- optimEnv$buildPbDefArg()
    
    if (input$workflowType == LAGUN_WF) {
      # Update available optim descriptions (which are knwon by Lagun)
      optimEnv$optimDescrs <<- getOptimDescr(pbDefinition)
      
      optimEnv$updateOptimizersInput()
    }
    else {
      # Send a message to retrieve optim descriptions (which are known by the simulations launcher)
      session$sendCustomMessage("getOptimDescrs", pbDefinition)
    }
  }

  ###############################
  #' Build optim function argument 'PbDefinition' from UI inputs.
  ###############################

  optimEnv$buildPbDefArg <- function() {
    pbDefinition <- list(tags = optimEnv$optimTags())
    
    pbDefinition$indmin = as.numeric(input$toMinimizeIndex)
    pbDefinition$indmax = as.numeric(input$toMaximizeIndex)
    pbDefinition$indcons = as.numeric(input$toConstrainIndex)
    pbDefinition$ncons = length(pbDefinition$indcons)
    
    pbDefinition$tags$constraints = !identical(pbDefinition$indcons, numeric(0))
    
    pbDefinition$nobj = length(unique(c(pbDefinition$indmin, pbDefinition$indmax)))
    pbDefinition$tags$multiobj = (pbDefinition$nobj > 1)
    pbDefinition$tags$monoobj = !pbDefinition$tags$multiobj
    
    return(pbDefinition)
  }

  ###############################
  #' Update 'optimizer' selector and the gui (built dynamically) associated to the selected optimizer.
  ###############################

  optimEnv$updateOptimizersInput <- function() {
    choices <- list()
    for (i in seq_len(length(optimEnv$optimDescrs))) {
      choices[optimEnv$optimDescrs[[i]]$dispname] <- i
    }
    updateSelectInput(session, inputId = "optimizer", choices = choices)
    
    # Update the gui associated to the selected optimizer.
    optimEnv$optimizerHasChanged()
  }

  ###############################
  #' Update all gui elements associated to the selected optimizer.
  ###############################

  optimEnv$removeOptimParamsUI <- function() {
    removeUI(
      selector = "div[class=\"optimParams\"]",
      immediate = TRUE # 'addPopover" seems to fail if 'immediate' is not set to 'TRUE'
    )
  }

  ###############################
  #' Callback associated to the 'optimizer' selector.
  ###############################

  optimEnv$optimizerHasChanged <- function() {
    # Remove previous optim parameters UI
    optimEnv$removeOptimParamsUI()

    # Check if there're available optim descriptions for given problem definition
    req(input$optimizer)
    req(length(optimEnv$optimDescrs) != 0)

    optimIndex <- as.numeric(input$optimizer)
    req(length(optimEnv$optimDescrs) >= optimIndex)
    
    # Build UI elements for parameters of current optimizer
    paramDefs <- optimEnv$optimDescrs[[optimIndex]]$param
    paramsUI <- lapply(names(paramDefs), function(paramName) {
      paramDef <- paramDefs[[paramName]]
      return(optimEnv$getParamUI(paramName, paramDef))
    })

    # Build UI elements for advanced parameters of current optimizer
    advParamDefs <- optimEnv$optimDescrs[[optimIndex]]$advparam
    advParamsUI <- lapply(names(advParamDefs), function(paramName) {
      advParamDef <- advParamDefs[[paramName]]
      return(optimEnv$getParamUI(paramName, advParamDef))
    })

    # Insert the built UI elements
    optimDescrText <- optimEnv$optimDescrs[[optimIndex]]$descr
    insertUI(
      selector = "div:has(> #optimizer)",
      where = "afterEnd",
      ui = tags$div(class = "optimParams", 
          h6(optimDescrText),
          bsCollapse(
            id = "optimCollapse", 
            open = "Parameters",
            bsCollapsePanel("Parameters", paramsUI, style = "info"),
            bsCollapsePanel("Advanced Parameters", advParamsUI, style = "info")
          )
      ),
      immediate = TRUE # 'addPopover" seems to fail if 'immediate' is not set to 'TRUE'
    )

    # Add popover to UI elements of parameters
    for (paramName in names(paramDefs)) {
      paramDef <- paramDefs[[paramName]]
      addPopover(session, id = paramName, title = paramName, content = paramDef$description, placement = "top")
    }
    # Add popover to UI elements of advanced parameters
    for (paramName in names(advParamDefs)) {
      advParamDef <- advParamDefs[[paramName]]
      addPopover(session, id = paramName, title = paramName, content = advParamDef$description, placement = "top")
    }
  }

  ###############################
  #' Generate a gui for a given optimizer parameter.
  #' @param paramName identifier of the optimizer parameter
  #' @param paramDef description of the optimizer parameter
  ###############################

  optimEnv$getParamUI <- function(paramName, paramDef) {
    label <- paramName
    if (!is.null(paramDef$enum)) {
      paramInput <- selectInput(paramName, label = label, choices = paramDef$enum, selected = paramDef$default)
    }
    else if (paramDef$type == "logical") {
      paramInput <- checkboxInput(paramName, label = label, value = paramDef$default)
    }
    else if (paramDef$type == "integer") {
      paramInput <- numericInput(paramName, label = label, value = paramDef$default)
    }
    else if (paramDef$type == "float") {
      if (is.infinite(paramDef$default)) {
        paramDef$default <- sign(paramDef$default) * .Machine$double.xmax
      }
      paramInput <- numericInput(paramName, label = label, value = paramDef$default)
    }
    else {
      paramInput <- textInput(paramName, label = label, value = paramDef$default)
    }
    return(paramInput)
  }

  ###############################
  #' Callback associated to the 'optimAction' button.
  ###############################

  optimEnv$optimActionClicked <- function() {
    req(launcherEnv$lastFoundInputs())
    # Don't do anything if analysis is already being run
    if (optimEnv$id() != 0) {
      showNotification("Already running analysis")
      return(NULL)
    }
    
    launcherEnv$initLaunching()
    optimEnv$workflowType(input$workflowType)
    optimEnv$simulatorId(as.numeric(input$simulator))
    optimEnv$runIndexes(c())
    optimEnv$optimResult(NULL)
    optimEnv$monitoringValues(NULL)
    
    # Generate an unique 'optimId' to manage concurrent analyses
    optimEnv$id(as.numeric(floor(difftime(Sys.time(), "1970-01-01 00:00:00", units = "secs"))))
    
    nd <- length(launcherEnv$paramNames())

    optimIndex <- as.numeric(input$optimizer)
    optimDescr <- optimEnv$optimDescrs[[optimIndex]]
    optimBaseName <- optimDescr$name
    optimFuncName <- paste0(optimBaseName, ".run")
    print(paste("optimFuncName:", optimFuncName))

    # Init optim function argument 'ParametersValues' 
    parametersValues <- optimEnv$parametersValues(optimDescr$param)

    # Init optim function argument 'AdvancedParametersValues' 
    advParametersValues <- optimEnv$parametersValues(optimDescr$advparam)

    pbDefinition <- optimEnv$buildPbDefArg()
    
    print(paste0("Response to optimize : nobj= ",pbDefinition$nobj," ncons= ",pbDefinition$ncons))
    print(pbDefinition$indmax)
    print("indices of responses to be minimized: ");if (length(pbDefinition$indmin)>0) print(pbDefinition$indmin) else print("NULL")
    print("indices of responses to be maximized: ");if (length(pbDefinition$indmax)>0) print(pbDefinition$indmax) else print("NULL")
    print("indices of responses to be constrained (<= 0): ");if (length(pbDefinition$indcons)>0) print(pbDefinition$indcons) else print("NULL")
	
    #Initial point
    pbDefinition$x0 <- matrix(rep(0, nd), ncol = nd)
    colnames(pbDefinition$x0) <- launcherEnv$paramNames()

    pbDefinition$paramNames <- launcherEnv$paramNames()
    
    #Input variable types
    pbDefinition$tags$categorical = FALSE
    pbDefinition$inputflag = matrix("C",ncol=nd) #by default all the input variables are continuous
    if (pbDefinition$tags$categorical){
      #should identify categorical/integer variables by their indices
      #pbDefinition$inputflag[] = "I" or "Cat"
    }
    
    #Input bounds
    pbDefinition$lb <- matrix(rep(-2, nd), ncol = nd)
    pbDefinition$ub <- matrix(rep(2, nd), ncol = nd)
    
    
    ## Test case ToyPb with 1 categorical input
    
    # pbDefinition$lb[1] = 0
    # pbDefinition$lb[2] = 1
    # pbDefinition$ub[2] = 10
    # pbDefinition$tags$categorical = TRUE
    # pbDefinition$inputflag[1] = "C"
    # pbDefinition$inputflag[2] = "Cat"
    # pbDefinition$x0[2] = 1

    args <- list(
      optimFileName = optimDescr$Filename,
      optimFuncName = optimFuncName, 
      optimFuncArgs = list(
        PbDefinition = pbDefinition, 
        ParametersValues = parametersValues, 
        AdvancedParametersValues = advParametersValues
      ), 
      optimId = optimEnv$id()
    )
    if (optimEnv$workflowType() == LAGUN_WF) {
      result <- optimEnv$futureOptim(args) %...>% optimEnv$optimResult()
    
      # Catch inturrupt (or any other error) and notify user
      result <- catch(result, function(e) {
        print(e$message)
        showNotification(e$message)
      })
      
      # After the promise has been evaluated set 'optimId' to 0 to allow for another optim
      result <- finally(result, function() {
        del.file(tmp.file(optimEnv$id(), PROGRESS_FILE_PREFIX, TMP_DIR))
        del.file(tmp.file(optimEnv$id(), INTERRUPT_FILE_PREFIX, TMP_DIR))
        optimEnv$id(0)
      })
    }
    # Send to client-side a 'optimAction' message
    session$sendCustomMessage("optimAction", args)

    # Return something other than the promise so shiny remains responsive
    return(NULL)
  }

  ###############################
  #' Retrieves from gui the values of given parameters.
  ###############################

  optimEnv$parametersValues <- function(paramDefs) {
    paramValues <- list()
    for (paramName in names(paramDefs)) {
      paramValues[paramName] <- input[[paramName]]

      # adjust value according to parameter type
      paramDef <- paramDefs[[paramName]]
      if (paramDef$type == "integer") {
        p = as.numeric(paramValues[paramName])
        paramValues[paramName] <- round(p)
      }
      else if (paramDef$type == "float") {
        p = as.numeric(paramValues[paramName]) - .Machine$double.xmin
        if (is.infinite(p)) {
          paramValues[paramName] <- as.double(sign(p) * .Machine$double.xmax)
        }
        else {
          paramValues[paramName] <- p
        }
      }
    }
    return(paramValues)
  }

  ###############################
  #' Callback associated to the 'stopAction' button.
  ###############################

  optimEnv$stopActionClicked <- function() {
    if (optimEnv$workflowType() == LAGUN_WF) {
      cat("interrupt\n", file = tmp.file(optimEnv$id(), INTERRUPT_FILE_PREFIX, TMP_DIR))
      ask.Y.unlock(id = optimEnv$id(), dev.path = TMP_DIR)
    }
    else {
      # Send to client-side a 'stopOptim' message
      session$sendCustomMessage("stopOptim", optimEnv$id())
      optimEnv$id(0)
    }
  }
  
  ###############################
  #' Compute Y for given X.
  #' If 'workflowType == LAGUN_WF', X are given through 'xTodo' file;
  #' if 'workflowType == LAUNCHER_WF or LAUNCHER_WF_ASKY_BY_LAGUN', X are given through 'askYEvent' reactive variable.
  ###############################

  optimEnv$computeY <- function() {
    if (optimEnv$workflowType() == LAGUN_WF) {
      xTodo <- req(xTodoFile())
    }
    else {
      req(input$askYEvent)
      askYEvent <- jsonlite::fromJSON(input$askYEvent)
      req(askYEvent$optimId == optimEnv$id())
      xTodo <- as.data.frame(askYEvent$xTodo)
    }
    colnames(xTodo) <- launcherEnv$paramNames()

    searchRun <- function(paramValues) {
      for (rowIndex in names(launcherEnv$runs)) {
        curParamValues <- launcherEnv$runs[[rowIndex]]$paramValues
        if (
          length(paramValues) == length(curParamValues) &&
          all(colnames(paramValues) == colnames(curParamValues)) &&
          all(paramValues == curParamValues) &&
          launcherEnv$runs[[rowIndex]]$simulatorId == optimEnv$simulatorId()
        ) {
          return(rowIndex)
        }
        NULL
      }
    }

    iterationIndexes <- c()
    runIndexes <- optimEnv$runIndexes()
    lastRunIndex <- length(reactiveValuesToList(launcherEnv$runs))
    matIndexes <- c()
    for (i in seq_len(nrow(xTodo))) {
      foundIndex <- searchRun(xTodo[i,])
      curRunIndex <- ifelse(is.null(foundIndex), lastRunIndex + 1, foundIndex)
      if (is.null(foundIndex)) {
        launcherEnv$runs[[toString(curRunIndex)]]$paramValues <<- xTodo[i,]
        launcherEnv$runs[[toString(curRunIndex)]]$simulatorId <<- optimEnv$simulatorId()
        lastRunIndex <- curRunIndex
        matIndexes <- append(matIndexes, i)
      }
      iterationIndexes <- append(iterationIndexes, curRunIndex)
      if (!(curRunIndex %in% runIndexes)) {
        runIndexes <- append(runIndexes, curRunIndex)
      }
    }
    optimEnv$runIndexes(runIndexes)
    optimEnv$iterationIndexes(iterationIndexes)
    optimEnv$runsChanged()

    if (length(matIndexes) != 0) {
      runs <- list(
        paramNames = launcherEnv$paramNames(),
        mat = as.matrix(xTodo[matIndexes,])
      )
      
      args <- list(
        runs = runs,
        simulatorId = optimEnv$simulatorId()
      )
      session$sendCustomMessage("addRuns", args)
    }
  }
  
  ###############################
  #' Reacts to a change of 'runs'; if all iteration points are completed, send results to optim.
  ###############################

  optimEnv$runsChanged <- function() {
    req(length(optimEnv$iterationIndexes()) != 0, optimEnv$id() != 0)

    completed <- sapply(optimEnv$iterationIndexes(), function(index) {
      status <- launcherEnv$runs[[toString(index)]]$status
      return (!is.null(status) && (status == "ended" || status == "onerror"))
    })

    # if all iteration points are completed
    if (all(completed)) {
      # Extract Y values for this iteration and send them to optim 'ask.Y'
      y <- NULL
      for (i in optimEnv$iterationIndexes()) {
          result <- launcherEnv$runs[[toString(i)]]$result
          if (is.null(y)) {
              y <- result
          }
          else {
              y <- rbind(y, result)
          }
      }
      # Clean 'optimEnv$iterationIndexes' to avoid to send optim results if other 'runs' event occured
      optimEnv$iterationIndexes(c())

      yRes <- as.matrix(y)
      
      if (optimEnv$workflowType() == LAGUN_WF) {
        # Send result through a file which will be read by 'ask.Y'
        tell.Y(yRes, id = optimEnv$id(), dev.path = TMP_DIR)
      }
      else if (optimEnv$workflowType() == LAUNCHER_WF_ASKY_BY_LAGUN) {
        # Send result to simulations launcher
        session$sendCustomMessage(
          "tellY",
          list(y = yRes, optimId = optimEnv$id())
        )
      }
    }
  }
  
  ###############################
  #' Callback associated to the 'optimResults' reactive input (set by simulation launcher via client-side javascript).
  ###############################

  optimEnv$optimResultsOccured <- function() {
    # display 'results'
    req(input$optimResults)
    results <- jsonlite::fromJSON(input$optimResults$results)
    req(results$optimId == optimEnv$id())
    if (input$optimResults$success) {
      optimEnv$optimResult(results$result)
    }
    else {
      optimEnv$optimResult(paste("optim failed:", results$result))
    }
    optimEnv$id(0)
  }

  ###############################
  #' Build gui for 'monitoringPlot' output.
  #' If 'workflowType == LAGUN_WF', data are given through 'progress' file;
  #' if 'workflowType == LAUNCHER_WF or LAUNCHER_WF_ASKY_BY_LAGUN', data are given through 'askYEvent' reactive variable.
  ###############################

  optimEnv$monitoringPlot <- function() {
    c(optimEnv$progressFile(), input$askYEvent)
    isolate({
      if (optimEnv$workflowType() == LAGUN_WF) {
        req(optimEnv$progressFile(), cancelOutput = TRUE)
        curXvalues <- optimEnv$progressFile()
      }
      else {
        req(input$askYEvent, cancelOutput = TRUE)
        askYEvent <- jsonlite::fromJSON(input$askYEvent)
        req(askYEvent$optimId == optimEnv$id(), cancelOutput = TRUE)
        curXvalues <- askYEvent$xTodo
      }
      if (is.null(optimEnv$monitoringValues())) {
        optimEnv$monitoringValues(curXvalues)
      }
      else {
        optimEnv$monitoringValues(rbind(optimEnv$monitoringValues(), curXvalues))
      }

      plot_ly(
        x = optimEnv$monitoringValues()[, 1],
        y = optimEnv$monitoringValues()[, 2],
        type = "scatter",
        mode = "markers"
      )
    })
  }

  ###############################
  #' Build text for 'launchStatus' output. 
  #' Depends of 'launcherEvent' reactive input (set by simulation launcher via client-side javascript).
  ###############################

  optimEnv$launchStatusText <- function() {
    req(input$launcherEvent, input$launcherEvent$type == "ended", cancelOutput = TRUE)
    expIndex <- input$launcherEvent$index + 1
    expParamValues <- isolate(launcherEnv$runs[[toString(expIndex)]]$paramValues)
    eventData <- launcherEnv$parseResults(input$launcherEvent$data)
    return(paste(
      "From launcher: run_", expIndex,
      " with ", launcherEnv$paramNames()[1], " = ", expParamValues[launcherEnv$paramNames()[1]],
      " and ", launcherEnv$paramNames()[2], " = ", expParamValues[launcherEnv$paramNames()[2]],
      " is completed with result:", jsonlite::toJSON(eventData), sep = "")
    )
  }
  
  ###############################
  #' Use 'future' to do the optimisation.
  #' Only called if 'workflowType == LAGUN_WF'.
  ###############################

  optimEnv$futureOptim <- function(args) {
    optimId <- optimEnv$id()
    paramNames <- launcherEnv$paramNames()

    future({
      # Function to optimize
      args$optimFuncArgs$PbDefinition$ObjFunc <- function(x) {
        # Check for user interrupts
        interruptFileName <- tmp.file(optimId, INTERRUPT_FILE_PREFIX, TMP_DIR)
        if (file.exists(file = interruptFileName)) {
          del.file(interruptFileName)
          stop("User Interrupt")
        }
        
        nsim = length(x)/nd
        x <- as.data.frame(t(matrix(x,ncol= nsim,nrow=nd)))   #useful for nlopt (nlopt does not return a matrix)
        colnames(x) <- colnames(args$optimFuncArgs$PbDefinition$x0)
        
        # Send some monitoring data through 'progressFile' reactiveFileReader
        set.object(file = tmp.file(optimId, PROGRESS_FILE_PREFIX, TMP_DIR), data = x)

        # Send 'x' through the 'xTodoFile' reactiveFileReader and retrieve Y through 'launcherEvent'
        y <- as.numeric(ask.Y(x, id = optimId, dev.path = TMP_DIR))
        
        # print(paste0("nsim = ",nsim))
        y <- matrix(y, nrow=nsim, byrow=F)
        indmax = args$optimFuncArgs$PbDefinition$indmax
        if (!is.null(indmax)){
          y[,indmax] <- -y[,indmax, drop=F]
        }
        
        yRes <- y[,c(args$optimFuncArgs$PbDefinition$indmin,args$optimFuncArgs$PbDefinition$indmax,args$optimFuncArgs$PbDefinition$indcons), drop=F]
        # print(paste0("Responses given to the optimizer: "))
        # print(yRes)
        return(yRes)
      }

      nd = length(paramNames)
      source(args$optimFileName)
      solution <- do.call(args$optimFuncName, args$optimFuncArgs)

      namedResults <- as.data.frame(solution)
      
      return(namedResults)
    }, seed = NULL)
  }
  
  return(optimEnv)
}

##########################
# Create Shiny app objects
##########################
shinyApp(ui, server)
