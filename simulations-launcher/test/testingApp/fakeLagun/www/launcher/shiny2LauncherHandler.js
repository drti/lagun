(function() {

    // Types of message sent through sockets
    const MESSAGE = {
        attachSet: "attach set", 
        submitAction: "submit action", 
        launcherEvent: "launcher event"
    }

    // Launcher server part is reached through port 3000 of the local host
    const launcherPort = 3000;

    // Retrieve a socket instance connected to launcher server, specifying a namespace called 'simulations'
    const url = "http://localhost:" + launcherPort + "/simulations"; // "https://localhost:" + launcherPort + "/simulations";
    const opts = { "reconnectionAttemps" : "Infinity", "timeout" : 10000, "transports" : ["websocket"] };
    // @ts-ignore
    const simulationsSocket = io.connect(url, opts);

    // Register a client-side function to receive 'submitAction' messages which come from Shiny server
    // @ts-ignore
    Shiny.addCustomMessageHandler("submitAction", function(doe) {
        // Forward 'submitAction' messages to the launcher server
        simulationsSocket.emit(MESSAGE.submitAction, doe);
    });

    simulationsSocket.emit(
        MESSAGE.attachSet,
        "default", 
        function(attached) {
            console.log("Simulations set is attached: ", attached);
            if (attached) {
                // Listen event sent by the launcher server
                simulationsSocket.on(MESSAGE.launcherEvent, function (launcherEvent) {
                    console.log(launcherEvent, " received");
                    // Forward 'launcher event' to Shiny through a reactive input value
                    // @ts-ignore
                    Shiny.setInputValue("launcherEvent", launcherEvent, {priority: "event"});
                });
            }
        }
    );

})(); // Properly isolate your script environment with an IIFE (immediately-invoked function expression)
