(function() {

    // Types of message sent through sockets
    const MESSAGE = {
        attachSet: "attach set", 
        submitAction: "submit action", 
        runAction: "run action", 
        addRuns: "add runs", 
        setRunSimulator: "set run simulator", 
        launcherEvent: "launcher event",
        getLauncherConfig: 'get launcher config', 
        searchMustacheInputs: 'search mustache inputs'
    }

    // Launcher server part is reached through port 3000 of the local host
    const launcherPort = 3000;

    const url = 'http://localhost:' + launcherPort; // 'https://localhost:' + launcherPort;
    // Retrieve a socket instance connected to launcher server, specifying a namespace called 'simulators'
    const simulatorsUrl = url + '/simulators';
    // Retrieve a socket instance connected to launcher server, specifying a namespace called 'simulations'
    const simulationsUrl = url + '/simulations';
    const opts = { 'reconnectionAttemps' : 'Infinity', 'timeout' : 10000, 'transports' : ['websocket'] };
    const simulatorsSocket = io.connect(simulatorsUrl, opts);
    const simulationsSocket = io.connect(simulationsUrl, opts);

    // Register a client-side function to receive 'get launcher config' messages which come from Shiny server
    Shiny.addCustomMessageHandler('getLauncherConfig', function(x) {
        // Send a 'get launcher config' message to the launcher server,
        // providing a callback function, in order to retrieve simulators config.
        simulatorsSocket.emit(MESSAGE.getLauncherConfig, "", function(launcherConfig) {
            Shiny.setInputValue('simulatorsConfigs', launcherConfig);
        });
    });
                
    // Register a client-side function to receive 'search mustache inputs' messages which come from Shiny server
    Shiny.addCustomMessageHandler('searchMustacheInputs', function(simulatorId) {
        // Send a 'search mustache inputs' message to the launcher server,
        // providing a callback function, in order to retrieve input variable names for given simulator id.
        simulationsSocket.emit(MESSAGE.searchMustacheInputs, simulatorId, function(foundInputs) {
            Shiny.setInputValue('foundInputs', foundInputs);
        });
    });
                
    simulationsSocket.emit(
        MESSAGE.attachSet,
        "default", 
        function(attached) {
            console.log("Simulations set is attached: ", attached);
            if (attached) {
                // Listen event sent by the launcher server
                simulationsSocket.on(MESSAGE.launcherEvent, function (launcherEvent) {
                    // Forward 'launcher event' to Shiny through a reactive input value
                    // @ts-ignore
                    Shiny.setInputValue("launcherEvent", launcherEvent, {priority: "event"});
                });

                // Register a client-side function to receive 'addRuns' messages which come from Shiny server
                // @ts-ignore
                Shiny.addCustomMessageHandler("addRuns", function(args) {
                    // Send 'addRuns' message to the launcher server
                    simulationsSocket.emit(MESSAGE.addRuns, args.runs, addedRuns => {
                       // Send a launcher event for runs which already exists and are 'ended' or 'onerror'
                        addedRuns.filter(r => r.status === "ended" || "onerror")
                            .forEach(r => {
                                const launcherEvent = {
                                    index: r.index,
                                    type: r.status,
                                    data: r.result
                                }
                                Shiny.setInputValue("launcherEvent", launcherEvent, {priority: "event"});
                            });

                        // Launch runs which have 'ready' status
                        const readyRuns = addedRuns.filter(r => r.status === "ready");
                        if (readyRuns.length !== 0) {
                            const runIds = addedRuns.map(r => r.index);
                            simulationsSocket.emit(MESSAGE.setRunSimulator, {
                                simulatorId: r2JsIndex(args.simulatorId),
                                runIds: runIds
                            });
                            simulationsSocket.emit(MESSAGE.runAction, {
                                actionId: "ConfigureLaunchLoad",
                                runIds: runIds
                            });
                        }
                    });
                });
            }
        }
    );

    function r2JsIndex(index) {
        return (index !== null) ? index - 1 : index;
    }

})(); // Properly isolate your script environment with an IIFE (immediately-invoked function expression)
