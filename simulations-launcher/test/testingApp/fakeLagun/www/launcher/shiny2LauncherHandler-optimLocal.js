(function() {

    // Types of message sent through sockets
    const MESSAGE = {
        attachSet: "attach set", 
        getRuns: 'get runs', 
        submitAction: "submit action", 
        getOptimDescrs: "get optim descrs", 
        startOptim: "start optim", 
        stopOptim: "stop optim", 
        runAction: "run action", 
        addRuns: "add runs", 
        setRunSimulator: "set run simulator", 
        launcherEvent: "launcher event",
        getLauncherConfig: 'get launcher config', 
        searchMustacheInputs: 'search mustache inputs',
        askYEvent: "askY event",
        tellY: "tell Y"
    }

    // Launcher server part is reached through port 3000 of the local host
    const launcherPort = 3000;

    const url = 'http://localhost:' + launcherPort; // 'https://localhost:' + launcherPort;
    // Retrieve a socket instance connected to launcher server, specifying a namespace called 'simulators'
    const simulatorsUrl = url + '/simulators';
    // Retrieve a socket instance connected to launcher server, specifying a namespace called 'simulations'
    const simulationsUrl = url + '/simulations';
    const opts = { 'reconnectionAttemps' : 'Infinity', 'timeout' : 10000, 'transports' : ['websocket'] };
    const simulatorsSocket = io.connect(simulatorsUrl, opts);
    const simulationsSocket = io.connect(simulationsUrl, opts);

    // Register a client-side function to receive 'get launcher config' messages which come from Shiny server
    Shiny.addCustomMessageHandler('getLauncherConfig', function(x) {
        // Send a 'get launcher config' message to the launcher server,
        // providing a callback function, in order to retrieve simulators config.
        simulatorsSocket.emit(MESSAGE.getLauncherConfig, "", function(launcherConfig) {
            Shiny.setInputValue('simulatorsConfigs', launcherConfig);
        });
    });
                
    // Register a client-side function to receive 'search mustache inputs' messages which come from Shiny server
    Shiny.addCustomMessageHandler('searchMustacheInputs', function(simulatorIds) {
        // Send a 'search mustache inputs' message to the launcher server,
        // providing a callback function, in order to retrieve input variable names for given simulator id.
        simulationsSocket.emit(MESSAGE.searchMustacheInputs, simulatorIds, function(foundInputs) {
            Shiny.setInputValue('foundInputs', foundInputs);
        });
    });
                
    // Register a client-side function to receive 'get optim descrs' messages which come from Shiny server
    Shiny.addCustomMessageHandler('getOptimDescrs', function(optimPbDef) {
        // Send a 'get optim descrs' message to the launcher server,
        // providing a callback function, in order to retrieve descriptions of optimizers that can be used for the given problem definition.
        simulatorsSocket.emit(MESSAGE.getOptimDescrs, optimPbDef, function(success, optimDescrs) {
            Shiny.setInputValue('optimDescrs', 
                { success: success, results: JSON.stringify(optimDescrs) }
            );
        });
    });
                
    simulationsSocket.emit(
        MESSAGE.attachSet,
        "default", 
        function(attached) {
            console.log("Simulations set is attached: ", attached);
            if (attached) {
                // Listen event sent by the launcher server
                simulationsSocket.on(MESSAGE.launcherEvent, function (launcherEvent) {
                    // Forward 'launcher event' to Shiny through a reactive input value
                    // @ts-ignore
                    Shiny.setInputValue("launcherEvent", launcherEvent, {priority: "event"});
                });

                // Listen event sent by the launcher server
                simulationsSocket.on(MESSAGE.askYEvent, function (askYEvent) {
                    // Forward 'askY event' to Shiny through a reactive input value
                    // @ts-ignore
                    Shiny.setInputValue("askYEvent", JSON.stringify(askYEvent), {priority: "event"});
                    // dataframe are flattened by Shiny => workaround, stringify dataframe https://github.com/rstudio/shiny/issues/1098#issuecomment-216579034
                });

                // Register a client-side function to receive 'getRuns' messages which come from Shiny server
                // @ts-ignore
                Shiny.addCustomMessageHandler("getRuns", function(args) {
                    // Send 'getRuns' messages to the launcher server
                    simulationsSocket.emit(MESSAGE.getRuns, "", function(runs) {
                        runs.forEach(r => { r.simulatorId = js2RIndex(r.simulatorId); });
                        // @ts-ignore
                        Shiny.setInputValue("runList", JSON.stringify(runs), {priority: "event"});
                    });
                });

                // Register a client-side function to receive 'optimAction' messages which come from Shiny server
                // @ts-ignore
                Shiny.addCustomMessageHandler("optimAction", function(args) {
                    // Send 'startOptim' messages to the launcher server
                    simulationsSocket.emit(MESSAGE.startOptim, args, (success, results) => {
                        console.log("success:", success, " results:", results);
                        // @ts-ignore
                        Shiny.setInputValue("optimResults", 
                            { success: success, results: JSON.stringify(results) }, 
                            { priority: "event" }
                        );
                    });
                });

                // Register a client-side function to receive 'stopOptim' messages which come from Shiny server
                // @ts-ignore
                Shiny.addCustomMessageHandler("stopOptim", function(optimId) {
                    // Send 'stopOptim' messages to the launcher server
                    simulationsSocket.emit(MESSAGE.stopOptim, optimId);
                });

                // Register a client-side function to receive 'tellY' messages which come from Shiny server
                // @ts-ignore
                Shiny.addCustomMessageHandler("tellY", function(args) {
                    // Send 'tellY' messages to the launcher server
                    simulationsSocket.emit(MESSAGE.tellY, args);
                });

                // Register a client-side function to receive 'addRuns' messages which come from Shiny server
                // @ts-ignore
                Shiny.addCustomMessageHandler("addRuns", function(args) {
                    if (args.runs.length !== 0) {
                        // Send 'addRuns' message to the launcher server
                        simulationsSocket.emit(MESSAGE.addRuns, args.runs, addedRuns => {
                            const runIds = addedRuns.map(r => r.index);
                            simulationsSocket.emit(MESSAGE.setRunSimulator, {
                                simulatorId: r2JsIndex(args.simulatorId),
                                runIds: runIds
                            });
                            simulationsSocket.emit(MESSAGE.runAction, {
                                actionId: "ConfigureLaunchLoad",
                                runIds: runIds
                            });
                        });
                    }
                });
            }
        }
    );

    function r2JsIndex(index) {
        return (index !== null) ? index - 1 : index;
    }

    function js2RIndex(index) {
        return (index !== null) ? index + 1 : index;
    }

})(); // Properly isolate your script environment with an IIFE (immediately-invoked function expression)
