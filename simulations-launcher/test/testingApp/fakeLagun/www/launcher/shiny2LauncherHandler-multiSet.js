(function() {

    // Types of message sent through sockets
    const MESSAGE = {
        attachSet: "attach set", 
        submitAction: "submit action", 
        launcherEvent: "launcher event"
    }

    // Launcher server part is reached through port 3000 of the local host
    const launcherPort = 3000;

    // Retrieve a socket instance connected to launcher server, specifying a namespace called 'simulations'
    const url = "http://localhost:" + launcherPort + "/simulations"; // "https://localhost:" + launcherPort + "/simulations";
    const opts = { "reconnectionAttemps" : "Infinity", "timeout" : 10000, "transports" : ["websocket"] };

    const simSetSockets = new Map();
    
    // Register a client-side function to receive 'submitAction' messages which come from Shiny server
    // @ts-ignore
    Shiny.addCustomMessageHandler("submitAction", function(args) {
        const simulationsSocket = getSimulationsSocket(args);
        // Forward 'submitAction' messages to the launcher server
        // @ts-ignore
        simulationsSocket.emit(MESSAGE.submitAction, args.doe);
    });

    // Register a client-side function to receive 'listenEvents' messages which come from Shiny server
    // @ts-ignore
    Shiny.addCustomMessageHandler("listenEvents", function(args) {
        const simulationsSocket = getSimulationsSocket(args);
        console.log("listenEvents, setId:", args.setId, " simulationsSocket:", simulationsSocket);
    });

    function getSimulationsSocket(args) {
        let simulationsSocket = simSetSockets.get(args.setId);
        if (!simulationsSocket) {
            // @ts-ignore
            simulationsSocket = io.connect(url, opts);
            simulationsSocket.emit(
                MESSAGE.attachSet,
                args.setId, 
                function(attached) {
                    console.log("Simulations set '", args.setId, "' is attached: ", attached);
                    if (attached && args.eventReactive) {
                        // Listen event sent by the launcher server
                        simulationsSocket.on(MESSAGE.launcherEvent, function (launcherEvent) {
                            console.log(launcherEvent, " received for simulations set '", args.setId);
                            // Forward 'launcher event' to Shiny through a reactive input value
                            // @ts-ignore
                            Shiny.setInputValue(args.eventReactive["launcherEvent"], launcherEvent, {priority: "event"});
                        });
                    }
                }
            );
            simSetSockets.set(args.setId, simulationsSocket);
        }
        return simulationsSocket;
    }

})(); // Properly isolate your script environment with an IIFE (immediately-invoked function expression)
