![Simulations Launcher snapshot](./doc/snapshots/SL.png)

# Simulations Launcher

Simulations Launcher is a web server allowing to run a set of simulations. It can be used to directly connect Lagun to your simulation scripts.

## Install & run

### `node.js` prerequisite (version 18)

- retrieve the `node.js` installer and run it (see instructions in web site <https://nodejs.org>) => will also allow to use `npm` , the package manager of `node.js`;
- in a command terminal, `node -v` must return the version (for example "v18.18.1").

Optionally, you have to configure `npm` to indicate the proxies via the commands:
- `npm config set proxy http://irproxy:8082`
- `npm config set https-proxy http://irproxy:8082`

### Installation

- clone the current repository (if not already done): `git clone https://gitlab.com/drti/lagun.git`
- in a command terminal, move to the root directory `simulations-launcher`;
- run the command `npm ci`;
- run the command `npm run build`.

**Notes**

Lagun can ask to the simulations launcher to run optimization process. In this case, R4.4.1 must be installed and the package manager `renv` is used to complete installation of required R packages:
- install R4.4.1 (available from [CRAN](https://cran.r-project.org/));
- in a command terminal, move to the root directory `simulations-launcher`;
- install `renv` with the command `R -e "install.packages('renv', repos = 'http://cloud.r-project.org')"`;
- download and install all the required packages with the command `R -e "options(timeout = 600); options(renv.config.connect.timeout = 600); renv::load(); renv::restore()"`.

### Execution

- move to `dist` directory;
- start a simulation launcher `node main.js`

By default, it listens on port 3000, but you can specify another port (for example 3001) by adding an argument `node main.js 3001`

Once the application is running:

- Simulations launcher is available on port 3000, start Lagun to use it:

![Simulations Launcher with Lagun](./doc/snapshots/SL-With-Lagun.png)

- A site is available at <http://localhost:3000>, it can help to configure simulators and monitor simulations.

![Simulators Configurations](./doc/snapshots/SimulatorsConfigurations.png)

If the simulations launcher listens on another port (for example 3001), you must specify it to Lagun. There are two possibilities:

- before starting of lagun, set an environment variable `simulations_launcher_url`

``` bash
cd {LAGUN_DIR}
R -e "renv::load(); Sys.setenv(simulations_launcher_url = 'http://localhost:3001'); shiny::runApp('app.R', host = '0.0.0.0', port=6023, launch.browser = TRUE)"
```

- use the query string '?' in the Lagun URL to specify `simulations_launcher_url`

``` bash
cd {LAGUN_DIR}
R -e "renv::load(); shiny::runApp('app.R', host = '0.0.0.0', port = 6023, launch.browser = function(url) { browseURL(paste0(url, '?simulations_launcher_url=http://localhost:3001')) })"
```

## Simulators Configurations

Simulators Configurations are stored in a file called `launcherServer.json`. 

To write your own file, you can:
- when the simulations launcher is not running, modify with a text editor the one which is present in the `dist` directory; further details on its structure are provided in [doc/launcherConfigurationFile.md](./doc/launcherConfigurationFile.md).
- use the gui available at <http://localhost:3000> to modify it.

To use your own file, you have two possibilities:
- before the simulations launcher is started, copy it in the `dist` directory;
- use the gui available at <http://localhost:3000> to import it.

Some `launcherServer.json` examples are provided:
- [../simulator-examples/ModelicaSimulator/launcherServer.json](../simulator-examples/ModelicaSimulator/launcherServer.json);
- [../simulator-examples/PySimulator/launcherServer.json](../simulator-examples/PySimulator/launcherServer.json);
- [../simulator-examples/RSimulator/launcherServer.json](../simulator-examples/RSimulator/launcherServer.json);
- [../simulator-examples/UM-Bridge/launcherServer.json](../simulator-examples/UM-Bridge/launcherServer.json);
- [src/typescrip/src/launcherServer-example.json](./src/typescrip/src/launcherServer-example.json).

**Notes**

When a simulation is started:
- a directory (dedicated to the execution of the simulations) is created in `storing_dir`
- for each simulation, a directory is created, with the name `runN` , where `N` is the index of the simulated point;
- input files of the simulator are processed by a template engine (in input files, {{inputName}} are searched and replaced by the corresponding input of the simulated point);
- processed input files are copied into `runN` directory;
- simulator is launched from `runN` directory.

## Using Dockerfile

- in a command terminal, move to the root directory `simulations-launcher`;
- build the image (if not already done) with the command:
```bash
docker build -f Dockerfile-Alpine --target simulations_launcher_with_optimizers -t simulations-launcher-alpine .
```
- if a container is running from this image, stop it,
  - run the command `docker ps` to retieve the id of this container,
  - run the command `docker stop <the-container-id>` to stop this container,
  - run the command `docker rm <the-container-id>` to remove this container;
- start a container with the command (use `127.0.0.1` in lieu of `0.0.0.0` if you don't want simulations launcher to be visible on the network):

if you use Linux:

```bash
docker run -dp 0.0.0.0:3000:3000 \
    --mount type=volume,src=running_dir,target=/runningDir \
    --mount type=bind,src="$(pwd)/../simulator-examples",target=/simulator-examples \
    simulations-launcher-alpine
```

if you use Windows and want to use Git Bash (see [Working with Git Bash](https://docs.docker.com/desktop/troubleshoot/topics/#working-with-git-bash) for syntax differences):

```bash
docker run -dp 0.0.0.0:3000:3000 \
    --mount type=volume,src=running_dir,target=//runningDir \
    --mount type=bind,src="/$(pwd)/../simulator-examples",target=/simulator-examples \
    simulations-launcher-alpine
```

if you use Windows and want to use PowerShell:

```bash
docker run -dp 0.0.0.0:3000:3000 `
    --mount type=volume,src=running_dir,target=/runningDir `
    --mount type=bind,src="$(pwd)/../simulator-examples",target=/simulator-examples `
    simulations-launcher-alpine
```
