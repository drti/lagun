# `src/typescript` directory

It contains `typescript` source files for:
- the simulations launcher itself [./src/typescript/src/main.ts](./src/typescript/src/main.ts);
- front-end components [./src/typescript/src/view](./src/typescript/src/view).

# `src/web-files` directory

This directory contains `html` source files for front-end.

# `htmlwidget/simulationsView` directory

This directory contains a htmlwidget composed of three parts:
- a table containing the list of all the simulations; by clicking on lines (possibly using the `Ctrl` and/or `Shift` keys), the state of simulation becomes "selected";
- a drop-down list to assign a simulator to selected simulations (or all simulations if none are selected);
- a drop-down list to execute an action to selected simulations (or all simulations if none are selected); available actions are `Configure, Launch & Load`, `Configure`, `Load` and `Cancel`.

# `htmlwidget/simulatorsView` directory

This directory contains a htmlwidget composed of a single button `Simulator Configurations`. By clicking this button, a modal dialog is opened and allows to edit the launcher configuration file ([doc/launcherConfigurationFile.md](./doc/launcherConfigurationFile.md) describes available fields of the `launcherServer.json` file).

# `doc` directory

This directory contains additional documentation:
- [doc/launcherConfigurationFile.md](./doc/launcherConfigurationFile.md) describes available fields of the `launcherServer.json` file (which contains some parameters of the simulations launcher);
- [doc/communication.md](./doc/communication.md) describes the way to communicate between Lagun and simulation Launcher;
- [doc/launcherMessages.md](./doc/launcherMessages.md) describes the `socket.io` messages available to communicate with the launcher.

# `test` directory

- `testingApp` directory contains a `Shiny` application that allows you to test the launcher outside `Lagun` (more info in [test/testingApp/readMe.md](./test/testingApp/readMe.md));

- `mocha` directory contains unit tests which can be run thanks to command `npm test`.

- `data` directory contains data used by the `Shiny` application and by unit tests.

`testingApp` provides examples of integration to a Shiny application; for simplest example, see files [test/testingApp/fakeLagun/fakeLagun.R](./test/testingApp/fakeLagun/fakeLagun.R) and [test/testingApp/fakeLagun/www/launcher/shiny2LauncherHandler.js](./test/testingApp/fakeLagun/www/launcher/shiny2LauncherHandler.js) (more info in [test/testingApp/readMe.md](./test/testingApp/readMe.md)).

# `package.json` and `package-lock.json` files

These files contains:
- the list of required packages (`package-lock.json` contains the full tree of all required dependencies);
- scripts used to generate the simulations launcher.
