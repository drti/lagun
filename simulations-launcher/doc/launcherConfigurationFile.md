# Launcher configuration File

The file `src/typescript/src/launcherServer-example.json` contains some parameters of the simulations launcher.

It can be edited using a text editor (restarting of the server is needed), or using the gui available at <http://localhost:3000>.

It defines the following parameters:
- `storing_dir` defines the directory where the input and output files of the simulations will be stored (reference for relative paths is the 'main' execution directory);
- `simulator_configs` lists the simulator configurations, each element containing the following fields:
    - `config_name` provides the name of the configuration (intended to be displayed in the gui),
    - `config_descr` provides a description of the configuration (intended to be displayed in the gui),
    - `host` specifies the host to use for executing the simulator,
    - `port` provides the port used to contact the simulator host (optional, mandatory if host is distant),
    - `running_dir` indicates the directory where the simulations will be run (optional, mandatory if host is distant),
    - `cluster_size` is optional and indicates how many runs can run in parallel on this host,
    - `simulator_exe` references the simulator to use (reference for relative paths is the 'main' execution directory),
    - `argument` provides a string which is sent as argument to the `simulator_exe` file,
    - `simulation_dir` indicates the directory containing the input files that will be used by the simulator (reference for relative paths is the 'main' execution directory),
    - `simulation_param_files` indicates the list of input files that are "parameterized",
    - `simulation_data_files` indicates the list of input files that are "not parameterized",
    - `result_file_name` indicates the name of the file containing the results (a `csv` file to find in the simulation directory).
    - `res_count` indicates the number of columns to be found in `result_file_name`. Can be an array of numbers where each value is the number of columns to be found for a specific functional output (for example, '[nc1, nc2]' means there are two functional outputs, where the first one is associated to the 'nc1' first columns and the second one is associated to the 'nc2' last columns). Both possibilities can be used by inserting them in an array.
