# 'Client to server' messages

## 'get protocol version' (data, fn)

The client wants to retrieve the protocol version used by the server.

- `data`: unused.
- `fn`: a callback function receiving protocol version as argument.

## 'get run sets' (data, fn)

The client wants to retrieve the list of the available sets of runs.

- `data`: unused.
- `fn`: a callback function receiving `runSets` as argument (an array of set ids).

## 'attach set' (runSetId, fn)

The client wants to attach its connection to a given set of runs.

- `runSetId`: unused.
- `fn`: a callback function receiving nothing as argument.

**Note**: when a socket is not attached to a run set, a default one is used (its id is 'default').

## 'submit action' (doe)

The client submits the given `doe` to the server. If operation succeeds, a `init runs` message is emitted.

``` js
doe = { 
    mat: mat, // array (one element for each run) of array (one value for each input parameter)
    paramNames: paramNames // array of string (one name for each input parameter)
}
``` 

## 'get optim list' (data, fn)

The client wants to retrieve the list of optimizations already done.

- `data`: unused.
- `fn`: a callback function receiving the list of optimizations `optimInfoList` as argument.

``` js
optimInfoList = { 
    optimArgs: optimArgs, // optimization arguments (as described for 'start optim')
    iterations: iterations // an array (one for each iteration) of array of xValues (a structure '[xName]:xValue')
}
```

## 'get runs' (data, fn)

The client wants to retrieve runs, for example to reset its runs table.

- `data`: unused.
- `fn`: a callback function receiving `runs` as argument (an array of runs as described for `init runs`).

## 'add runs' (doe, fn)

The client submits the additional `doe` to the server.

- `doe`: additional runs.
- `fn`: a callback function receiving `runs` as argument (an array of runs as described for `init runs`).

``` js
doe = { 
    mat: mat, // array (one element for each run) of array (one value for each input parameter)
    paramNames: paramNames // array of string (one name for each input parameter)
}
``` 

## 'add simulator config' (simulatorConfig, fn)

The client wants to add the given simulator configuration.

- `simulatorConfig`: object describing the simulator configuration.
- `fn`: a callback function receiving `(success, data)` as argument (`success` is a boolean indicating if request has been fulfiled; `data` is a string describing a potential error).

``` js
simulatorConfig = { 
    id: id, // Unique identifier of this simulator configuration.
    config_name: config_name, // The name of the configuration.
    config_descr: config_descr, // A description of the configuration.
    cluster_size: cluster_size, // Is optional and indicates how many runs can run in parallel on this host.
    simulator_exe: simulator_exe, // References the simulator to use (reference for relative paths is the 'main' execution directory).
    argument: argument, // Provides a string which is sent as argument to the `simulator_exe` file.
    simulation_dir: simulation_dir, // The directory containing the input files that will be used by the simulator (reference for relative paths is the 'main' execution directory).
    simulation_param_files: simulation_param_files, // The list of input files that are "parameterized".
    simulation_data_files: simulation_data_files, // The list of input files that are "not parameterized".
    result_file_name: result_file_name, // Name of the file containing the results (a `csv` file to be found in the simulation directory).
    res_count: res_count // Number of columns to be found in `result_file_name`. Can be an array of numbers where each value is the number of columns to be found for a specific functional output. Both possibilities can be used by inserting them in an array.
}
```

## 'update simulator config' (simulatorConfig, fn)

The client wants to update a simulator configuration with the given data.

- `simulatorConfig`: object describing the simulator configuration (as described for `add simulator config`).
- `fn`: a callback function receiving `(success, data)` as argument (`success` is a boolean indicating if request has been fulfiled; `data` is a string describing a potential error).

## 'remove simulator config' (simulatorConfig, fn)

The client wants to remove a simulator configuration.

- `simulatorConfig`: object describing the simulator configuration (in this case, only the `id` field is used).
- `fn`: a callback function receiving `(success, data)` as argument (`success` is a boolean indicating if request has been fulfiled; `data` is a string describing a potential error).

## 'update storing dir' (storingDir, fn)

The client wants to set 'storingDir' (the directory where the input and output files of the simulations will be stored) to the given value. If operation succeeds, a `init runs` message is emitted.

- `storingDir`: string representation of the storing directory.
- `fn`: a callback function receiving `(success, data)` as argument (`success` is a boolean indicating if request has been fulfiled; `data` is a string describing a potential error).

## 'get launcher config' (data, fn)

The client wants to retrieve the list of the simulator configurations.

- `data`: unused.
- `fn`: a callback function receiving `launcherConfig` as argument (a structure described below; more info in [launcherConfigurationFile.md](./launcherConfigurationFile.md).

``` js
launcherConfig = {
    simulator_configs: simulator_configs; // Array of simulator configurations as described for `add simulator config`.
    storing_dir: storing_dir // String representation of the storing directory.
}
```

## 'search mustache inputs' (simulatorIds, fn)

The client wants to know the input variable names defined by mustaches in input files, for given simulator configurations.

- `simulatorIds`: array of simulator unique identifiers (`null` is allowed and means 'all simulators').
- `fn`: a callback function receiving `foundInputs` as argument (an object with a property for each element of `simulatorIds`, where `key` is the simulator id and `value` is an array of string containing the input variable names).

``` js
foundInputs = {
    0: ["p1", "p2", "p3"], // Three parameters "p1, p2 and p3" have been found for the simulator having id 0.
    1: ["par1","par2"] // Two parameters "par1 and par2" have been found for the simulator having id 1.
}
```

## 'import launcher config' (configAsText, fn)

The client wants to change the launcher configuration to the given value.

- `configAsText`: JSON string corresponding to the new content of the launcher configuration (more info in [launcherConfigurationFile.md](./launcherConfigurationFile.md)).
- `fn`: a callback function receiving `(success, data)` as argument (`success` is a boolean indicating if request has been fulfiled; `data` is a string describing a potential error).

## 'list files' (lsFile, fn)

The client wants to get the list of files contained in the given directory.

- `lsFile`: an object indicating the directory to read (described below).
- `fn`: a callback function receiving `(success, data)` as argument (`success` is a boolean indicating if request has been fulfiled; `data` is a string describing a potential error; otherwise, `data` is an object described below).

``` js
lsFile = {
    parent: filePath, // First segments of the file path
    child: fileName // Last segment of the file path (must be null if 'parent' is a file)
}
``` 

``` js
data = {
    parent: filePath, // A canonicalized absolute pathname which represents read directory (or simply 'lsFile' if it does not reference a directory).
    filesList: fileNames // The list of files contained int 'parent', or 'null' if 'lsFile' does not reference a directory.
}
``` 

## 'set run simulator' (setRunSimulatorArgs)

The client wants to attribute a simulator to given runs.

- `setRunSimulatorArgs`: specifies a simulator and the runs to assign.

``` js
setRunSimulatorArgs = { 
    runIds: number[], // Indexes of affected runs.
    simulatorId: number // An id referencing the simulator configuration to use for specified runs.
}
```

## 'run action' (runActionArg)

The client wants to execute the specified action on given runs.

- `runActionArg`: specifiy which action is to apply to which runs. 

``` js
runActionArg = { 
    runIds: number[], // Indexes of affected runs.
    actionId: string // Identifier of the action to perform.
}
```

Allowed values for `actionId`:
- `ConfigureLaunchLoad`: input files are processed (`configure` action), then simulator is called (`configure` action), then results are retrieved (`load` action);
- `Configure`: input files are processed;
- `Load`: results are retrieved;
- `Cancel`: selected `Waiting` runs are removed from the queue of 'runs to be executed';
- `EnDisable`: if status is `Ready`, `Ended` or `On Error`, set status to `Disabled`; if status is `Disabled`, set status to `Ready`.

## 'login password needed' (simulatorId, fn)

The client wants to know if the server knows login/password for a given simulator.

- `simulatorId`: unique identifier of a simulator configuration.
- `fn`: a callback function receiving `(success, data)` as argument (`success` is a boolean indicating if request has been fulfiled; `data` is a string describing a potential error; otherwise, `data` is a `boolean`, `true` indicating that login/password is unknown for the given simulator).

## 'add login password' (logPwdArgs, fn)

The client asks to the server to associate the given login/password to the given simulator (if the given login/password is validated).

- `logPwdArgs`: an object indicating the login/password and the associated simulator (described below).
- `fn`: a callback function receiving `(success, data)` as argument (`success` is a boolean indicating if request has been fulfiled; `data` is a string describing a potential error; otherwise, `data` is a `boolean`, `true` if login/password has been validated by the server).

``` js
logPwdArgs = {
    simulatorId: number, 
    loginPassword: {
        login: string,
        password: string
    }
}
``` 

## 'get optim descrs' (optimPbDef, fn)

The client wants to know which optimizers are comptible with the given optimization problem.

- `optimPbDef`: description of the optimization problem (described below).
- `fn`: a callback function receiving `(success, optimDescrs)` as argument (`success` is a boolean indicating if request has been fulfiled; `optimDescrs` is a string describing a potential error; otherwise, `optimDescrs` is a list of descriptions, more info in [Header_template.R](../optimizations/optimizers/Header_template.R)).

``` js
optimPbDef = {
    tags: {
        constraints: boolean, 
        categorical: boolean, 
        monoobj: boolean, 
        multiobj: boolean, 
        derivative: boolean
    }
}
``` 

## 'start optim' (optimArgs, fn)

The client wants to start an optimization process identified by a given unique id `optimId`.

- `optimArgs`: optimizer parameters values; since optimizers descriptions are retrieved dynamically, structure of optimizer parameters is free, only a `optimId` field is required (below is an overview of the structure used by Lagun).
- `fn`: a callback function receiving `(success, optimResults)` as argument (`success` is a boolean indicating if request has been fulfiled; `optimResults` is a string describing a potential error; otherwise, `optimResults` is free and depends of the optimization problem).

``` js
optimArgs = { 
    optimFileName: optimFileName,
    optimFuncName: optimFuncName, 
    optimFuncArgs: {
        PbDefinition: {
            x0: x0,
            lb: lb,
            ub: ub
        }, 
        ParametersValues: parametersValues, 
        AdvancedParametersValues: advParametersValues
    }, 
    optimId: optimId
}
``` 

``` js
optimResults = { 
    result: result,
    optimId: optimId
}
``` 

## 'stop optim' (optimId)

The client wants to stop the optimization process (which is identified by a given unique id `optimId`).

## 'tell Y' (tellYArgs)

The client wants to provide given Y values to the optimization process (which is identified by a given unique id `optimId`). Form of Y values depends of the optimization problem.

``` js
tellYArgs = { 
    y: yValues,
    optimId: optimId
}
``` 

# 'Server to clients' messages

## 'init runs' (runs)

The server notifies clients that a doe has been submitted.

- `runs`: an array of runs.

``` js
run = { 
    id: id, // Id of the run.
    status: status, // A status (cf. `launcher event` for description of the existing statuses).
    simulatorId: simulatorId, // An id referencing the simulator configuration to use for this run.
    paramNames: paramNames, // Array containing the name of each input parameter.
    paramValues: paramValues, // Array containing the value to use for each input parameter.
    result: result, // Meaning depends of the status: can be a value extracted from the location specified in the simulator configuration; can also be an error description.
    selected: selected // A boolean indicating if the state of this run is 'selected'.
}
```

## 'runs add' (runs)

The server notifies clients that runs have been added.

- `runs`: an array containing the added runs.

## 'simulator config add' (simulatorConfig)

The server notifies clients that a new simulator configuration has been added.

- `simulatorConfig`: object describing the simulator configuration (as described for `add simulator config`).

## 'simulator config update' (simulatorConfig)

The server notifies clients that a simulator configuration has been updated.

- `simulatorConfig`: object describing the simulator configuration (as described for `add simulator config`).

## 'simulator config remove' (simulatorConfig)

The server notifies clients that a simulator configuration has been removed.

- `simulatorConfig`: object describing the simulator configuration (in this case, only the `id` field is used).

## 'storing dir update' (storingDir)

The server notifies clients that 'storingDir' has changed.

- `storingDir`: string representation of the storing directory.

## 'launcher config import' (launcherConfig)

The server notifies clients that 'launcher configuration' has changed.

- `launcherConfig`: value of the new launcher configuration (more info in [launcherConfigurationFile.md](./launcherConfigurationFile.md)).

## 'run simulator set' (setRunSimulatorEvent)

The server notifies clients that the simulator has changed for runs specified by `setRunSimulatorEvent`.

``` js
setRunSimulatorEvent = { 
    runIds: number[], // Indexes of changed runs.
    simulatorId: simulatorId // An id referencing the simulator configuration to use for specified runs.
}
```

## 'launcher event' (event)

The server notifies clients that the status of a run has changed as described by `event`.

Possible values of `event`:

- `ready` event

``` js
event = { 
    id: id, // Id of a run.
    type: `ready` // The run has been submited successfully.
}
``` 

- `configuring` event

``` js
event = { 
    id: id, // Id of a run.
    type: `configuring`, // Input files with parameters are being processed).
}
``` 

- `configured` event

``` js
event = { 
    id: id, // Id of a run.
    type: `configured`, // The run is configured (input files with parameters have been processed).
}
``` 

- `waiting` event

``` js
event = { 
    ids: ids, // Array containing the id of some run
    type: `waiting` // The runs have been added to the queue of runs to be executed.
}
``` 

- `running` event

``` js
event = { 
    id: id, // Id of a run.
    type: `running` // The run is executing.
}
``` 

- `loading` event

``` js
event = { 
    id: id, // Id of a run.
    type: `loading`, // Output files result is being loaded).
}
``` 

- `ended` event

``` js
event = { 
    id: id, // Id of a run.
    type: `ended`, // The run is completed with no error detected.
    data: data // A value extracted from the location specified in the simulator configuration.
}
``` 

- `onerror` event

``` js
event = { 
    index: index, // Id of a run.
    type: `onerror`, // The run is completed or not, due to an error.
    data: error // A string describing the error.
}
``` 

## 'askY event' (askYArg)

The server notifies clients that an optimisation process (identified by a given unique id `optimId`) is waiting for the Y values corresponding to given X values.

``` js
askYArg = { 
    xTodo: xTodo,
    optimId: optimId
}
``` 

