# About communication

Back-end is written in `node.js`, and front-end communicates with back-end thanks to `socket.io` libray. Front-end can be `Lagun` itself, and in this case, communication between the `Shiny` server (which is in R) and the client part (which is in javascript) uses a technique presented in the article <https://shiny.rstudio.com/articles/communicating-with-js.html>.

# Using simulations launcher to run a design of experiments

```mermaid
sequenceDiagram
    autonumber

    participant R as R-Lagun
    participant J as JS-Lagun
    participant L as Simulations-Launcher

    rect rgba(23, 142, 255, .5)
        Note over R,L: Phase 1: Communication from Lagun to the simulations launcher
        R-->>R: React to user "submit" action
        R->>J: session$sendCustomMessage("submitAction", doe)
        J->>L: launcherSocket.emit("submit action", doe)
    end
```

```mermaid
sequenceDiagram
    autonumber

    participant R as R-Lagun
    participant J as JS-Lagun
    participant L as Simulations-Launcher

    rect rgba(23, 142, 255, .5)
        Note over R,L: Phase 2: Communication from the simulations launcher to Lagun
        L-->>L: React to end of run
        L->>J: io.emit("launcherEvent", endedEvent);
        J->>R: Shiny.setInputValue("launcherEvent", endedEvent)
    end
```

# Using simulations launcher to run an optimisation process

## Overview

```mermaid
sequenceDiagram
    autonumber

    participant R as R-Lagun
    participant J as JS-Lagun
    participant L as Simulations-Launcher

    rect rgba(23, 142, 255, .5)
        R-->>R: React to user "Launch Optim" action
        R->>J: session$sendCustomMessage("optimAction", optimArgs, optimId)
        J->>L: launcherSocket.emit("startOptim", optimArgs, optimId, resultFn)
        L-->L: Execute script 'optim.R(optimArgs, optimId)'
        Note over R,L: Loop: for each optim iteration, ask "xTodo" and tell "yResults" (described below)
        alt An error occured during Optim script execution
            L->>J: resultFn(optimScriptErr, optimId)
            J->>R: Shiny.setInputValue("optimResults", optimScriptErr, optimId)
        else No error occured
            L->>J: resultFn(optimResults, optimId)
            J->>R: Shiny.setInputValue("optimResults", optimResults, optimId)
        end
    end
```

## How 'optim.R' process retrieves 'yDone' from 'simulations launcher' ('askY' function)

```mermaid
sequenceDiagram
    autonumber

    participant R as Optim.R
    participant F as fileSystem

    rect rgba(23, 142, 255, .5)
        R->>F: writeFile("xTodo")
        R->>F: writeFile("askYLock")
        loop Every second
            R->>F: fileExists("askYLock")
            alt "askYLock" doesn't exist
                R->>F: removeFile("xTodo")
                R-->>R: return(null)
            end
            alt ellapsedTime > TIMEOUT
                R->>F: removeFile("xTodo")
                R->>F: removeFile("askYLock")
                R-->>R: return(null)
            end
            alt "yDone" exists
                R->>F: removeFile("askYLock")
                R->>F: readFile("yDone")
                R->>F: removeFile("yDone")
                R-->>R: return(parse(yDone))
            end
            R-->>R: increase 'ellapsedTime'
        end
    end
```

## How 'simulations launcher' retrieves 'xTodo' from 'optim.R process ('askX' function)

```mermaid
sequenceDiagram
    autonumber

    participant L as Simulations-Launcher
    participant F as fileSystem

    rect rgba(23, 142, 255, .5)
        L->>F: writeFile("askXLock")
        loop Every second
            L->>F: fileExists("askXLock")
            alt "askXLock" doesn't exist
                L-->>L: return(null)
            end
            alt ellapsedTime > TIMEOUT
                L->>F: removeFile("askXLock")
                L-->>L: return(null)
            end
            alt "xTodo" exists
                L->>F: removeFile("askXLock")
                L->>F: readFile("xTodo")
                L->>F: removeFile("xTodo")
                L-->>L: return(parse(xTodo))
            end
            L-->>L: increase 'ellapsedTime'
        end
    end
```

## Optimisation iteration - How simulations launcher retrieves Y values

```mermaid
sequenceDiagram
    autonumber

    participant R as R-Lagun
    participant J as JS-Lagun
    participant L as Simulations-Launcher

    rect rgba(23, 142, 255, .5)
        L-->>L: React to an optimisation 'askY'
        Note left of L: Simulations launcher requests Y values to Lagun
        L->>J: io.emit("askY event", xTodo, optimId);
        J->>R: Shiny.setInputValue("askYEvent", xTodo, optimId)
        Note over R,L: Y values comes from response surface models? From simulations?
        alt Y values come from response surface models
            R-->>R: use 'launcherEvent' to transmit rsm values
        else Y values come from simulations
            R->>J: session$sendCustomMessage("addRuns", args)
            J->>L: launcherSocket.emit("add runs", args.runs, callback)
            L->>J: callback(addedRuns)
            J->>L: launcherSocket.emit("set run simulator", addedRuns, args.simulatorId)
            J->>L: launcherSocket.emit("run action", addedRuns, "ConfigureLaunchLoad")
            L-->>L: React to end of run
            L->>J: io.emit("launcherEvent", endedEvent)
            J->>R: Shiny.setInputValue("launcherEvent", endedEvent)
        end
        Note over R,L: Lagun receives Y values via 'launcherEvent'
        R-->>R: React to 'launcherEvent', store result if it is about current iteration 'xTodo'
        alt all Y values of current iteration are available
            R->>J: session$sendCustomMessage("tellY", yValues, optimId)
            J->>L: launcherSocket.emit("tell Y", yValues, optimId)
        end
    end
```
