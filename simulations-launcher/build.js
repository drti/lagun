import { build } from "esbuild";

build({
  entryPoints: ["src/typescript/src/main.ts"],
  bundle: true,
  minify: false,
  outfile: "dist/main.js",
  platform: "node",
  packages: "external",
  format: "esm"
});

build({
  entryPoints: ["src/typescript/src/view/simulationsView.ts", "src/typescript/src/view/simulatorsView.ts", "src/typescript/src/view/runSetsView.ts"],
  bundle: true,
  minify: false,
  outdir: "src/web-files/js",
  platform: "browser",
  format: "esm"
});

build({
  entryPoints: ["src/typescript/src/indexForTests.ts"],
  bundle: true,
  minify: false,
  outfile: "dist/indexForTests.js",
  platform: "node",
  packages: "external",
  format: "esm"
});

build({
  entryPoints: ["src/typescript/src/simulationsHtmlwidget.ts"],
  bundle: true,
  minify: false,
  external: ["Shiny", "HTMLWidgets"],
  outfile: "htmlwidget/simulationsView/inst/htmlwidgets/simulationsView.js",
  platform: "node",
  format: "cjs",
});

build({
  entryPoints: ["src/typescript/src/simulatorsHtmlwidget.ts"],
  bundle: true,
  minify: false,
  external: ["Shiny", "HTMLWidgets"],
  outfile: "htmlwidget/simulatorsView/inst/htmlwidgets/simulatorsView.js",
  platform: "node",
  format: "cjs",
});