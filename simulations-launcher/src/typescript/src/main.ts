//
// This file is subject to the terms and conditions defined in
// the file 'LICENSE', which is part of this source code.
//

import { LauncherServer } from "./launcherServer.js";

// Determine port to use (default value is 3000)
let port = 3000;
if (process.argv.length > 2) {
    const arg1 = +process.argv[2];
    if (arg1 >=0 && arg1 < 65536) {
        port = arg1;
    }
}

// Start simulations launcher server
new LauncherServer(port);