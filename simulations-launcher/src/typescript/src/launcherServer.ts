//
// This file is subject to the terms and conditions defined in
// the file 'LICENSE', which is part of this source code.
//

// ******************************
// Requirements / Initializations 
// ******************************

// Retrieve SSH client and server modules
import { NodeSSH } from "node-ssh";

// File system module
import * as fs from "fs";

// Utilities for working with file and directory paths
import * as path from "path";
// Utilities primarily designed to support the needs of Node.js' own internal APIs (for example, provides the 'promisify' function)
import * as util from "util";
// Convert functions following the common error-first callback style, i.e. taking a (err, value) => ... callback as the last argument, and returns a version that returns promises.
const readFile = util.promisify(fs.readFile);

// Initialize the templating language 'nunjucks'
import { nunjucks } from "./server/template/templateEngine";

import { Logger } from "./common/logger.js";
import { SimulatorsConnection } from "./server/simulatorsConnection.js";
import { SimulationsConnection } from "./server/simulationsConnection.js";
import { RunsSet } from "./server/runs/runsSet.js";

// Types of message sent through sockets
import { SIMULATION_MESSAGE } from "./common/launcherConst.js";

import express from "express";
import * as socketio from "socket.io";
import * as http from "http";
import { PrecompileOptions } from "nunjucks";

import { fileURLToPath } from "url";

export type ConnectionConfig = {
    host: string,
    port: number,
    username: string,
    password: string
};

export type SimulatorConfig = {
    id: number,
    config_name: string,
    config_descr: string,
    host: string,
    port: number,
    running_dir: string
    simulator_exe: string,
    argument: string,
    cluster_size: number,
    vector_support: boolean | undefined,
    vector_size: number | undefined,
    simulation_dir: string,
    simulation_param_files: string[],
    simulation_data_files: string[],
    result_file_name: string,
    res_count: number | number[]
};

export type LauncherConfig = {
    storing_dir: string,
    simulator_configs: SimulatorConfig[]
};

export type ParamValue = string | number | (string | number)[];

export type Doe = {
    mat: ParamValue[][],
    paramNames: string[]
};

export type Run = {
    id: number,
    status: string,
    simulatorId: number,
    paramNames: string[],
    paramValues: ParamValue[],
    result: string | null,
    queueState?: string,
    actionId?: string
};

export type mustacheCallback = (_foundInputs: { [s: number]: string[]; }) => void;

export type IoResponseCallback = (_success: boolean, _data?: any) => void;

export type IoResponseCallback2 = (_data: any) => void;

export type LoginPassword = {
    login: string,
    password: string
};

export type LogPwdArgs = {
    simulatorId: number, 
    loginPassword: LoginPassword
}

export type OptimPbDef = {
    tags: {
        constraints: boolean, 
        categorical: boolean, 
        monoobj: boolean, 
        multiobj: boolean, 
        derivative: boolean
    }
}

export type OptimArgs = {
    optimId: string | number,
    workflowType: string,
    simulatorName: string,
    optimFileName: string,
    optimFuncName: string,
    optimFuncArgs: {
        PbDefinition: {
            nd: number,
            COformulation: unknown,
            inputflag: unknown,
            x0: unknown,
            lb_orig: unknown,
            ub_orig: unknown,
            lb: unknown,
            ub: unknown,
            levels: unknown,
            paramNames: string[],
            savepath: string
        }
    }
};

export type TellYArgs = {
    y: unknown,
    optimId: string | number
};

export type StoreBinFileArgs = {
    optimId: string | number,
    fileName: string,
    content: string,
};

class LauncherServer {

    /** 
     * Number indicating the version of the protocol.
     * @type {number} 
     **/
    static readonly PROTOCOL_VERSION: number = 2;

    /** 
     * Name of the file containing data defining the simulations launcher.
     * @type {string} 
     **/
    static readonly LAUNCHER_SERVER_JSON: string = "./launcherServer.json";

    logger: Logger = new Logger("simulations-launcher");

    /** 
     * Objects in charge of scheduling the execution of runs.
     **/ 
    runsSets: Map<string, RunsSet>;

    io: socketio.Server;

    /** 
     * Object containig data defining the simulations launcher.
     * @type {LauncherConfig} 
     **/ 
    launcherConfig: LauncherConfig;

    constructor(port: number) {
        // Initialize the launcher http server and supply it with an 'express' application
        const expressApp = express();
        const httpServer = http.createServer(expressApp);
        //const httpServer = require('https').createServer({key: fs.readFileSync('key.pem'), cert: fs.readFileSync('cert.pem')}, expressApp);

        // Initialize a new instance of 'socket.io' by passing the launcher server and specifying a namespace called 'launcherServer'
        this.io = new socketio.Server(httpServer);

        this.runsSets = new Map();
        
        if (fs.existsSync(LauncherServer.LAUNCHER_SERVER_JSON)) {
            try {
                const fileContent = fs.readFileSync(LauncherServer.LAUNCHER_SERVER_JSON, "utf8");
                this.launcherConfig = JSON.parse(fileContent);
            }
            catch (error) {
                this.logger.trace(error);
                this.launcherConfig = { storing_dir: "../runningDir", simulator_configs: [] };
            }
        }
        else {
            this.launcherConfig = { storing_dir: "../runningDir", simulator_configs: [] };
        }

        // Set an id to each simulator config
        this.launcherConfig.simulator_configs.forEach((c, i) => { c.id = i; });

        LauncherServer.defineExpressHandlers(expressApp);

        // Listen on the 'connection' event for incoming sockets
        const launcherServer = this;
        this.io.of("/simulators").on("connection", socket => new SimulatorsConnection(launcherServer, socket));
        this.io.of("/simulations").on("connection", socket => new SimulationsConnection(launcherServer, socket));

        // We make the http launcher server listen on given port
        httpServer.listen(port, function () {
            launcherServer.logger.info("listening on *:" + port);
        });
    }

    /**
     * Define Express route handlers.
     * @param {import('express').Application} expressApp 
     */
    // eslint-disable-next-line max-lines-per-function
    static defineExpressHandlers(expressApp: express.Application) {
        const __filename = fileURLToPath(import.meta.url);
        const __dirname = path.dirname(__filename);

        // expressApp.use(express.static('view'));

        expressApp
            // We define a route handler '/' (called when we hit our website home)
            .get("/", (_request, response) => {
                // Server response is 'view/launcherHome.html'
                response.sendFile(__dirname + "/view/launcherHome.html");
            })
            .get("/simulations", (_request, response) => {
                // Server response is 'view/simulations.html'
                response.sendFile(__dirname + "/view/simulations.html");
            })
            .get("/js/jquery-1.12.4.min.js", (_request, response) => {
                response.sendFile(__dirname + "/view/js/jquery-1.12.4.min.js");
            })
            .get("/js/bootstrap-3.3.7.min.js", (_request, response) => {
                response.sendFile(__dirname + "/view/js/bootstrap-3.3.7.min.js");
            })
            .get("/js/vue.global-3.4.38.min.js", (_request, response) => {
                response.sendFile(__dirname + "/view/js/vue.global-3.4.38.min.js");
            })
            .get("/js/filesaver.js", (_request, response) => {
                response.sendFile(__dirname + "/view/js/filesaver.js");
            })
            .get("/js/runSetsView.js", (_request, response) => {
                response.sendFile(__dirname + "/view/js/runSetsView.js");
            })
            .get("/js/simulationsView.js", (_request, response) => {
                response.sendFile(__dirname + "/view/js/simulationsView.js");
            })
            .get("/js/simulatorsView.js", (_request, response) => {
                response.sendFile(__dirname + "/view/js/simulatorsView.js");
            })
            .get("/css/bootstrap-3.3.7.min.css", (_request, response) => {
                response.sendFile(__dirname + "/view/css/bootstrap-3.3.7.min.css");
            })
            .get("/fonts/glyphicons-halflings-regular.ttf", (_request, response) => {
                response.sendFile(__dirname + "/view/fonts/glyphicons-halflings-regular.ttf");
            })
            .get("/fonts/glyphicons-halflings-regular.woff", (_request, response) => {
                response.sendFile(__dirname + "/view/fonts/glyphicons-halflings-regular.woff");
            })
            .get("/fonts/glyphicons-halflings-regular.woff2", (_request, response) => {
                response.sendFile(__dirname + "/view/fonts/glyphicons-halflings-regular.woff2");
            })
            .get("/css/launcherView.css", (_request, response) => {
                response.sendFile(__dirname + "/view/css/launcherView.css");
            })
            // SourceMaps for debugging
            .get("/maps/view/simulatorsView.js.map", (_request, response) => {
                response.sendFile(__dirname + "/maps/view/simulatorsView.js.map");
            })
            .get("/src/view/simulatorsView.ts", (_request, response) => {
                response.sendFile(__dirname + "/src/view/simulatorsView.ts");
            })
            .get("/maps/view/simulationsView.js.map", (_request, response) => {
                response.sendFile(__dirname + "/maps/view/simulationsView.js.map");
            })
            .get("/src/view/simulationsView.ts", (_request, response) => {
                response.sendFile(__dirname + "/src/view/simulationsView.ts");
            })
            // No more route handler available => Server response is a 404 error
            .use(function (_request, response, _next) {
                response.setHeader("Content-Type", "text/plain");
                response.status(404).send("Page not found!");
            });
    }

    // ************************
    // Action message callbacks
    // ************************

    /**
     * Used to determine the input variable names defined by mustaches in input files, for given simulator configurations.
     * @param {number[]} simulatorIdsOrEmpty Array of simulator unique identifiers (if empty, means 'all simulators').
     * @param {mustacheCallback} fn A 'mustacheCallback' alloying a client to retrieve the list of the simulator configurations.
     */
    async searchMustacheInputs(simulatorIdsOrEmpty: string[] | number[], fn: mustacheCallback) {
        if (typeof fn !== "function") {
            this.logger.error(`'searchMustacheInputs' received a 'fn' which is not a function: ${fn}`);
            return;
        }
        const simulatorIdsCheck = LauncherServer.checkSimulatorIdsOrEmpty("searchMustacheInputs", simulatorIdsOrEmpty);
        if (simulatorIdsCheck.length !== 0) {
            this.logger.error(simulatorIdsCheck);
            fn({});
            return;
        }
        /** @type {Object.<number, string[]>} */const foundInputs: { [n: number]: string[]; } = {};
        const simulatorIds = simulatorIdsOrEmpty.length === 0
            ? this.launcherConfig.simulator_configs.map(s => s.id)
            : simulatorIdsOrEmpty;
        for (let index = 0; index < simulatorIds.length; index++) {
            const simulatorId = +simulatorIds[index];
            const simulatorConfig = this.launcherConfig.simulator_configs.find(s => s.id === simulatorId);
            if (simulatorConfig) {
                foundInputs[simulatorId] = await LauncherServer.mustachesInParamFiles(simulatorConfig, this.logger);
            }
            else {
                this.logger.error(SIMULATION_MESSAGE.c2s.searchMustacheInputs + " received for simulator " + simulatorId + " but is unknown");
            }
        }
        fn(foundInputs);
    }

    private static checkSimulatorIdsOrEmpty(checkedMessage: string, simulatorIdsOrEmpty: string[] | number[]) {
        if (!Array.isArray(simulatorIdsOrEmpty)) {
            return `'${checkedMessage}' received a 'simulatorIdsOrEmpty' which is not an array`;
        }
        for (let i = 0; i < simulatorIdsOrEmpty.length; i++) {
            const simulatorIdType = typeof simulatorIdsOrEmpty[i];
            if (simulatorIdType !== "number" && simulatorIdType !== "string") {
                return `'${checkedMessage}': ${simulatorIdsOrEmpty} (element ${i} of 'simulatorIdsOrEmpty') is not valid`;
            }
        }
        return "";
    }

    /**
     * Pre-processes input files to retrieve parameter names.
     * @param {SimulatorConfig} simulatorConfig
     * @returns {Promise<string[]>} a promise with an error description if rejected, or an array containnig parameter names if fulfilled
     */
    static async mustachesInParamFiles(simulatorConfig: SimulatorConfig, logger: Logger): Promise<string[]> {
        const mustacheSet: Set<string> = new Set();
        for (const paramFileName of simulatorConfig.simulation_param_files) {
            try {
                await LauncherServer.mustachesInParamFile(simulatorConfig, paramFileName, mustacheSet);
            }
            catch (error) {
                logger.error("error processing: " + paramFileName);
                logger.error(SIMULATION_MESSAGE.c2s.searchMustacheInputs + " received for simulator " + simulatorConfig.id + " but failed. Cause: " + error);
                logger.trace(error);
            }
        }
        return [...mustacheSet];
    }

    /**
     * @param {SimulatorConfig} simulatorConfig
     * @param {string} paramFileName
     * @param {Set<string>} mustacheSet
     */
    static async mustachesInParamFile(simulatorConfig: SimulatorConfig, paramFileName: string, mustacheSet: Set<string>) {
        const fullParamFileName = path.join(simulatorConfig.simulation_dir, paramFileName);

        const fileContent = await readFile(fullParamFileName, "utf8");
        const opt = {
            name: "unused",
            wrapper: function (templates: { name: string, template: string }[], _opts: PrecompileOptions) {
                for (let i = 0; i < templates.length; i++) {
                    const template = templates[i].template;
                    const foundMatching = template.match(/context, frame, "\w+"/g);
                    if (foundMatching !== null) {
                        const matches = foundMatching.map(e => e.substring(17, e.length - 1));
                        matches.forEach(m => mustacheSet.add(m));
                        mustacheSet.delete("ln");
                        mustacheSet.delete("exp");
                    }
                }
                return "";
            }
        };
        // @ts-ignore
        nunjucks.precompileString(fileContent, opt);
    }

    /**
     * Tells if the server knows login/password for a given simulator.
     * @param {number} simulatorId 
     * @param {import('../launcherServer').IoResponseCallback} fn
     */
    async loginPasswordNeeded(simulatorId: number, fn: IoResponseCallback) {
        if (typeof fn !== "function") {
            this.logger.error(`'loginPasswordNeeded' received a 'fn' which is not a function: ${fn}`);
            return;
        }
        try {
            const simulatorConfig = await this.checkSimulatorConfig(simulatorId);
            if (simulatorConfig.host !== "localhost") {
                const loginPassword = RunsSet.simulator2loginPwdMap.get(simulatorId);
                if (!loginPassword) {
                    // loginPassword is needed
                    fn(true, true);
                    return;
                }
                const connConf = {
                    host: simulatorConfig.host,
                    port: simulatorConfig.port,
                    username: loginPassword.login,
                    password: loginPassword.password
                };
                const ssh = new NodeSSH();
                await ssh.connect(connConf);
                await ssh.requestSFTP();
                ssh.dispose();
            }
            // loginPassword is not needed
            fn(true, false);
        }
        catch (error) {
            fn(false, "" + error);
        }
    }
    
    /**
     * Associates the given login/password to the given simulator, if the given login/password is validated.
     * @param {import('../launcherServer').LogPwdArgs} logPwdArgs An object contanining information about simulator and login/password.
     * @param {import('../launcherServer').IoResponseCallback} fn
     */
    async addLoginPassword(logPwdArgs: LogPwdArgs, fn: IoResponseCallback) {
        // TODO
        if (typeof fn !== "function") {
            this.logger.error(`'addLoginPassword' received a 'fn' which is not a function: ${fn}`);
            return;
        }
        const logPwdArgsCheck = LauncherServer.checkLogPwdArgs(logPwdArgs);
        if (logPwdArgsCheck.length !== 0) {
            this.logger.error(logPwdArgsCheck);
            fn(false, logPwdArgsCheck);
            return;
        }
        try {
            const simulatorConfig = await this.checkSimulatorConfig(logPwdArgs.simulatorId);
            if (simulatorConfig.host !== "localhost") {
                const connConf = {
                    host: simulatorConfig.host,
                    port: simulatorConfig.port,
                    username: logPwdArgs.loginPassword.login,
                    password: logPwdArgs.loginPassword.password
                };
                const ssh = new NodeSSH();
                await ssh.connect(connConf);
                await ssh.requestSFTP();
                ssh.dispose();
                RunsSet.simulator2loginPwdMap.set(logPwdArgs.simulatorId, logPwdArgs.loginPassword);
            }
        }
        catch (error) {
            this.logger.error(error);
            fn(false, "" + error);
            return;
        }
        fn(true, true);
    }

    private static checkLogPwdArgs(logPwdArgs: LogPwdArgs) {
        const simulatorIdType = typeof logPwdArgs.simulatorId;
        if (simulatorIdType !== "number") {
            return `'checkLogPwdArgs' received a 'logPwdArgs' which has an invalid 'simulatorId': ${logPwdArgs.simulatorId}`;
        }
        if (!Object.prototype.hasOwnProperty.call(logPwdArgs, "loginPassword")) {
            return "'checkLogPwdArgs' received a 'logPwdArgs' which doesn't have an 'loginPassword' property";
        }
        if (typeof logPwdArgs.loginPassword.login !== "string") {
            return `'checkLogPwdArgs' received a 'logPwdArgs.loginPassword' which has an invalid 'login': ${logPwdArgs.loginPassword.login}`;
        }
        if (typeof logPwdArgs.loginPassword.password !== "string") {
            return "'checkLogPwdArgs' received a 'logPwdArgs.loginPassword' which has an invalid 'password'";
        }
        return "";
    }
    
    /**
     * Checks if the requested simulator conf id is known.
     * @param {number} simulatorConfId
     * @returns {Promise<import('../launcherServer').SimulatorConfig>} a promise with an error description if rejected, or a simulator conf if fulfilled
     */
    async checkSimulatorConfig(simulatorConfId: number): Promise<SimulatorConfig> {
        const simulatorConfig = this.launcherConfig.simulator_configs.find(conf => conf.id === simulatorConfId);
        if (!simulatorConfig) {
            throw new Error(simulatorConfId + " is an unknown simulator configuration"); // promise is rejected with an error message
        }
        return simulatorConfig;
    }

    /**
     * @param {string} configAsText JSON string corresponding to a content for the launcher configuration.
     */
    static parseLauncherConfigText(configAsText: string) {
        const conf = JSON.parse(configAsText);
        if (typeof conf.storing_dir === "undefined" || conf.storing_dir.length === 0) {
            throw new Error("'storing_dir' is missing");
        }
        if (typeof conf.simulator_configs === "undefined") {
            throw new Error("'simulator_configs' is missing");
        }

        conf.simulator_configs.forEach(LauncherServer.parseSimulatorConfigText);

        return conf as LauncherConfig;
    }

    /**
     * @param {SimulatorConfig} s
     * @param {number} i
     */
    static parseSimulatorConfigText(s: SimulatorConfig, i: number) {
        if (typeof s.config_name === "undefined" || s.config_name.length === 0) {
            throw new Error("'config_name' is missing for conf " + i);
        }
        if (typeof s.config_descr === "undefined") {
            throw new Error("'config_descr' is missing for conf " + i);
        }
        if (typeof s.simulator_exe === "undefined" || s.simulator_exe.length === 0) {
            throw new Error("'simulator_exe' is missing for conf " + i);
        }
        if (typeof s.host === "undefined") {
            throw new Error("'host' is missing for conf " + i);
        }
        if (typeof s.port === "undefined" && s.host !== "localhost") {
            throw new Error("'port' is missing for conf " + i);
        }
        if (typeof s.running_dir === "undefined" && s.host !== "localhost") {
            throw new Error("'running_dir' is missing for conf " + i);
        }
        if (typeof s.argument === "undefined") {
            throw new Error("'argument' is missing for conf " + i);
        }
        if (typeof s.simulation_dir === "undefined" || s.simulation_dir.length === 0) {
            throw new Error("'simulation_dir' is missing for conf " + i);
        }
        if (typeof s.simulation_param_files === "undefined") {
            throw new Error("'simulation_param_files' is missing for conf " + i);
        }
        if (typeof s.simulation_data_files === "undefined") {
            throw new Error("'simulation_data_files' is missing for conf " + i);
        }
        if (typeof s.res_count === "undefined") {
            throw new Error("'res_count' is missing for conf " + i);
        }
        if (typeof s.result_file_name === "undefined" || s.result_file_name.length === 0) {
            throw new Error("'result_file_name' is missing for conf " + i);
        }
    }
}

export {LauncherServer};
