// @ts-nocheck
import { SimulationsView } from "./view/simulationsView";

// eslint-disable-next-line no-undef
HTMLWidgets.widget({

  name: "simulationsView",

  type: "output",

  factory: function(el: HTMLElement, _width: number, _height: number) {

    // TODO: define shared variables for this instance

    const simulationsView = new SimulationsView(el.id);

    return {

      renderValue: function(config) {

        // TODO: code to render the widget, e.g.
        el.innerHTML = config.rawHtml;
        simulationsView.init(config);

      },

      resize: function(_newWidth: number, _newHeight: number) {

        // TODO: code to re-render the widget with a new size

      }

    };
  }
});