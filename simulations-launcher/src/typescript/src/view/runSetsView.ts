//
// This file is subject to the terms and conditions defined in
// the file 'LICENSE', which is part of this source code.
//

import { io, Socket } from "socket.io-client";

// @ts-ignore
const { createApp } = Vue;

type Config = {
    rawHtml: string,
    url: string,
    setId: string
}

export default interface RunSetsModel {
    runSets: string[];
}

export class RunSetsView {
    /**
     * Types of message sent through sockets.
     * @enum {string}
     **/
    static readonly SIMULATOR_MESSAGE = {
        c2s: {
            getRunSets: "get run sets"
        }
    };

    bindto: string;

    vm: RunSetsModel;

    simulatorsSocket: Socket | null = null;

    constructor(id: string) {
        this.bindto = "#" + id;
    }

    public id() {
        return this.bindto.substring(1);
    }

    /**
     * @param {Config=} config
     */
    init(config: Config | undefined) {
        let launcherUrl: string | undefined;
        if (config) {
            launcherUrl = config.url;
        }
        this.simulatorsSocket = RunSetsView.initSimulatorsSocket(launcherUrl);

        this.initVueModel();

        // Send a 'get runs' message to 'launcherSocket', providing a callback function, in order to retrieve runs description and update the runs table.
        this.simulatorsEmit(RunSetsView.SIMULATOR_MESSAGE.c2s.getRunSets, "", RunSetsView.prototype.initRunSetsTableModel.bind(this));
    }

    simulatorsEmit(event: string, ...args: any[]): Socket | null {
        if (this.simulatorsSocket) {
            this.simulatorsSocket.emit(event, ...args);
        }
        else {
            console.log("'RunSetsView.emit' error because 'simulators_io' is null");
        }
        return this.simulatorsSocket;
    }

    /**
     * Retrieve a socket instance connected to launcher server, specifying a namespace called 'simulators'.
     * @param {string=} launcherUrl
     * @returns {import("socket.io").Socket}
     */
    static initSimulatorsSocket(launcherUrl?: string): Socket {
        // prettier-ignore
        return typeof launcherUrl === "undefined"
            ? io("/simulators")
            : io(launcherUrl + "/simulators");
    }

    // ******************************
    // User Interface Initializations
    // ******************************

    initVueModel() {
        this.vm = createApp({
            data() {
                return {
                    runSets: []
                }
            }
        }).mount(this.bindto);
    }

    // *******************************
    // Socket.io callbacks definitions
    // *******************************

    /**
     * Reset run sets table model with given data.
     * @param {import('../launcherServer').Run[]} runSets A runSets description
     */
    initRunSetsTableModel(runSets: string[]) {
        // Update "vm.runSets"
        this.vm.runSets.splice(0);
        for (let setIndex = 0; setIndex < runSets.length; setIndex++) {
            const runSet = runSets[setIndex].toString();
            this.vm.runSets.push(runSet);
        }
    }

}
