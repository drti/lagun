//
// This file is subject to the terms and conditions defined in
// the file 'LICENSE', which is part of this source code.
//

import "./init";
import * as d3 from "d3";
import { IoResponseCallback, LauncherConfig, LogPwdArgs, Run, SimulatorConfig } from "../launcherServer";
import { LauncherEvent, SetRunSimulatorEvent } from "../server/runs/runsSet";
import { io, Socket } from "socket.io-client";

// @ts-ignore
const { createApp } = Vue;

type Config = {
    rawHtml: string,
    url: string,
    setId: string
}

// type Row = {
//     runId: number,
//     simulator: number,
//     status: string,
//     blockSize: number
// };
type Row = { [columnAttribute: string]: any; }

type Cell = {
    columnId: string,
    runId: number
}

export default interface SimulationsVueModel {
    runActions: number;
    simulators: SimulatorConfig[];
    rowsLength: number;
    logPwdDialog: string;
    config_name: string;
    config_descr: string;
    logPwdSimulator: SimulatorConfig | null;
    login: string | null;
    password: string | null;
}

export class SimulationsView {
    /**
     * Column labels of the runs table.
     * @enum {string}
     **/
    static readonly COLUMN_LABELS = {
        runId: "ID",
        status: "Status",
        simulator: "Simulator",
        result: "Result",
    };

    /**
     * Column types of the runs table.
     * @enum {string}
     **/
    static readonly COLUMN_IDS = {
        runId: "runId",
        param: "param",
        status: "status",
        simulator: "simulator",
        result: "result",
    };

    /**
     * Run status labels.
     * @enum {string}
     **/
    static readonly STATUS_LABEL = {
        ready: "Ready",
        configuring: "Configuring",
        configured: "Configured",
        waiting: "Waiting",
        running: "Running",
        loading: "Loading",
        ended: "Ended",
        onerror: "On error",
        disabled: "Disabled"
    };

    /**
     * Run statuses (also used as launcher event types).
     * @enum {string}
     **/
    static readonly STATUS = {
        ready: "ready",
        configuring: "configuring",
        configured: "configured",
        waiting: "waiting",
        running: "running",
        loading: "loading",
        ended: "ended",
        onerror: "onerror",
        disabled: "disabled"
    };
    static readonly MAX_VISIBLE_RESULTS = 100;

    /**
     * Types of simulator messages sent through sockets.
     * @enum {string}
     **/
    static readonly SIMULATOR_MESSAGE = {
        c2s: {
            getProtocolVersion: "get protocol version", 
            getLauncherConfig: "get launcher config",
            updateSimulatorConfig: "update simulator config"
        },
        s2c: {
            simulatorConfigAdd: "simulator config add",
            simulatorConfigUpdate: "simulator config update",
            simulatorConfigRemove: "simulator config remove"
        }
    };

    /**
     * Types of simulation messages sent through sockets.
     * @enum {string}
     **/
    static readonly SIMULATION_MESSAGE = {
        c2s: {
            attachSet: "attach set", 
            submitAction: "submit action",
            runAction: "run action",
            getRuns: "get runs",
            addRuns: "add runs",
            setRunSimulator: "set run simulator",
            loginPasswordNeeded: "login password needed",
            addLoginPassword: "add login password"
        },
        s2c: {
            initRuns: "init runs",
            runsAdd: "runs add",
            runSimulatorSet: "run simulator set",
            launcherEvent: "launcher event"
        }
    };

    /**
     * Launcher actions (used as argument of SIMULATION_MESSAGE.runAction).
     * @enum {string}
     **/
    static readonly RUN_ACTION = {
        configureLaunchLoad: "ConfigureLaunchLoad",
        configure: "Configure",
        load: "Load",
        cancel: "Cancel",
        enDisable: "EnDisable"
    };

    /**
     * Multi modal dialog type.
     * @enum {string}
     **/
    static readonly MULTI_MODAL_DIALOG = {
        LoginPassword: "LoginPassword"
    };

    static readonly NONBREAKING_SPACE = String.fromCharCode(0xA0);

    static readonly EXP_FORMATS: Record<string, string> = {
        "y": "-24",
        "z": "-21",
        "a": "-18",
        "f": "-15",
        "p": "-12",
        "n": "-9",
        "µ": "-6",
        "m": "-3",
        "k": "3",
        "M": "6",
        "G": "9",
        "T": "12",
        "P": "15",
        "E": "18",
        "Z": "21",
        "Y": "24"
    }

    static readonly f2s = d3.format("~s");

    static readonly f3f = d3.format("~r");

    static sToExp(siValue: string) {
        const siStr = /[yzafpnµmkMGTPEZY]/.exec(siValue)
        if (siStr !== null) {
            return siValue.replace(siStr[0], SimulationsView.NONBREAKING_SPACE + "E" + SimulationsView.EXP_FORMATS[siStr[0]]);
        }
        return siValue;
    }

    static format(value: number | { valueOf(): number; }) {
        if (value.valueOf() > 1e3 || value.valueOf() < -1e3 || (value.valueOf() < 1e-3 && value.valueOf() > -1e-3)) {
            return SimulationsView.sToExp(SimulationsView.f2s(value));
        }
        return SimulationsView.f3f(value);
    }

    // Index of last selected row (useful when Shift key is used)
    lastSelectedRow = -1;

    bindto: string;

    vm: SimulationsVueModel;

    resultColumnNames = ["Result 1"];

    simulationsSocket: Socket | null = null;

    simulatorsSocket: Socket | null = null;

    dispatch = d3.dispatch("loginPassword");

    paramNames: string[] = [];
    
    columnIds: string[] = [];

    runId2RowMap: Map<number, Row> = new Map();

    selectedRunIds: Set<number> = new Set();

    sortedRunIds: number[] = [];

    sortingColumnId: string | null = null;

    sortingFactor: number = 1;

    constructor(id: string) {
        this.bindto = "#" + id;
    }

    public id() {
        return this.bindto.substring(1);
    }

    /**
     * @param {Config=} config
     */
    init(config: Config | undefined) {
        let launcherUrl: string | undefined;
        let setId: string | undefined = "default";
        if (config) {
            launcherUrl = config.url;
            setId = config.setId;
        }
        this.simulationsSocket = SimulationsView.initSimulationsSocket(launcherUrl);
        this.simulatorsSocket = SimulationsView.initSimulatorsSocket(launcherUrl);

        // Send a 'attach set' message to the launcher server, providing a callback function, in order to finalize init.
        this.emit(
            SimulationsView.SIMULATION_MESSAGE.c2s.attachSet,
            setId, 
            SimulationsView.prototype.afterAttachSet.bind(this)
        );
    }

    private afterAttachSet() {
        this.initVueModel();

        // Send a 'get launcher config' message to the launcher server, providing a callback function, in order to retrieve simulators info.
        this.simulatorsEmit(
            SimulationsView.SIMULATOR_MESSAGE.c2s.getLauncherConfig,
            "", 
            SimulationsView.prototype.afterGetLauncherConfig.bind(this)
        );
    }

    private afterGetLauncherConfig(launcherConfig: LauncherConfig) {
        this.updateLauncherConfig(launcherConfig);

        // Send a 'get runs' message to 'launcherSocket', providing a callback function, in order to retrieve runs description and update the runs table.
        this.emit(SimulationsView.SIMULATION_MESSAGE.c2s.getRuns, "", SimulationsView.prototype.afterGetRuns.bind(this));
    }

    private afterGetRuns(runs: Run[]) {
        this.initRunsTableModel(runs);

        // React to messages received from Launcher server-side
        if (this.simulationsSocket && this.simulatorsSocket) {
            this.simulationsSocket.on(SimulationsView.SIMULATION_MESSAGE.s2c.initRuns, SimulationsView.prototype.initRunsTableModel.bind(this));
            this.simulationsSocket.on(SimulationsView.SIMULATION_MESSAGE.s2c.runsAdd, SimulationsView.prototype.runsAddOccured.bind(this));
            this.simulatorsSocket.on(SimulationsView.SIMULATOR_MESSAGE.s2c.simulatorConfigAdd, SimulationsView.prototype.addSimulatorConfigEvent.bind(this));
            this.simulatorsSocket.on(SimulationsView.SIMULATOR_MESSAGE.s2c.simulatorConfigUpdate, SimulationsView.prototype.updateSimulatorConfigEvent.bind(this));
            this.simulatorsSocket.on(SimulationsView.SIMULATOR_MESSAGE.s2c.simulatorConfigRemove, SimulationsView.prototype.removeSimulatorConfigEvent.bind(this));
            this.simulationsSocket.on(SimulationsView.SIMULATION_MESSAGE.s2c.runSimulatorSet, SimulationsView.prototype.setRunSimulatorEvent.bind(this));
            this.simulationsSocket.on(SimulationsView.SIMULATION_MESSAGE.s2c.launcherEvent, SimulationsView.prototype.handleLauncherEvent.bind(this));
        }
        else {
            console.log("'SimulationsView.finalizeInit' error because sockets are not initalized");
        }
    }

    emit(event: string, ...args: any[]): Socket | null {
        if (this.simulationsSocket) {
            this.simulationsSocket.emit(event, ...args);
        }
        else {
            console.log("'SimulationsView.emit' error because 'launcherSocket' is null");
        }
        return this.simulationsSocket;
    }

    simulatorsEmit(event: string, ...args: any[]): Socket | null {
        if (this.simulatorsSocket) {
            this.simulatorsSocket.emit(event, ...args);
        }
        else {
            console.log("'SimulationsView.emit' error because 'simulators_io' is null");
        }
        return this.simulatorsSocket;
    }

    /**
     * Retrieve a socket instance connected to launcher server, specifying a namespace called 'simulations'.
     * @param {string=} launcherUrl
     * @returns {import("socket.io").Socket}
     */
    static initSimulationsSocket(launcherUrl?: string) {
        // prettier-ignore
        return typeof launcherUrl === "undefined"
            ? io("/simulations")
            : io(launcherUrl + "/simulations");
    }

    /**
     * Retrieve a socket instance connected to launcher server, specifying a namespace called 'simulators'.
     * @param {string=} launcherUrl
     * @returns {import("socket.io").Socket}
     */
    static initSimulatorsSocket(launcherUrl?: string): Socket {
        // prettier-ignore
        return typeof launcherUrl === "undefined"
            ? io("/simulators")
            : io(launcherUrl + "/simulators");
    }

    // ******************************
    // User Interface Initializations
    // ******************************

    initVueModel() {
        this.vm = createApp({
            data() {
                return SimulationsView.vueData();
            },
            methods: this.vueMethods()
        }).mount(this.bindto);
    }

    // *************
    // Vue.js - Data
    // *************

    static vueData() {
        return {
            runActions: [
                {
                    id: SimulationsView.RUN_ACTION.configureLaunchLoad,
                    label: "Configure, Launch & Load",
                },
                { id: SimulationsView.RUN_ACTION.configure, label: "Configure" },
                { id: SimulationsView.RUN_ACTION.load, label: "Load" },
                { id: SimulationsView.RUN_ACTION.cancel, label: "Cancel" },
                { id: SimulationsView.RUN_ACTION.enDisable, label: "Enable/Disable" },
            ],
            /** @type {import('../launcherServer').SimulatorConfig[]} */
            simulators: [],
            /** @type {number} */
            rowsLength: 0,
            /** @type {MULTI_MODAL_DIALOG} */
            logPwdDialog: "",
            /** @type {?import('../launcherServer').SimulatorConfig} */
            logPwdSimulator: null,
            /** @type {string} */
            login: "",
            /** @type {string} */
            password: ""
        };
    }

    // ****************
    // Vue.js - Methods
    // ****************

    vueMethods() {
        return {
            assignSimulator: SimulationsView.prototype.assignSimulator.bind(this),
            executeRunAction: SimulationsView.prototype.executeRunAction.bind(this),
            openLogPwdDialog: SimulationsView.prototype.openLogPwdDialog.bind(this),
            closeLogPwdDialog: SimulationsView.prototype.closeLogPwdDialog.bind(this),
            submitLoginPassword: SimulationsView.prototype.submitLoginPassword.bind(this),
            cancelLoginPassword: SimulationsView.prototype.cancelLoginPassword.bind(this)
        };
    }

    /**
     * Callback for 'Assign a simulator' dropdown button.
     * @param {number} simulatorId
     **/
    assignSimulator(simulatorId: number) {
        this.emit(SimulationsView.SIMULATION_MESSAGE.c2s.setRunSimulator, {
            simulatorId: simulatorId,
            runIds: this.affectedRows().map(r => r.runId)
        });
    }

    private affectedRows() {
        const selectedRows = [...this.selectedRunIds.values()].map(runId => this.row(runId));
        return selectedRows.length === 0 ? [...this.runId2RowMap.values()] : selectedRows;
    }

    /**
     * Callback for 'Execute' dropdown button.
     * @param {string} actionId
     **/
    async executeRunAction(actionId: string) {
        let isDialogToClose = false;
        try {
            const affectedRows = this.affectedRows();
            const simulatorIds = new Set(affectedRows.map(r => r.simulator));
            // For each involved simulator: send a message 'loginPasswordNeeded' to know if a login/password is needed
            for (const simulatorId of simulatorIds) {
                const needed = await this.loginPasswordNeeded(simulatorId);
                if (!needed) {
                    continue;
                }
                // If password is needed, ask it via a dialog until validation or cancelling
                let loginPasswordOk = false;
                while (!loginPasswordOk) {
                    isDialogToClose = true;
                    loginPasswordOk = await this.askLoginPassword(simulatorId);
                    if (!loginPasswordOk) {
                        // 'Cancel' => Stop 'runAction' process
                        return;
                    }
                }
            }
            // Emit runAction
            this.emit(SimulationsView.SIMULATION_MESSAGE.c2s.runAction, {
                actionId: actionId,
                runIds: affectedRows.map(r => r.runId)
            });
        }
        finally {
            if (isDialogToClose) {
                this.closeLogPwdDialog();
            }
        }
    }

    /**
     * Sends a message to the launcher server to know if it has login/password for a given simulator.
     * @param simulatorId 
     * @returns {Promise<boolean>} a promise with an error description if rejected, or a boolean if fulfilled (indicating if login/password is known by server)
     */
    async loginPasswordNeeded(simulatorId: number): Promise<boolean> {
        const thisLauncherView = this;
        return new Promise<boolean>((resolve, reject) => {
            const ioRespCallback: IoResponseCallback = (success: boolean, data: any) => {
                if (success) {
                    resolve(data === true);
                }
                else {
                    reject(data);
                }
            };
            thisLauncherView.emit(SimulationsView.SIMULATION_MESSAGE.c2s.loginPasswordNeeded, simulatorId, ioRespCallback);
        });
    }

    /**
     * Opens a dialog to ask to user login/password for a given simulator.
     * @param simulatorId 
     * @returns {Promise<boolean>} a promise with an error description if rejected, or a boolean if fulfilled (indicating if login/password has been filled)
     */
    async askLoginPassword(simulatorId: number): Promise<boolean> {
        const simulatorConfig = this.vm.simulators.find(s => s.id === simulatorId);
        if (simulatorConfig) {
            this.vm.logPwdSimulator = simulatorConfig;
            this.openLogPwdDialog(SimulationsView.MULTI_MODAL_DIALOG.LoginPassword);
            return this.loginPasswordFilled();
        }
        throw new Error("askLoginPassword received an unknown simulatorId:" + simulatorId)
    }

    /**
     * Indicates if 'login/password' dialog has been filled with valid login/password.
     * @returns {Promise<boolean>} a promise with an error description if rejected, or a boolean if fulfilled (indicating if login/password has been filled)
     */
    async loginPasswordFilled() {
        const thisLauncherView = this;
        return new Promise<boolean>((resolve, reject) => {
            const ioRespCallback: IoResponseCallback = (success: boolean, data: any) => {
                if (success) {
                    resolve(data === true);
                }
                else {
                    reject(data);
                }
            };
            thisLauncherView.dispatch.on("loginPassword", ioRespCallback);
        });
    }

    /**
     * Callback for 'Ok' button from 'Login/Password' dialog.
     */
    async submitLoginPassword() {
        try {
            d3.select(this.bindto + " #lgPwdstatus").text("Checking login/password");
            const check = await this.checkLoginPassword();
            if (check) {
                this.dispatch.call("loginPassword", undefined, true, true);
                d3.select(this.bindto + " #lgPwdstatus").text("");
            }
            else {
                if (this.vm.logPwdSimulator === null) {
                    throw new Error("'submitLoginPassword' is called but 'logPwdSimulator' is not set");
                }
                d3.select(this.bindto + " #lgPwdstatus").text("Authentication failed!");
            }
        }
        catch (error) {
            console.log("submitLoginPassword error:" + error);
            d3.select(this.bindto + " #lgPwdstatus").text("Authentication failed!");
        }
    }

    /**
     * Callback for 'Cancel' button from 'Login/Password' dialog.
     */
    async cancelLoginPassword() {
        this.dispatch.call("loginPassword", undefined, true, false);
    }

    /**
     * Sends a message to the launcher server to associate the given login/password (if validated) to the given simulator.
     * @returns {Promise<boolean>} a promise with an error description if rejected, or a boolean if fulfilled (indicating if login/password has been validated by the launcher server)
     */
    async checkLoginPassword() {
        const thisLauncherView = this;
        return new Promise<boolean>((resolve, reject) => {
            if (thisLauncherView.vm.logPwdSimulator === null || thisLauncherView.vm.login === null || thisLauncherView.vm.password === null) {
                reject("'checkLoginPassword' is called but 'logPwdSimulator|login|password' is not set");
                return;
            }
            const logPwdArgs: LogPwdArgs = {
                simulatorId: thisLauncherView.vm.logPwdSimulator.id, 
                loginPassword: {
                    login: thisLauncherView.vm.login, 
                    password: thisLauncherView.vm.password
                }
            };
            thisLauncherView.emit(SimulationsView.SIMULATION_MESSAGE.c2s.addLoginPassword, logPwdArgs, (success: boolean, data: any) => {
                if (success) {
                    resolve(data === true);
                }
                else {
                    reject(data);
                }
            });
        });
    }

    /**
     * @param {string} columnId A column identifier
     * @returns {string} the text to use in the runs table header for the given column
     */
    headerContent(columnId: string) {
        let headerContent;
        // if column identifier starts with 'param', extract the parameter index in order to return the parameter name
        if (columnId.startsWith(SimulationsView.COLUMN_IDS.param)) {
            const paramIndex = +columnId.substring(SimulationsView.COLUMN_IDS.param.length);
            headerContent = this.paramNames[paramIndex];
        }
        // if column identifier starts with 'result', extract the result index in order to return the result name
        else if (columnId.startsWith(SimulationsView.COLUMN_IDS.result)) {
            const resultIndex = +columnId.substring(SimulationsView.COLUMN_IDS.result.length);
            headerContent = this.resultColumnNames[resultIndex];
        } else if (Object.keys(SimulationsView.COLUMN_LABELS).includes(columnId)) {
            headerContent = SimulationsView.COLUMN_LABELS[columnId as keyof typeof SimulationsView.COLUMN_LABELS];
        } else {
            console.log(
                "'headerContent' received an invalid 'column':",
                columnId
            );
            headerContent = "";
        }
        if (columnId === this.sortingColumnId) {
            headerContent = (this.sortingFactor === 1 ? "↑ " : "↓ ") + headerContent;
        }
        return headerContent;
    }

    private updateSortedRunIds() {
        const sortingColumnId = this.sortingColumnId;
        const sortedRows = [...this.runId2RowMap.values()];
        if (sortingColumnId === null || sortingColumnId === SimulationsView.COLUMN_IDS.runId) {
            sortedRows.sort((row1, row2) => {
                return this.sortingFactor * (row1.runId - row2.runId);
            });
        }
        else if (sortingColumnId === SimulationsView.COLUMN_IDS.status) {
            sortedRows.sort((row1, row2) => {
                return this.sortingFactor * (SimulationsView.statusLabel(row1[SimulationsView.COLUMN_IDS.status]) < SimulationsView.statusLabel(row2[SimulationsView.COLUMN_IDS.status]) ? -1 : 1);
            });
        }
        else if (sortingColumnId === SimulationsView.COLUMN_IDS.simulator) {
            sortedRows.sort((row1, row2) => {
                return this.sortingFactor * (this.simulatorLabel(row1[SimulationsView.COLUMN_IDS.simulator]) < this.simulatorLabel(row2[SimulationsView.COLUMN_IDS.simulator]) ? -1 : 1);
            });
        }
        else if (sortingColumnId.startsWith(SimulationsView.COLUMN_IDS.result) || sortingColumnId.startsWith(SimulationsView.COLUMN_IDS.param)) {
            sortedRows.sort((row1, row2) => {
                let value1 = +row1[sortingColumnId];
                value1 = isNaN(value1) ? row1[sortingColumnId] : value1;
                let value2 = row2[sortingColumnId];
                value2 = isNaN(value2) ? row2[sortingColumnId] : value2;
                return this.sortingFactor * (value1 < value2 ? -1 : 1);
            });
        }
        this.sortedRunIds = sortedRows.map(row => row.runId);
    }

    /**
     * @param {string} columnId A column identifier
     * @param {number} runId id of a run
     * @returns {string|number} the text to use in the runs table for the cell referenced by the arguments (column, rowIndex).
     */
    cellContent(columnId: string, runId: number): string | number {
        const row = this.row(runId);
        const value = row[columnId];
        if (columnId === SimulationsView.COLUMN_IDS.runId) {
            return value + 1;
        }
        if (columnId === SimulationsView.COLUMN_IDS.status) {
            return SimulationsView.statusLabel(value);
        }
        if (columnId === SimulationsView.COLUMN_IDS.simulator) {
            return this.simulatorLabel(value);
        }
        if (columnId.startsWith(SimulationsView.COLUMN_IDS.result)) {
            return SimulationsView.resultLabel(columnId, row);
        }
        if (columnId.startsWith(SimulationsView.COLUMN_IDS.param)) {
            return SimulationsView.formatValue(value);
        }
        throw new Error("Unexpected columnId " + columnId);
    }

    private static statusLabel(value: string) {
        if (
            typeof value === "string" &&
            Object.keys(SimulationsView.STATUS_LABEL).includes(value)
        ) {
            return SimulationsView.STATUS_LABEL[value as keyof typeof SimulationsView.STATUS_LABEL];
        } else {
            console.log("'cellContent' with an invalid status:", value);
            return "";
        }
    }

    private simulatorLabel(value: number) {
        const simulatorConfig = this.vm.simulators.find(s => s.id === value);
        return simulatorConfig ? simulatorConfig.config_name : "";
    }

    private static resultLabel(columnId: string, row: Row) {
        const value = row[columnId];
        if (row[SimulationsView.COLUMN_IDS.status] === SimulationsView.STATUS.onerror) {
            const resultIndex = +columnId.substring(SimulationsView.COLUMN_IDS.result.length);
            return (resultIndex === 0) ? JSON.stringify(value) : "";
        }
        return SimulationsView.formatValue(value);
    }

    private static formatValue(value: any) {
        if (typeof value === "undefined") {
            return "";
        }
        if (Array.isArray(value)) {
            return value.map(function(e): string {
                return SimulationsView.formatValue(e);
            }).join("\n");
        }
        const valueAsNumber = +value;
        return isNaN(valueAsNumber) ? value : SimulationsView.format(valueAsNumber);
    }

    private fillRowResults(row: Row, data?: any) {
        let results: string[] | string[][] = [""];
        let resultNames: string[] = ["Result 1"];
        if (typeof data === "string") {
            const dsv = d3.dsvFormat(";");
            const parsedRows = dsv.parseRows(data) as string[][];
            if (parsedRows.length === 0) {
                console.log("'csvFileContent' has no row");
                results = ["NaN"];
            }
            else {
                const resultsBlockByRwo = parsedRows.slice(parsedRows.length - row.blockSize, parsedRows.length);
                results = resultsBlockByRwo[0].map((_value, colIndex) => resultsBlockByRwo.map(blockRow => blockRow[colIndex]));
                resultNames = (parsedRows.length - row.blockSize === 1) 
                    ? parsedRows[0]
                    : parsedRows[0].map((_value, i) => "Result " + (i + 1));
                if (resultNames.length > SimulationsView.MAX_VISIBLE_RESULTS) {
                    resultNames = resultNames.slice(0, SimulationsView.MAX_VISIBLE_RESULTS);
                }
            }
        } else {
            console.log(
                "'cellContent' expected a string but found:",
                data
            );
            results = ["" + data];
        }
        for (
            let resultIndex = 0;
            resultIndex < results.length;
            resultIndex++
        ) {
            row[SimulationsView.COLUMN_IDS.result + resultIndex] = results[resultIndex];
        }

        if (resultNames.length > this.resultColumnNames.length) {
            this.resultColumnNames = resultNames;
        }
    }

    /**
     * Callback for 'Simulations Table Panel', when a table column header of content part is clicked.
     * @param {string} columnId
     * @param {{ ctrlKey: boolean; shiftKey: boolean; }} _event
     */
    clickTh(
        columnId: string,
        _event: MouseEvent
    ) {
        if (this.sortingColumnId === null || this.sortingColumnId === columnId) {
            this.sortingFactor = -this.sortingFactor;
        }
        else {
            this.sortingFactor = 1;
        }
        this.sortingColumnId = columnId;
        this.updateSortedRunIds();

        const trHead = d3.select(this.bindto + " #simulationsTable thead tr");
        trHead.selectAll("th").remove();
        this.updateRunsTableHeader();

        const tbody = d3.select(this.bindto + " #simulationsTable tbody");
        tbody.selectAll<HTMLTableRowElement, number>("tr").remove();
        this.addNewRunsToTableBody();
    }

    private row(runId: number) {
        const row = this.runId2RowMap.get(runId);
        if (!row) {
            throw new Error("Unexpected runId " + runId);
        }
        return row;
    }

    /**
     * Callback for 'Simulations Table Panel', when a row of content part is clicked.
     * @param {number} rowIndex
     * @param {{ ctrlKey: boolean; shiftKey: boolean; }} event
     */
    clickTr(
        runId: number,
        event: MouseEvent
    ) {
        const thisView = this;
        const rowIndex = this.sortedRunIds.indexOf(runId);
        if (rowIndex === -1 ) {
            console.log("Unknown runId: ", runId);
            return;
        }
        if (event.ctrlKey && event.shiftKey && this.lastSelectedRow !== -1) {
            const range =
                rowIndex < this.lastSelectedRow
                    ? d3.range(rowIndex, this.lastSelectedRow + 1)
                    : d3.range(this.lastSelectedRow, rowIndex + 1);
                this.changeSelection(range, true);
                this.lastSelectedRow = rowIndex;
        } else if (event.shiftKey && this.lastSelectedRow !== -1) {
            const range =
                rowIndex < this.lastSelectedRow
                    ? d3.range(rowIndex, this.lastSelectedRow + 1)
                    : d3.range(this.lastSelectedRow, rowIndex + 1);
            this.clearSelection();
            this.changeSelection(range, true);
            this.lastSelectedRow = rowIndex;
        } else if (event.ctrlKey) {
            if (this.selectedRunIds.has(runId)) {
                this.lastSelectedRow = -1;
                this.changeSelection([rowIndex], false);
            } else {
                this.lastSelectedRow = rowIndex;
                this.changeSelection([rowIndex], true);
            }
        } else {
            this.lastSelectedRow = rowIndex;
            this.clearSelection();
            this.changeSelection([rowIndex], true);
        }

        d3.select(this.bindto + " #simulationsTable tbody").selectAll<HTMLTableRowElement, number>("tr").each(function(aRunId) {
            d3.select(this).classed("selected", thisView.selectedRunIds.has(aRunId))
        });
    }

    /**
     * @param {MULTI_MODAL_DIALOG} logPwdDialog
     **/
    openLogPwdDialog(logPwdDialog: string) {
        if (Object.keys(SimulationsView.MULTI_MODAL_DIALOG).some(d => d === logPwdDialog)) {
            // @ts-ignore
            $(this.bindto + " #LogPwdDialog").modal("show");
            this.vm.logPwdDialog = logPwdDialog;
        } else {
            console.log(
                "'openLogPwdDialog' received '" +
                logPwdDialog +
                    "' which is unknown"
            );
        }
    }

    closeLogPwdDialog() {
        switch (this.vm.logPwdDialog) {
            case SimulationsView.MULTI_MODAL_DIALOG.LoginPassword:
                // @ts-ignore
                $(this.bindto + " #LogPwdDialog").modal("hide");
                break;
            default:
                console.log(
                    "'closeLogPwdDialog' received '" +
                        this.vm.logPwdDialog +
                        "' which is unknown"
                );
                break;
        }
    }

    // *******************************
    // Socket.io callbacks definitions
    // *******************************

    /**
     * @param {import('../launcherServer').SimulatorConfig} simulatorConfig
     */
    addSimulatorConfigEvent(simulatorConfig: SimulatorConfig) {
        this.vm.simulators.push(simulatorConfig);
    }

    /**
     * @param {import('../launcherServer').SimulatorConfig} simulatorConfig
     */
    updateSimulatorConfigEvent(simulatorConfig: SimulatorConfig) {
        const index = this.vm.simulators.findIndex(s => s.id === simulatorConfig.id);
        if (index === -1) {
            console.log(
                SimulationsView.SIMULATOR_MESSAGE.s2c.simulatorConfigUpdate +
                    " received but ignored because " +
                    simulatorConfig.id +
                    " is an unknown id"
            );
        } else {
            this.vm.simulators[index] = simulatorConfig;
        }
    }

    /**
     * @param {import('../launcherServer').SimulatorConfig} simulatorConfig
     */
    removeSimulatorConfigEvent(simulatorConfig: SimulatorConfig) {
        const index = this.vm.simulators.findIndex(s => s.id === simulatorConfig.id);
        if (index === -1) {
            console.log(
                SimulationsView.SIMULATOR_MESSAGE.s2c.simulatorConfigRemove +
                    " received but ignored because " +
                    simulatorConfig.id +
                    " is an unknown id"
            );
        } else {
            this.vm.simulators.splice(index, 1);
        }
    }

    /**
     * @param {import('../launcherServer').LauncherConfig} launcherConfig
     */
    updateLauncherConfig(launcherConfig: LauncherConfig) {
        this.vm.simulators.splice(0);
        Array.prototype.push.apply(
            this.vm.simulators,
            launcherConfig.simulator_configs
        );
    }

    /**
     * Handle 'setRunSimulatorEvent'
     * @param {SetRunSimulatorEvent} setRunSimulatorEvent identifies a simulator and the assigned runs.
     */
    setRunSimulatorEvent(setSimulatorEvent: SetRunSimulatorEvent) {
        const runIdSet = new Set(setSimulatorEvent.runIds);
        this.runId2RowMap.forEach(row => {
            if (runIdSet.has(row.runId)) {
                row.simulator = setSimulatorEvent.simulatorId;
            }
        });
        const simulatorLabel = this.simulatorLabel(setSimulatorEvent.simulatorId);
        d3.select(this.bindto + " #simulationsTable tbody").selectAll<HTMLTableRowElement, number>("tr")
            .filter(runId => runIdSet.has(runId)).each(function() {
                d3.select(this).selectAll<HTMLTableRowElement, Cell>("td")
                    .filter(cell => cell.columnId === SimulationsView.COLUMN_IDS.simulator)
                    .text(simulatorLabel);
            });
    }

    /**
     * Reset runs table model with given data.
     * @param {import('../launcherServer').Run[]} runs A runs description
     */
    initRunsTableModel(runs: Run[]) {
        this.resultColumnNames = ["Result 1"];

        this.paramNames.splice(0);

        // Update "rows" and "columns"
        this.runId2RowMap.clear();
        this.runsAddOccured(runs);
    }

    private resetColumnIds() {
        this.columnIds.splice(0);
        Array.prototype.push.apply(this.columnIds, [
            SimulationsView.COLUMN_IDS.runId,
            SimulationsView.COLUMN_IDS.status,
            SimulationsView.COLUMN_IDS.simulator,
        ]);
        Array.prototype.push.apply(
            this.columnIds,
            this.paramNames.map((_name, i) => SimulationsView.COLUMN_IDS.param + i)
        );
        Array.prototype.push.apply(
            this.columnIds,
            d3.range(this.resultColumnNames.length).map(i => SimulationsView.COLUMN_IDS.result + i)
        );
    }

    runsAddOccured(runs: Run[]) {
        if (this.paramNames.length === 0 && runs.length !== 0) {
            Array.prototype.push.apply(this.paramNames, runs[0].paramNames);
        }

        this.updateRows(runs);
        this.resetColumnIds();
        this.updateRunsTableHeader();
        this.addNewRunsToTableBody();
    }
    /**
     * Update runs table model with given data.
     * @param {import('../launcherServer').Run[]} runs A runs description
     */
    updateRows(runs: Run[]) {
        for (let runIndex = 0; runIndex < runs.length; runIndex++) {
            const run = runs[runIndex];
            const row = {
                runId: +run.id,
                simulator: run.simulatorId,
                status: run.status,
                blockSize: Array.isArray(run.paramValues[0]) ? run.paramValues[0].length : 1
            } as Row;

            if (run.status === SimulationsView.STATUS.ended) {
                this.fillRowResults(row, run.result);
            }
            if (run.status === SimulationsView.STATUS.onerror) {
                row[SimulationsView.COLUMN_IDS.result + 0] = JSON.stringify(run.result);
            }
            
            for (
                let paramIndex = 0;
                paramIndex < run.paramNames.length;
                paramIndex++
            ) {
                row[SimulationsView.COLUMN_IDS.param + paramIndex] = run.paramValues[paramIndex];
            }
            this.runId2RowMap.set(row.runId, row);
            this.sortedRunIds.push(row.runId);
        }
        this.vm.rowsLength = this.runId2RowMap.size;
    }

    private updateRunsTableHeader() {
        const trHead = d3.select(this.bindto + " #simulationsTable thead tr");
        
        trHead.selectAll("th").data(this.columnIds)
            .join(
                enter => enter.append("th"),
                update => update,
                exit => exit.remove()
            )
            .text(columnId => this.headerContent(columnId))
            .on("click", (event, columnId) => {
                this.clickTh(columnId, event);
            })
    }

    private addNewRunsToTableBody() {
        const tbody = d3.select(this.bindto + " #simulationsTable tbody");
        
        tbody.selectAll<HTMLTableRowElement, number>("tr").data(this.sortedRunIds)
            .enter()
            .append("tr")
            .classed("selected", runId => this.selectedRunIds.has(runId))
            .on("click", (event, runId) => {
                this.clickTr(runId, event);
            })
            .selectAll<HTMLTableCellElement, Cell>("td").data(runId => {
                return this.columnIds.map(columnId => {
                    return { columnId: columnId, runId: runId }
                });
            })
            .enter()
            .append("td")
            .text(cell => this.cellContent(cell.columnId, cell.runId));
    }

    /**
     * Handle 'launcher event' received from Launcher server-side
     * @param {import('../set/runsSet').LauncherEvent} launcherEvent The launcher event to handle
     */
    handleLauncherEvent(launcherEvent: LauncherEvent) {
        switch (launcherEvent.type) {
            case SimulationsView.STATUS.configuring:
            case SimulationsView.STATUS.configured:
            case SimulationsView.STATUS.running:
            case SimulationsView.STATUS.loading:
            case SimulationsView.STATUS.ready:
            case SimulationsView.STATUS.disabled:
                this.handleCommonEvent(launcherEvent);
                break;

            case SimulationsView.STATUS.waiting:
                this.handleWaitingEvent(launcherEvent);
                break;

            case SimulationsView.STATUS.ended:
                this.handleEndedEvent(launcherEvent);
                break;

            case SimulationsView.STATUS.onerror:
                this.handleOnerrorEvent(launcherEvent);
                break;

            default:
                console.log("Unknown event type:" + launcherEvent.type);
                break;
        }
    }

    /**
     * @param {import('../set/runsSet').LauncherEvent} launcherEvent
     */
    handleCommonEvent(launcherEvent: LauncherEvent) {
        if (typeof launcherEvent.id === "undefined") {
            console.log(
                "'handleCommonEvent' is called but 'launcherEvent.id' is not set"
            );
            return;
        }
        this.row(launcherEvent.id).status = launcherEvent.type;
        
        this.updateStatusTableCell(launcherEvent);
    }

    /**
     * @param {import('../set/runsSet').LauncherEvent} launcherEvent
     */
    handleWaitingEvent(launcherEvent: LauncherEvent) {
        if (typeof launcherEvent.ids !== "undefined") {
            launcherEvent.ids.forEach((id: number) => {
                this.row(id).status = launcherEvent.type;
            });
        
            this.updateStatusTableCell(launcherEvent);
        }
    }

    private updateStatusTableCell(launcherEvent: LauncherEvent) {
        let rowSelector: d3.ValueFn<HTMLTableRowElement, number, boolean>;
        if (launcherEvent.type === SimulationsView.STATUS.waiting) {
            const runIdSet = new Set(launcherEvent.ids);
            rowSelector = runId => runIdSet.has(runId);
        }
        else {
            rowSelector = runId => runId === launcherEvent.id;
        }
        d3.select(this.bindto + " #simulationsTable tbody").selectAll<HTMLTableRowElement, number>("tr")
            .filter(rowSelector).each(function() {
                d3.select(this).selectAll<HTMLTableCellElement, Cell>("td")
                    .filter(cell => cell.columnId === SimulationsView.COLUMN_IDS.status)
                    .text(SimulationsView.statusLabel(launcherEvent.type));
            });
    }

    /**
     * @param {import('../set/runsSet').LauncherEvent} launcherEvent
     */
    handleEndedEvent(launcherEvent: LauncherEvent) {
        if (typeof launcherEvent.id === "undefined") {
            console.log(
                "'handleEndedEvent' is called but 'launcherEvent.id' is not set"
            );
            return;
        }
        const row = this.row(launcherEvent.id);
        row.status = launcherEvent.type;

        this.fillRowResults(row, launcherEvent.data);

        // if this event implies more results, reset the runs table
        if (this.columnIds.length < 3 + this.paramNames.length + this.resultColumnNames.length) {
            this.resetColumnIds();
            this.updateRunsTableHeader();
            d3.select(this.bindto + " #simulationsTable tbody").selectAll<HTMLTableRowElement, number>("tr").remove();
            this.addNewRunsToTableBody();
        }
        else {
            this.updateRunsTableRow(launcherEvent);
        }
    }

    /**
     * @param {import('../set/runsSet').LauncherEvent} launcherEvent
     */
    handleOnerrorEvent(launcherEvent: LauncherEvent) {
        if (typeof launcherEvent.id === "undefined") {
            console.log(
                "'handleEndedEvent' is called but 'launcherEvent.id' is not set"
            );
            return;
        }
        const row = this.row(launcherEvent.id);
        row.status = launcherEvent.type;
        row[SimulationsView.COLUMN_IDS.result + 0] = JSON.stringify(launcherEvent.data);

        this.updateRunsTableRow(launcherEvent);
    }

    private updateRunsTableRow(launcherEvent: LauncherEvent) {
        const thisView = this;
        d3.select(this.bindto + " #simulationsTable tbody").selectAll<HTMLTableRowElement, number>("tr")
            .filter(runId => runId === launcherEvent.id).each(function(runId) {
                d3.select(this).selectAll<HTMLTableRowElement, Cell>("td").each(function(cell) {
                    if (cell.columnId === SimulationsView.COLUMN_IDS.status) {
                        d3.select(this).text(SimulationsView.statusLabel(launcherEvent.type));
                    }
                    else if (cell.columnId.startsWith(SimulationsView.COLUMN_IDS.result)) {
                        d3.select(this).text(SimulationsView.resultLabel(cell.columnId, thisView.row(runId)));
                    }
                });
            });
    }

    /**
     * @param {number[]} rowIndexes
     * @param {boolean} selected
     */
    changeSelection(rowIndexes: number[], selected: boolean) {
        for (const rowIndex of rowIndexes) {
            if (selected) {
                this.selectedRunIds.add(this.sortedRunIds[rowIndex]);
            }
            else {
                this.selectedRunIds.delete(this.sortedRunIds[rowIndex]);
            }
        }
    }

    clearSelection() {
        this.selectedRunIds.clear();
    }
}
