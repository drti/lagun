//
// This file is subject to the terms and conditions defined in
// the file 'LICENSE', which is part of this source code.
//

import "./init";
import * as d3 from "d3";
import { LauncherConfig, SimulatorConfig } from "../launcherServer";
import { io, Socket } from "socket.io-client";

// @ts-ignore
const { createApp } = Vue;

type Config = {
    rawHtml: string,
    url: string,
    setId: string
}

export default interface SimulatorsVueModel {
    protocolVersion: number,
    simulators: SimulatorConfig[];
    multiModalDialog: string;
    shownSimulator: SimulatorConfig | null;
    confsToImport: string | null;
    storingDir: string;
    editedStoringDir: string;
    editedDir: string;
    editedChildren: string[];
    editedHostName: string;
    editedPort: number;
    editedRunningDir: string;
    simulator_exe: string;
    argument: string;
    cluster_size: number;
    simulation_dir: string;
    simulation_param_files: string;
    simulation_data_files: string;
    result_file_name: string;
    res_count: number | number[];
}

export class SimulatorsView {
    /**
     * Types of simulator messages sent through sockets.
     * @enum {string}
     **/
    static readonly SIMULATOR_MESSAGE = {
        c2s: {
            getProtocolVersion: "get protocol version", 
            getLauncherConfig: "get launcher config", 
            updateStoringDir: "update storing dir",
            addSimulatorConfig: "add simulator config",
            updateSimulatorConfig: "update simulator config",
            removeSimulatorConfig: "remove simulator config",
            listFiles: "list files",
            importLauncherConfig: "import launcher config"
        },
        s2c: {
            storingDirUpdate: "storing dir update",
            simulatorConfigAdd: "simulator config add",
            simulatorConfigUpdate: "simulator config update",
            simulatorConfigRemove: "simulator config remove",
            launcherConfigImport: "launcher config import"
        }
    };

    /**
     * Multi modal dialog type.
     * @enum {string}
     **/
    static readonly MULTI_MODAL_DIALOG = {
        SimulatorConfigurations: "SimulatorConfigurations",
        ImportSimulatorConfigurations: "ImportSimulatorConfigurations",
        EditStoringDir: "EditStoringDir",
        EditSimulatorConfig: "EditSimulatorConfig",
        BrowseSimulatorExeFile: "BrowseSimulatorExeFile",
        BrowseInputFileDir: "BrowseInputFileDir",
        BrowseStoringDir: "BrowseStoringDir",
        HostDefinition: "HostDefinition"
    };

    bindto: string;

    vm: SimulatorsVueModel;

    simulatorsSocket: Socket | null = null;

    dispatch = d3.dispatch("loginPassword");

    constructor(id: string) {
        this.bindto = "#" + id;
    }

    public id() {
        return this.bindto.substring(1);
    }

    /**
     * @param {Config=} config
     */
    init(config: Config | undefined) {
        let launcherUrl: string | undefined;
        if (config) {
            launcherUrl = config.url;
        }
        this.simulatorsSocket = SimulatorsView.initSimulatorsSocket(launcherUrl);
        
        this.initVueModel();

        // Send a 'get launcher config' message to the launcher server, providing a callback function, in order to retrieve simulators info.
        this.emit(
            SimulatorsView.SIMULATOR_MESSAGE.c2s.getLauncherConfig,
            "", 
            SimulatorsView.prototype.updateLauncherConfig.bind(this)
        );

        // Send a 'get launcher config' message to the launcher server, providing a callback function, in order to retrieve simulators info.
        this.emit(
            SimulatorsView.SIMULATOR_MESSAGE.c2s.getProtocolVersion,
            "", 
            SimulatorsView.prototype.updateProtocolVersion.bind(this)
        );

        // React to messages received from Launcher server-side
        if (this.simulatorsSocket) {
            this.simulatorsSocket.on(SimulatorsView.SIMULATOR_MESSAGE.s2c.storingDirUpdate, SimulatorsView.prototype.updateStoringDirEvent.bind(this));
            this.simulatorsSocket.on(SimulatorsView.SIMULATOR_MESSAGE.s2c.simulatorConfigAdd, SimulatorsView.prototype.addSimulatorConfigEvent.bind(this));
            this.simulatorsSocket.on(SimulatorsView.SIMULATOR_MESSAGE.s2c.simulatorConfigUpdate, SimulatorsView.prototype.updateSimulatorConfigEvent.bind(this));
            this.simulatorsSocket.on(SimulatorsView.SIMULATOR_MESSAGE.s2c.simulatorConfigRemove, SimulatorsView.prototype.removeSimulatorConfigEvent.bind(this));
            this.simulatorsSocket.on(SimulatorsView.SIMULATOR_MESSAGE.s2c.launcherConfigImport, SimulatorsView.prototype.updateLauncherConfig.bind(this));
        }
        else {
            console.log("'SimulatorsView.finalizeInit' error because 'launcherSocket' is null");
        }
    }

    emit(event: string, ...args: any[]): Socket | null {
        if (this.simulatorsSocket) {
            this.simulatorsSocket.emit(event, ...args);
        }
        else {
            console.log("'SimulatorsView.emit' error because 'launcherSocket' is null");
        }
        return this.simulatorsSocket;
    }

    /**
     * Retrieve a socket instance connected to launcher server, specifying a namespace called 'simulators'
     * @param {string=} launcherUrl
     * @returns {import("socket.io").Socket}
     */
    static initSimulatorsSocket(launcherUrl?: string): Socket {
        // prettier-ignore
        return typeof launcherUrl === "undefined"
            ? io("/simulators")
            : io(launcherUrl + "/simulators");
    }

    // ******************************
    // User Interface Initializations
    // ******************************

    initVueModel() {
        this.vm = createApp({
            data() {
                return SimulatorsView.vueData();
            },
            methods: this.vueMethods(),
            computed: this.vueComputed()
        }).mount(this.bindto);
    }

    // *************
    // Vue.js - Data
    // *************

    static vueData() {
        return {
            protocolVersion: -1,
            /** @type {import('../launcherServer').SimulatorConfig[]} */
            simulators: [],
            /** @type {MULTI_MODAL_DIALOG} */
            multiModalDialog: "",
            /** @type {?import('../launcherServer').SimulatorConfig} */
            shownSimulator: null,
            /** @type {string} */
            storingDir: "",
            /** @type {string} */
            editedStoringDir: "",
            /** @type {string} */
            editedDir: "",
            /** @type {string[]} */
            editedChildren: [],
            /** @type {string} */
            editedHostName: "",
            /** @type {number} */
            editedPort: 22,
            /** @type {string} */
            editedRunningDir: ""
        };
    }

    // ****************
    // Vue.js - Methods
    // ****************

    vueMethods() {
        return {
            addSimulator: SimulatorsView.prototype.addSimulator.bind(this),
            exportConfigs: SimulatorsView.prototype.exportConfigs.bind(this),
            openConfsFileChooser: SimulatorsView.prototype.openConfsFileChooser.bind(this),
            importSimulatorConfigurations: SimulatorsView.prototype.importSimulatorConfigurations.bind(this),
            editSimulator: SimulatorsView.prototype.editSimulator.bind(this),
            removeSimulator: SimulatorsView.prototype.removeSimulator.bind(this),
            submitStoringDir: SimulatorsView.prototype.submitStoringDir.bind(this),
            submitSimulatorChanges: SimulatorsView.prototype.submitSimulatorChanges.bind(this),
            clickDir: SimulatorsView.prototype.clickDir.bind(this),
            editDir: SimulatorsView.prototype.editDir.bind(this),
            clickExeFile: SimulatorsView.prototype.clickExeFile.bind(this),
            changeStoringDir: SimulatorsView.prototype.changeStoringDir.bind(this),
            changeHostDefinition: SimulatorsView.prototype.changeHostDefinition.bind(this),
            changeInputDir: SimulatorsView.prototype.changeInputDir.bind(this),
            changeInputParamFiles: SimulatorsView.prototype.changeInputParamFiles.bind(this),
            changeInputDataFiles: SimulatorsView.prototype.changeInputDataFiles.bind(this),
            openMultiModalDialog: SimulatorsView.prototype.openMultiModalDialog.bind(this),
            closeMultiModalDialog: SimulatorsView.prototype.closeMultiModalDialog.bind(this),
            openHostDefinitionDialog : SimulatorsView.prototype.openHostDefinitionDialog.bind(this)
        };
    }

    addSimulator() {
        this.openMultiModalDialog(SimulatorsView.MULTI_MODAL_DIALOG.EditSimulatorConfig);
        this.vm.shownSimulator = SimulatorsView.newSimulatorConfig();
    }

    /**
     * @returns {import('../launcherServer').SimulatorConfig}
     */
    static newSimulatorConfig(): SimulatorConfig {
        /** @type {import('../launcherServer').SimulatorConfig} */
        const simulatorConfig: SimulatorConfig = {
            id: -1,
            config_name: "",
            config_descr: "",
            simulator_exe: "",
            host: "localhost",
            port: 22,
            running_dir: "",
            argument: "",
            cluster_size: 1,
            vector_support: false,
            vector_size: 5,
            simulation_dir: "",
            simulation_param_files: [],
            simulation_data_files: [],
            res_count: 1,
            result_file_name: "",
        };

        return simulatorConfig;
    }

    /**
     * Callback for 'Export' button from 'Simulator Configurations' dialog.
     */
    exportConfigs() {
        this.emit(
            SimulatorsView.SIMULATOR_MESSAGE.c2s.getLauncherConfig,
            "",
            /**
             * @param {import('../launcherServer').LauncherConfig} launcherConfig
             */
            (launcherConfig: LauncherConfig) => {
                if (launcherConfig) {
                    const text = JSON.stringify(launcherConfig, null, 4);
                    const blob = new Blob([text], {
                        type: "application/json;charset=utf-8",
                    });
                    // @ts-ignore
                    saveAs(blob, "launcherServer.json");
                } else {
                    alert("Sorry, simulator configurations exporting failed");
                }
            }
        );
    }

    /**
     * Callback for 'Yes/No' buttons from 'Import Simulator Configurations' dialog.
     */
    importSimulatorConfigurations(useStoringDir: boolean) {
        const thisLauncherView = this;
        this.emit(
            SimulatorsView.SIMULATOR_MESSAGE.c2s.importLauncherConfig,
            { useStoringDir: useStoringDir, configAsText: this.vm.confsToImport },
            /**
             * @param {boolean} success
             * @param {any} data
             */
            (success: boolean, data: any) => {
                if (!success) {
                    alert(
                        "Sorry, simulator configurations importing failed. Cause: " +
                            data
                    );
                }
                thisLauncherView.openMultiModalDialog(SimulatorsView.MULTI_MODAL_DIALOG.SimulatorConfigurations);
            }
        );
    }

    /**
     * Callback for 'Import' button from 'Simulator Configurations' dialog.
     */
    openConfsFileChooser() {
        const thisLauncherView = this;
        d3.select(this.bindto + " #importSelect").on("change", function() {
            // @ts-ignore
            const curFiles = this.files;
            if (curFiles.length) {
                const reader = new FileReader();
                reader.addEventListener("load", function() {
                    if (reader.result) {
                        thisLauncherView.vm.confsToImport = reader.result.toString();
                        // Open a dialog to ask if 'storingDir' must be used
                        thisLauncherView.openMultiModalDialog(SimulatorsView.MULTI_MODAL_DIALOG.ImportSimulatorConfigurations);
                    }
                });
                reader.readAsText(curFiles[0]);
            }
        });
        const importSelect = d3.select<HTMLButtonElement, any>(this.bindto + " #importSelect").node();
        if (importSelect) {
            importSelect.click();
        }
        // $(this.bindto + " #importSelect").trigger("click");
    }

    /**
     * Callback for 'Edit' button from 'Simulator Configurations' table.
     * @param {import('../launcherServer').SimulatorConfig} simulator
     **/
    editSimulator(simulator: SimulatorConfig) {
        this.openMultiModalDialog(SimulatorsView.MULTI_MODAL_DIALOG.EditSimulatorConfig);
        this.vm.shownSimulator = JSON.parse(JSON.stringify(simulator));
    }

    /**
     * Callback for 'Remove' button from 'Simulator Configurations' table.
     * @param {import('../launcherServer').SimulatorConfig} simulator
     **/
    removeSimulator(simulator: SimulatorConfig) {
        // Send a 'remove simulator config' message to 'launcherSocket', providing a callback function, in order to know if operation has succeeded.
        this.emit(
            SimulatorsView.SIMULATOR_MESSAGE.c2s.removeSimulatorConfig,
            simulator,
            /**
             * @param {boolean} success
             * @param {any} data
             */
            (success: boolean, data: any) => {
                if (!success) {
                    alert(
                        "Sorry, simulator configuration removing failed. Cause: " +
                            data
                    );
                }
            }
        );
    }

    /**
     * Callback for 'Submit Changes' from 'Edit Storing Dir' dialog.
     */
    submitStoringDir() {
        const thisLauncherView = this;
        // Send a 'update storing dir' message to 'launcherSocket', providing a callback function, in order to know if operation has succeeded.
        this.emit(
            SimulatorsView.SIMULATOR_MESSAGE.c2s.updateStoringDir,
            thisLauncherView.vm.editedStoringDir,
            /**
             * @param {boolean} success
             * @param {any} data
             */
            (success: boolean, data: any) => {
                if (success) {
                    thisLauncherView.vm.storingDir = thisLauncherView.vm.editedStoringDir;
                } else {
                    alert(
                        "Sorry, 'Storing directory' updating failed. Cause: " +
                            data
                    );
                    thisLauncherView.vm.editedStoringDir = thisLauncherView.vm.storingDir;
                }
                thisLauncherView.openMultiModalDialog(SimulatorsView.MULTI_MODAL_DIALOG.SimulatorConfigurations);
            }
        );
    }

    /**
     * Callback for 'Submit Changes' from 'Edit Simulator Config' dialog.
     */
    submitSimulatorChanges() {
        if (!this.vm.shownSimulator) {
            console.log(
                "'submitSimulatorChanges' is called but 'shownSimulator' is not set"
            );
            return;
        }
        // Clean some fields
        this.vm.shownSimulator.config_name = this.vm.shownSimulator.config_name.trim();
        this.vm.shownSimulator.simulator_exe = this.vm.shownSimulator.simulator_exe.trim();
        this.vm.shownSimulator.argument = this.vm.shownSimulator.argument.trim();
        this.vm.shownSimulator.simulation_dir = this.vm.shownSimulator.simulation_dir.trim();
        this.vm.shownSimulator.simulation_param_files = this.vm.shownSimulator.simulation_param_files.map(
            (n: string) => n.trim()
        );
        this.vm.shownSimulator.simulation_data_files = this.vm.shownSimulator.simulation_data_files.map(
            (n: string) => n.trim()
        );
        this.vm.shownSimulator.result_file_name = this.vm.shownSimulator.result_file_name.trim();

        if (this.vm.shownSimulator.id === -1) {
            this.sendAddSimulatorConf();
        } else {
            this.sendUpdateSimulatorConf();
        }
    }

    /**
     * Send a 'add simulator config' message to 'launcherSocket', providing a callback function, in order to know if operation has succeeded.
     */
    sendAddSimulatorConf() {
        const thisLauncherView = this;
        this.emit(
            SimulatorsView.SIMULATOR_MESSAGE.c2s.addSimulatorConfig,
            this.vm.shownSimulator,
            /**
             * @param {boolean} success
             * @param {any} data
             */
            (success: boolean, data: any) => {
                if (!success) {
                    alert(
                        "Sorry, simulator configuration adding failed. Cause: " +
                            data
                    );
                }
                thisLauncherView.openMultiModalDialog(SimulatorsView.MULTI_MODAL_DIALOG.SimulatorConfigurations);
            }
        );
    }

    /**
     * Send a 'update simulator config' message to 'launcherSocket', providing a callback function, in order to know if operation has succeeded.
     */
    sendUpdateSimulatorConf() {
        const thisLauncherView = this;
        this.emit(
            SimulatorsView.SIMULATOR_MESSAGE.c2s.updateSimulatorConfig,
            this.vm.shownSimulator,
            /**
             * @param {boolean} success
             * @param {any} data
             */
            (success: boolean, data: any) => {
                if (!success) {
                    alert(
                        "Sorry, simulator configuration updating failed. Cause: " +
                            data
                    );
                }
                thisLauncherView.openMultiModalDialog(SimulatorsView.MULTI_MODAL_DIALOG.SimulatorConfigurations);
            }
        );
    }

    /**
     * Callback for list items from 'BrowseInputFileDir' and 'BrowseStoringDir' dialog.
     * @param {*} lsFile
     **/
    clickDir(lsFile: any) {
        const thisLauncherView = this;
        this.emit(
            SimulatorsView.SIMULATOR_MESSAGE.c2s.listFiles,
            { parent: this.vm.editedDir, child: lsFile.name },
            /**
             * @param {boolean} success
             * @param {any} data
             */
            (success: boolean, data: any) => {
                // If 'lsFile' is not a directory (ie 'data.childrenFiles' is null), no matter if 'success' is true
                if (data.childrenFiles && success) {
                    thisLauncherView.vm.editedDir = data.parent;
                    thisLauncherView.vm.editedChildren.splice(0);
                    Array.prototype.push.apply(
                        thisLauncherView.vm.editedChildren,
                        data.childrenFiles
                    );
                }
            }
        );
    }

    /**
     * Callback for 'Browse' button from 'EditStoringDir' Dialog.
     * @param {string} editedDir
     * @param {MULTI_MODAL_DIALOG} multiModalDialog
     **/
    editDir(editedDir: string, multiModalDialog: string) {
        const thisLauncherView = this;
        this.vm.editedDir = editedDir;
        this.openMultiModalDialog(multiModalDialog);
        this.emit(
            SimulatorsView.SIMULATOR_MESSAGE.c2s.listFiles,
            { parent: this.vm.editedDir, child: null },
            /**
             * @param {boolean} success
             * @param {any} data
             */
            (success: boolean, data: any) => {
                if (success) {
                    thisLauncherView.vm.editedDir = data.parent;
                    thisLauncherView.vm.editedChildren.splice(0);
                    Array.prototype.push.apply(
                        thisLauncherView.vm.editedChildren,
                        data.childrenFiles
                    );
                }
            }
        );
    }

    /**
     * Callback for list items from 'BrowseSimulatorExeFile' dialog.
     * @param {*} lsFile
     **/
    clickExeFile(lsFile: any) {
        const thisLauncherView = this;
        const submit = !lsFile.isDirectory;
        this.emit(
            SimulatorsView.SIMULATOR_MESSAGE.c2s.listFiles,
            { parent: this.vm.editedDir, child: lsFile.name },
            /**
             * @param {boolean} success
             * @param {any} data
             */
            (success: boolean, data: any) => {
                if (success) {
                    if (submit) {
                        if (thisLauncherView.vm.shownSimulator) {
                            thisLauncherView.vm.shownSimulator.simulator_exe = data.parent;
                            thisLauncherView.vm.editedChildren.splice(0);
                            thisLauncherView.vm.multiModalDialog =
                            SimulatorsView.MULTI_MODAL_DIALOG.EditSimulatorConfig;
                        } else {
                            console.log(
                                "'clickExeFile' is called but 'shownSimulator' is not set"
                            );
                        }
                    } else {
                        thisLauncherView.vm.editedDir = data.parent;
                        thisLauncherView.vm.editedChildren.splice(0);
                        Array.prototype.push.apply(
                            thisLauncherView.vm.editedChildren,
                            data.childrenFiles
                        );
                    }
                }
            }
        );
    }

    /**
     * Callback for 'Ok' button from 'BrowseStoringDir' dialog.
     */
    changeStoringDir() {
        this.vm.editedStoringDir = this.vm.editedDir;
        this.vm.editedChildren.splice(0);
        this.openMultiModalDialog(SimulatorsView.MULTI_MODAL_DIALOG.EditStoringDir);
    }

    /**
     * Callback for 'Edit' button from 'Host' section (in 'EditSimulatorConfig' dialog).
     */
    openHostDefinitionDialog() {
        if (!this.vm.shownSimulator) {
            console.log(
                "'openHostDefinitionDialog' is called but 'shownSimulator' is not set"
            );
            return;
        }
        this.vm.editedHostName = this.vm.shownSimulator.host;
        this.vm.editedPort = this.vm.shownSimulator.port;
        this.vm.editedRunningDir = this.vm.shownSimulator.running_dir;
        this.openMultiModalDialog(SimulatorsView.MULTI_MODAL_DIALOG.HostDefinition);
    }

    /**
     * Callback for 'Ok' button from 'HostDefinition' dialog.
     */
    changeHostDefinition() {
        if (!this.vm.shownSimulator) {
            console.log(
                "'changeHostDefinition' is called but 'shownSimulator' is not set"
            );
            return;
        }
        this.vm.shownSimulator.host = this.vm.editedHostName;
        this.vm.shownSimulator.port = this.vm.editedPort;
        this.vm.shownSimulator.running_dir = this.vm.editedRunningDir;
        this.openMultiModalDialog(SimulatorsView.MULTI_MODAL_DIALOG.EditSimulatorConfig);
    }

    /**
     * Callback for 'Ok' button from 'BrowseInputFileDir' dialog.
     */
    changeInputDir() {
        if (!this.vm.shownSimulator) {
            console.log(
                "'changeInputDir' is called but 'shownSimulator' is not set"
            );
            return;
        }
        this.vm.shownSimulator.simulation_dir = this.vm.editedDir;
        this.vm.editedChildren.splice(0);
        this.openMultiModalDialog(SimulatorsView.MULTI_MODAL_DIALOG.EditSimulatorConfig);
    }

    /**
     * Callback for 'Browse' button from 'Input files with parameters' section (in 'EditSimulatorConfig' dialog).
     */
    changeInputParamFiles() {
        // @ts-ignore
        const curFiles: FileSelection = d3
            .select(this.bindto + " #paramFilesSelect")
            .property("files");
        if (curFiles.length) {
            this.vm.simulation_param_files = Array.from(curFiles)
                // @ts-ignore
                .map(f => f.name)
                .join(";");
        }
    }

    /**
     * Callback for 'Browse' button from 'Input files without parameter' section (in 'EditSimulatorConfig' dialog).
     */
    changeInputDataFiles() {
        // @ts-ignore
        const curFiles: FileSelection = d3
            .select(this.bindto + " #dataFilesSelect")
            .property("files");
        if (curFiles.length) {
            this.vm.simulation_data_files = Array.from(curFiles)
                // @ts-ignore
                .map(f => f.name)
                .join(";");
        }
    }

    /**
     * @param {MULTI_MODAL_DIALOG} multiModalDialog
     **/
    openMultiModalDialog(multiModalDialog: string) {
        if (Object.keys(SimulatorsView.MULTI_MODAL_DIALOG).some(d => d === multiModalDialog)) {
            // @ts-ignore
            $(this.bindto + " #MultiModalDialog").modal("show");
            this.vm.multiModalDialog = multiModalDialog;
        } else {
            console.log(
                "'openMultiModalDialog' received '" +
                multiModalDialog +
                    "' which is unknown"
            );
        }
    }

    closeMultiModalDialog() {
        switch (this.vm.multiModalDialog) {
            case SimulatorsView.MULTI_MODAL_DIALOG.SimulatorConfigurations:
                // @ts-ignore
                $(this.bindto + " #MultiModalDialog").modal("hide");
                break;
            case SimulatorsView.MULTI_MODAL_DIALOG.ImportSimulatorConfigurations:
                this.openMultiModalDialog(SimulatorsView.MULTI_MODAL_DIALOG.SimulatorConfigurations);
                break;
            case SimulatorsView.MULTI_MODAL_DIALOG.EditStoringDir:
                this.openMultiModalDialog(SimulatorsView.MULTI_MODAL_DIALOG.SimulatorConfigurations);
                break;
            case SimulatorsView.MULTI_MODAL_DIALOG.EditSimulatorConfig:
                this.openMultiModalDialog(SimulatorsView.MULTI_MODAL_DIALOG.SimulatorConfigurations);
                break;
            case SimulatorsView.MULTI_MODAL_DIALOG.BrowseSimulatorExeFile:
                this.openMultiModalDialog(SimulatorsView.MULTI_MODAL_DIALOG.EditSimulatorConfig);
                break;
            case SimulatorsView.MULTI_MODAL_DIALOG.BrowseInputFileDir:
                this.openMultiModalDialog(SimulatorsView.MULTI_MODAL_DIALOG.EditSimulatorConfig);
                break;
            case SimulatorsView.MULTI_MODAL_DIALOG.BrowseStoringDir:
                this.openMultiModalDialog(SimulatorsView.MULTI_MODAL_DIALOG.EditStoringDir);
                break;
            case SimulatorsView.MULTI_MODAL_DIALOG.HostDefinition:
                this.openMultiModalDialog(SimulatorsView.MULTI_MODAL_DIALOG.EditSimulatorConfig);
                break;
            default:
                console.log(
                    "'closeMultiModalDialog' received '" +
                        this.vm.multiModalDialog +
                        "' which is unknown"
                );
                break;
        }
    }

    // ****************************
    // Vue.js - Computed properties
    // ****************************

    vueComputed() {
        return {
            config_name: this.configName(),
            config_descr: this.configDescr(),
            simulator_exe: this.simulatorExe(),
            argument: this.argument(),
            host_description: this.hostDescription(),
            vector_support: this.vectorSupport(),
            vector_size: this.vectorSize(),
            cluster_size: this.clusterSize(),
            simulation_dir: this.simulationDir(),
            simulation_param_files: this.simulationParamFiles(),
            simulation_data_files: this.simulationDataFiles(),
            result_file_name: this.resultFileName(),
            res_count: this.resCount(),
        };
    }

    configName() {
        const thisLauncherView = this;
        return {
            get: function() {
                return thisLauncherView.vm.shownSimulator ? thisLauncherView.vm.shownSimulator.config_name : "";
            },
            /** @param {string} newValue */
            set: function(newValue: string) {
                if (!thisLauncherView.vm.shownSimulator) {
                    console.log(
                        "'configName' is called but 'shownSimulator' is not set"
                    );
                    return;
                }
                thisLauncherView.vm.shownSimulator.config_name = newValue;
            },
        };
    }

    configDescr() {
        const thisLauncherView = this;
        return {
            get: function() {
                return thisLauncherView.vm.shownSimulator ? thisLauncherView.vm.shownSimulator.config_descr : "";
            },
            /** @param {string} newValue */
            set: function(newValue: string) {
                if (!thisLauncherView.vm.shownSimulator) {
                    console.log(
                        "'configDescr' is called but 'shownSimulator' is not set"
                    );
                    return;
                }
                thisLauncherView.vm.shownSimulator.config_descr = newValue;
            },
        };
    }

    simulatorExe() {
        const thisLauncherView = this;
        return {
            get: function() {
                return thisLauncherView.vm.shownSimulator ? thisLauncherView.vm.shownSimulator.simulator_exe : "";
            },
            /** @param {string} newValue */
            set: function(newValue: string) {
                if (!thisLauncherView.vm.shownSimulator) {
                    console.log(
                        "'simulatorExe' is called but 'shownSimulator' is not set"
                    );
                    return;
                }
                thisLauncherView.vm.shownSimulator.simulator_exe = newValue;
            },
        };
    }

    argument() {
        const thisLauncherView = this;
        return {
            get: function() {
                return thisLauncherView.vm.shownSimulator ? thisLauncherView.vm.shownSimulator.argument : "";
            },
            /** @param {string} newValue */
            set: function(newValue: string) {
                if (!thisLauncherView.vm.shownSimulator) {
                    console.log(
                        "'argument' is called but 'shownSimulator' is not set"
                    );
                    return;
                }
                thisLauncherView.vm.shownSimulator.argument = newValue;
            },
        };
    }

    hostDescription() {
        const thisLauncherView = this;
        return function() {
            if (thisLauncherView.vm.shownSimulator) {
                if (thisLauncherView.vm.shownSimulator.host !== "localhost") {
                    return thisLauncherView.vm.shownSimulator.host + ":" + thisLauncherView.vm.shownSimulator.port + "(" + thisLauncherView.vm.shownSimulator.running_dir + ")";
                }
                return thisLauncherView.vm.shownSimulator.host;
            }
            return "";
        };
    }

    vectorSupport() {
        const thisLauncherView = this;
        return {
            get: function() {
                return thisLauncherView.vm.shownSimulator && thisLauncherView.vm.shownSimulator.vector_support ? thisLauncherView.vm.shownSimulator.vector_support : false;
            },
            /** @param {boolean} newValue */
            set: function(newValue: boolean) {
                if (!thisLauncherView.vm.shownSimulator) {
                    console.log(
                        "'vectorSupport' is called but 'shownSimulator' is not set"
                    );
                    return;
                }
                thisLauncherView.vm.shownSimulator.vector_support = newValue;
            },
        };
    }

    vectorSize() {
        const thisLauncherView = this;
        return {
            get: function() {
                return thisLauncherView.vm.shownSimulator && thisLauncherView.vm.shownSimulator.vector_size ? thisLauncherView.vm.shownSimulator.vector_size : 1;
            },
            /** @param {number} newValue */
            set: function(newValue: number) {
                if (!thisLauncherView.vm.shownSimulator) {
                    console.log(
                        "'vectorSize' is called but 'shownSimulator' is not set"
                    );
                    return;
                }
                thisLauncherView.vm.shownSimulator.vector_size = +newValue;
            },
        };
    }

    clusterSize() {
        const thisLauncherView = this;
        return {
            get: function() {
                return thisLauncherView.vm.shownSimulator ? thisLauncherView.vm.shownSimulator.cluster_size : 1;
            },
            /** @param {number} newValue */
            set: function(newValue: number) {
                if (!thisLauncherView.vm.shownSimulator) {
                    console.log(
                        "'clusterSize' is called but 'shownSimulator' is not set"
                    );
                    return;
                }
                thisLauncherView.vm.shownSimulator.cluster_size = +newValue;
            },
        };
    }

    simulationDir() {
        const thisLauncherView = this;
        return {
            get: function() {
                return thisLauncherView.vm.shownSimulator
                    ? thisLauncherView.vm.shownSimulator.simulation_dir
                    : "";
            },
            /** @param {string} newValue */
            set: function(newValue: string) {
                if (!thisLauncherView.vm.shownSimulator) {
                    console.log(
                        "'simulationDir' is called but 'shownSimulator' is not set"
                    );
                    return;
                }
                thisLauncherView.vm.shownSimulator.simulation_dir = newValue;
            },
        };
    }

    simulationParamFiles() {
        const thisLauncherView = this;
        return {
            get: function() {
                return thisLauncherView.vm.shownSimulator
                    ? thisLauncherView.vm.shownSimulator.simulation_param_files.join(";")
                    : "";
            },
            /** @param {string} newValue */
            set: function(newValue: string) {
                if (!thisLauncherView.vm.shownSimulator) {
                    console.log(
                        "'simulationParamFiles' is called but 'shownSimulator' is not set"
                    );
                    return;
                }
                thisLauncherView.vm.shownSimulator.simulation_param_files = newValue.split(";")
                    .map(e => e.trim())
                    .filter(e => e.length !== 0);
            },
        };
    }

    simulationDataFiles() {
        const thisLauncherView = this;
        return {
            get: function() {
                return thisLauncherView.vm.shownSimulator
                    ? thisLauncherView.vm.shownSimulator.simulation_data_files.join(";")
                    : "";
            },
            /** @param {string} newValue */
            set: function(newValue: string) {
                if (!thisLauncherView.vm.shownSimulator) {
                    console.log(
                        "'simulationDataFiles' is called but 'shownSimulator' is not set"
                    );
                    return;
                }
                thisLauncherView.vm.shownSimulator.simulation_data_files = newValue.split(";")
                    .map(e => e.trim())
                    .filter(e => e.length !== 0);
        },
        };
    }

    resultFileName() {
        const thisLauncherView = this;
        return {
            get: function() {
                return thisLauncherView.vm.shownSimulator
                    ? thisLauncherView.vm.shownSimulator.result_file_name
                    : "";
            },
            /** @param {string} newValue */
            set: function(newValue: string) {
                if (!thisLauncherView.vm.shownSimulator) {
                    console.log(
                        "'resultFileName' is called but 'shownSimulator' is not set"
                    );
                    return;
                }
                thisLauncherView.vm.shownSimulator.result_file_name = newValue;
            },
        };
    }

    resCount() {
        const thisLauncherView = this;
        return {
            get: function() {
                return thisLauncherView.vm.shownSimulator 
                    ? JSON.stringify(thisLauncherView.vm.shownSimulator.res_count) : 
                    "0";
            },
            /** @param {number} newValue */
            set: function(newValue: string) {
                if (!thisLauncherView.vm.shownSimulator) {
                    console.log(
                        "'resCount' is called but 'shownSimulator' is not set"
                    );
                    return;
                }
                try {
                    thisLauncherView.vm.shownSimulator.res_count = JSON.parse(newValue);
                }
                catch(err) {
                    // Do nothing (TODO: indicate 'newValue is not valid)
                }
            },
        };
    }

    // *******************************
    // Socket.io callbacks definitions
    // *******************************

    /**
     * @param {string} storingDir
     */
    updateStoringDirEvent(storingDir: string) {
        this.vm.storingDir = storingDir;
        this.vm.editedStoringDir = storingDir;
    }

    /**
     * @param {import('../launcherServer').SimulatorConfig} simulatorConfig
     */
    addSimulatorConfigEvent(simulatorConfig: SimulatorConfig) {
        this.vm.simulators.push(simulatorConfig);
    }

    /**
     * @param {import('../launcherServer').SimulatorConfig} simulatorConfig
     */
    updateSimulatorConfigEvent(simulatorConfig: SimulatorConfig) {
        const index = this.vm.simulators.findIndex(s => s.id === simulatorConfig.id);
        if (index === -1) {
            console.log(
                SimulatorsView.SIMULATOR_MESSAGE.s2c.simulatorConfigUpdate +
                    " received but ignored because " +
                    simulatorConfig.id +
                    " is an unknown id"
            );
        } else {
            this.vm.simulators[index] = simulatorConfig;
        }
    }

    /**
     * @param {import('../launcherServer').SimulatorConfig} simulatorConfig
     */
    removeSimulatorConfigEvent(simulatorConfig: SimulatorConfig) {
        const index = this.vm.simulators.findIndex(s => s.id === simulatorConfig.id);
        if (index === -1) {
            console.log(
                SimulatorsView.SIMULATOR_MESSAGE.s2c.simulatorConfigRemove +
                    " received but ignored because " +
                    simulatorConfig.id +
                    " is an unknown id"
            );
        } else {
            this.vm.simulators.splice(index, 1);
        }
    }

    /**
     * @param {import('../launcherServer').LauncherConfig} launcherConfig
     */
    updateLauncherConfig(launcherConfig: LauncherConfig) {
        this.vm.simulators.splice(0);
        Array.prototype.push.apply(
            this.vm.simulators,
            launcherConfig.simulator_configs
        );
        this.vm.storingDir = launcherConfig.storing_dir;
        this.vm.editedStoringDir = launcherConfig.storing_dir;
    }

    updateProtocolVersion(protocolVersion: number) {
        this.vm.protocolVersion = protocolVersion;
    }
}
