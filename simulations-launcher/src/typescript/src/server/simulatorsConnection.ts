//
// This file is subject to the terms and conditions defined in
// the file 'LICENSE', which is part of this source code.
//

type Socket = import("socket.io").Socket;

// Types of message sent through sockets
import { SIMULATOR_MESSAGE } from "../common/launcherConst.js";

import { Logger } from "../common/logger.js";
import { LauncherServer } from "../launcherServer.js";
import { Optimizer } from "./optimizer.js";

type RunsSet = import("./runs/runsSet.js").RunsSet;
type IoResponseCallback = import("../launcherServer.js").IoResponseCallback;
type IoResponseCallback2 = import("../launcherServer.js").IoResponseCallback2;
type LsFile = {
    parent: string,
    child?: string
}
type ImportConfsArgs = {
    configAsText: string,
    useStoringDir: boolean
}
type OptimPbDef = import("../launcherServer.js").OptimPbDef;
type LauncherConfig = import("../launcherServer.js").LauncherConfig;
type SimulatorConfig = import("../launcherServer.js").SimulatorConfig;

// File system module
import * as fs from "fs";

// Utilities for working with file and directory paths
import * as path from "path";
// Utilities primarily designed to support the needs of Node.js' own internal APIs (for example, provides the 'promisify' function)
import * as util from "util";
// Convert functions following the common error-first callback style, i.e. taking a (err, value) => ... callback as the last argument, and returns a version that returns promises.
const writeFile = util.promisify(fs.writeFile);
const readdir = util.promisify(fs.readdir);

class SimulatorsConnection {

    logger: Logger;

    launcherServer: LauncherServer

    socket: Socket

    /** 
     * Object containig data defining the simulations launcher.
     * @type {LauncherConfig} 
     **/ 
    launcherConfig: LauncherConfig;

    /** 
     * Object in charge of scheduling the execution of runs.
     * @type {RunsSet} 
     **/ 
    runsSet: RunsSet | undefined = undefined;

    // eslint-disable-next-line max-lines-per-function
    constructor(launcherServer: LauncherServer, socket: Socket) {
        this.launcherServer = launcherServer;
        this.socket = socket;
        this.launcherConfig = launcherServer.launcherConfig;
        this.logger = new Logger(socket.id);

        const simulatorsConnection = this;
        this.logger.info("A new socket " + socket.id + " connected for Simulators");
        socket.on("disconnect", function() {
            simulatorsConnection.logger.info("Disconnecting the socket " + socket.id + " from Simulators");
        });

        // React to messages received from a Launcher view
        socket.on(
            SIMULATOR_MESSAGE.c2s.getProtocolVersion,
            SimulatorsConnection.prototype.getProtocolVersion.bind(simulatorsConnection)
        );
        socket.on(
            SIMULATOR_MESSAGE.c2s.updateStoringDir,
            SimulatorsConnection.prototype.updateStoringDir.bind(simulatorsConnection)
        );
        socket.on(
            SIMULATOR_MESSAGE.c2s.addSimulatorConfig,
            SimulatorsConnection.prototype.addSimulatorConfig.bind(simulatorsConnection)
        );
        socket.on(
            SIMULATOR_MESSAGE.c2s.updateSimulatorConfig,
            SimulatorsConnection.prototype.updateSimulatorConfig.bind(simulatorsConnection)
        );
        socket.on(
            SIMULATOR_MESSAGE.c2s.removeSimulatorConfig,
            SimulatorsConnection.prototype.removeSimulatorConfig.bind(simulatorsConnection)
        );
        socket.on(
            SIMULATOR_MESSAGE.c2s.listFiles,
            SimulatorsConnection.prototype.listFiles.bind(simulatorsConnection)
        );
        socket.on(
            SIMULATOR_MESSAGE.c2s.importLauncherConfig,
            SimulatorsConnection.prototype.importLauncherConfig.bind(simulatorsConnection)
        );
        socket.on(
            SIMULATOR_MESSAGE.c2s.getLauncherConfig,
            SimulatorsConnection.prototype.getLauncherConfig.bind(simulatorsConnection)
        );
        socket.on(
            SIMULATOR_MESSAGE.c2s.getRunSets,
            SimulatorsConnection.prototype.getRunSets.bind(simulatorsConnection)
        );
        socket.on(
            SIMULATOR_MESSAGE.c2s.getOptimDescrs,
            SimulatorsConnection.prototype.getOptimDescrs.bind(simulatorsConnection)
        );
        socket.conn.on("message", (msg: string) => {
            if (msg.split("/")[1].startsWith("simulators")) {
                const msgName = msg.split('"')[1];
                const supportedNames = Object.values(SIMULATOR_MESSAGE.c2s);
                if(!supportedNames.includes(msgName)) {
                    this.logger.warning(`WARNING: message '${msgName}' is not one of: ${supportedNames.toString()}`);
                }
            }
        });
    }

    emit(event: string, ...args: any[]) {
        this.launcherServer.io.of("/simulators").emit(event, ...args);
    }

    // *******************************
    // Socket.io callbacks definitions
    // *******************************

    /**
     * @param {string} storingDir new storing directory.
     * @param {IoResponseCallback} fn
     */
    async updateStoringDir(storingDir: string, fn: IoResponseCallback) {
        if (typeof fn !== "function") {
            this.logger.error(`'updateStoringDir' received a 'fn' which is not a function: ${fn}`);
            return;
        }
        if (typeof storingDir !== "string") {
            this.logger.warning(SIMULATOR_MESSAGE.c2s.updateStoringDir + " received but ignored because " + storingDir + " is not valid");
            fn(false, storingDir + " is not valid");
            return;
        }

        this.launcherConfig.storing_dir = storingDir;
        // Forward to all Launcher views
        this.emit(SIMULATOR_MESSAGE.s2c.storingDirUpdate, storingDir);

        try {
            await this.saveSimulatorConfigFile();
        }
        catch (error) {
            this.logger.error(SIMULATOR_MESSAGE.c2s.updateStoringDir + " failed to save '" + LauncherServer.LAUNCHER_SERVER_JSON + "'. Cause: " + error);
            this.logger.trace(error);
        }
        
        fn(true, this.launcherConfig.storing_dir);
    }

    /**
     * Add the given simulator configuration.
     * @param {SimulatorConfig} simulatorConfig Object describing the simulator configuration.
     * @param {IoResponseCallback} fn
     */
     async addSimulatorConfig(simulatorConfig: SimulatorConfig, fn: IoResponseCallback) {
        if (typeof fn !== "function") {
            this.logger.error(`'addSimulatorConfig' received a 'fn' which is not a function: ${fn}`);
            return;
        }
        const simulatorConfigCheck = SimulatorsConnection.checkSimulatorConfig("addSimulatorConfig", simulatorConfig);
        if (simulatorConfigCheck.length !== 0) {
            fn(false, simulatorConfigCheck);
            return;
        }
        if (this.launcherConfig.simulator_configs.length) {
            simulatorConfig.id = this.launcherConfig.simulator_configs[this.launcherConfig.simulator_configs.length - 1].id + 1;
        }
        else {
            simulatorConfig.id = 0;
        }

        this.launcherConfig.simulator_configs.push(simulatorConfig);
        // Forward to all Launcher views
        this.emit(SIMULATOR_MESSAGE.s2c.simulatorConfigAdd, simulatorConfig);

        try {
            await this.saveSimulatorConfigFile();
        }
        catch (error) {
            this.logger.error(SIMULATOR_MESSAGE.c2s.addSimulatorConfig + " failed to save '" + LauncherServer.LAUNCHER_SERVER_JSON + "'. Cause: " + error);
            this.logger.trace(error);
        }
        
        fn(true);
    }

    // eslint-disable-next-line max-lines-per-function, complexity
    private static checkSimulatorConfig(checkedMessage: string, simulatorConfig: SimulatorConfig) {
        if (simulatorConfig === null) {
            return `'${checkedMessage}' received a 'simulatorConfig' which is null`;
        }
        if (typeof simulatorConfig.id !== "number") {
            return `'${checkedMessage}' received a 'simulatorConfig' which has an invalid 'id': ${simulatorConfig.id}`;
        }
        if (typeof simulatorConfig.config_name !== "string") {
            return `'${checkedMessage}' received a 'simulatorConfig' which has an invalid 'config_name': ${simulatorConfig.config_name}`;
        }
        if (typeof simulatorConfig.config_descr !== "string") {
            return `'${checkedMessage}' received a 'simulatorConfig' which has an invalid 'config_descr': ${simulatorConfig.config_descr}`;
        }
        if (typeof simulatorConfig.host !== "string") {
            return `'${checkedMessage}' received a 'simulatorConfig' which has an invalid 'host': ${simulatorConfig.host}`;
        }
        if (Object.prototype.hasOwnProperty.call(simulatorConfig, "port") && typeof simulatorConfig.port !== "number") {
            return `'${checkedMessage}' received a 'simulatorConfig' which has an invalid 'port': ${simulatorConfig.port}`;
        }
        if (typeof simulatorConfig.running_dir !== "string" && simulatorConfig.host !== "localhost") {
            return `'${checkedMessage}' received a 'simulatorConfig' which has an invalid 'running_dir': ${simulatorConfig.running_dir}`;
        }
        if (typeof simulatorConfig.simulator_exe !== "string") {
            return `'${checkedMessage}' received a 'simulatorConfig' which has an invalid 'simulator_exe': ${simulatorConfig.simulator_exe}`;
        }
        if (typeof simulatorConfig.argument !== "string") {
            return `'${checkedMessage}' received a 'simulatorConfig' which has an invalid 'argument': ${simulatorConfig.argument}`;
        }
        if (typeof simulatorConfig.cluster_size !== "number") {
            return `'${checkedMessage}' received a 'simulatorConfig' which has an invalid 'cluster_size': ${simulatorConfig.cluster_size}`;
        }
        if (Object.prototype.hasOwnProperty.call(simulatorConfig, "vector_support") && typeof simulatorConfig.vector_support !== "boolean") {
            return `'${checkedMessage}' received a 'simulatorConfig' which has an invalid 'vector_support': ${simulatorConfig.vector_support}`;
        }
        if (typeof simulatorConfig.simulation_dir !== "string") {
            return `'${checkedMessage}' received a 'simulatorConfig' which has an invalid 'simulation_dir': ${simulatorConfig.simulation_dir}`;
        }
        if (!Array.isArray(simulatorConfig.simulation_param_files)) {
            return `'${checkedMessage}' received a 'simulatorConfig' which has an invalid 'simulation_param_files': ${simulatorConfig.simulation_param_files}`;
        }
        if (!Array.isArray(simulatorConfig.simulation_data_files)) {
            return `'${checkedMessage}' received a 'simulatorConfig' which has an invalid 'simulation_data_files': ${simulatorConfig.simulation_data_files}`;
        }
        if (typeof simulatorConfig.result_file_name !== "string") {
            return `'${checkedMessage}' received a 'simulatorConfig' which has an invalid 'result_file_name': ${simulatorConfig.result_file_name}`;
        }
        if (!Array.isArray(simulatorConfig.res_count) && typeof simulatorConfig.res_count !== "number") {
            return `'${checkedMessage}' received a 'simulatorConfig' which has an invalid 'res_count': ${simulatorConfig.res_count}`;
        }
        return "";
    }

    /**
     * Update a simulator configuration with the given data.
     * @param {SimulatorConfig} simulatorConfig object describing the simulator configuration.
     * @param {IoResponseCallback} fn
     */
    async updateSimulatorConfig(simulatorConfig: SimulatorConfig, fn: IoResponseCallback) {
        if (typeof fn !== "function") {
            this.logger.error(`'updateSimulatorConfig' received a 'fn' which is not a function: ${fn}`);
            return;
        }
        const simulatorConfigCheck = SimulatorsConnection.checkSimulatorConfig("updateSimulatorConfig", simulatorConfig);
        if (simulatorConfigCheck.length !== 0) {
            this.logger.warning(simulatorConfigCheck);
            fn(false, simulatorConfigCheck);
            return;
        }
        const updatedConfigIndex = this.launcherConfig.simulator_configs.findIndex(s => s.id === simulatorConfig.id);
        if (updatedConfigIndex === -1) {
            this.logger.warning(SIMULATOR_MESSAGE.c2s.updateSimulatorConfig + " received but ignored because " + simulatorConfig.id + " is an unknown id");
            fn(false, simulatorConfig.id + " is an unknown id");
            return;
        }

        this.launcherConfig.simulator_configs[updatedConfigIndex] = simulatorConfig;
        // Forward to all Launcher views
        this.emit(SIMULATOR_MESSAGE.s2c.simulatorConfigUpdate, simulatorConfig);

        try {
            await this.saveSimulatorConfigFile();
        }
        catch (error) {
            this.logger.error(SIMULATOR_MESSAGE.c2s.updateSimulatorConfig + " failed to save '" + LauncherServer.LAUNCHER_SERVER_JSON + " '. Cause: " + error);
            this.logger.trace(error);
        }
        
        fn(true);
    }

    /**
     * Remove a simulator configuration.
     * @param {SimulatorConfig} simulatorConfig object describing the simulator configuration.
     * @param {IoResponseCallback} fn
     */
    async removeSimulatorConfig(simulatorConfig: SimulatorConfig, fn: IoResponseCallback) {
        if (typeof fn !== "function") {
            this.logger.error(`'removeSimulatorConfig' received a 'fn' which is not a function: ${fn}`);
            return;
        }
        if (simulatorConfig === null || typeof simulatorConfig.id !== "number") {
            const simulatorConfigCheck = `'removeSimulatorConfig' received an invalid 'simulatorConfig': ${simulatorConfig.id}`;
            this.logger.warning(simulatorConfigCheck);
            fn(false, simulatorConfigCheck);
            return;
        }
        const removedConfigIndex = this.launcherConfig.simulator_configs.findIndex(s => s.id === simulatorConfig.id);
        if (removedConfigIndex === -1) {
            this.logger.warning(SIMULATOR_MESSAGE.c2s.removeSimulatorConfig + " received but ignored because " + simulatorConfig.id + " is an unknown id");
            fn(false, simulatorConfig.id + " is an unknown id");
            return;
        }

        this.launcherConfig.simulator_configs.splice(removedConfigIndex, 1);
        // Forward to all Launcher views
        this.emit(SIMULATOR_MESSAGE.s2c.simulatorConfigRemove, simulatorConfig);

        try {
            await this.saveSimulatorConfigFile();
        }
        catch (error) {
            this.logger.error(SIMULATOR_MESSAGE.c2s.removeSimulatorConfig + " failed to save '" + LauncherServer.LAUNCHER_SERVER_JSON + "'. Cause: " + error);
            this.logger.trace(error);
        }
        fn(true);
    }

    /**
     * Retrieves the list of files contained in the given directory.
     * @param {LsFile} lsFile is an object indicating the directory to read .
     * @param {IoResponseCallback} fn callback which receives an object 'LsFile2'.
     */
    async listFiles(lsFile: LsFile, fn: IoResponseCallback) {
        if (typeof fn !== "function") {
            this.logger.error(`'listFiles' received a 'fn' which is not a function: ${fn}`);
            return;
        }
        try {
            await SimulatorsConnection.listFilesWithExp(lsFile, fn);
        }
        catch (error) {
            fn(false, SIMULATOR_MESSAGE.c2s.listFiles + " failed. Cause: " + error);
            this.logger.trace(error);
        }
    }

    /**
     * Retrieves the list of files contained in the given directory.
     * @param {LsFile} lsFile is an object indicating the directory to read.
     * @param {IoResponseCallback} fn callback which receives an object 'LsFile2'.
     */
    static async listFilesWithExp(lsFile: LsFile, fn: IoResponseCallback) {
        let lsFileName = (lsFile.parent && fs.existsSync(lsFile.parent)) ? lsFile.parent : ".";
        // if 'lsFile.parent' is a file ...
        if (fs.lstatSync(lsFileName).isFile()) {
            // ... 'lsFile.child' must be undefined
            if (lsFile.child) {
                throw new Error(lsFile.parent + " can't be appended to " + lsFile.child);
            }    
            // ... and use parent directory
            lsFileName = path.dirname(lsFile.parent);
        }

        if (lsFile.child) {
            // if 'lsFile.child' is '..', try to use parent directory
            if (lsFile.child === "..") {
                // if 'lsFileAsString' is not a root directory, use parent directory
                const tryParent = path.dirname(lsFileName);
                if (tryParent !== ".") {
                    lsFileName = tryParent;
                }
            }
            // if 'lsFile.child' is not '..', append 'lsFile.child' path segment to 'lsFileAsString' 
            else {
                lsFileName = path.join(lsFileName, lsFile.child);
            }
        }

        if (!fs.existsSync(lsFileName)) {
            throw new Error(lsFileName + " does not exist");
        }

        // Convert as a canonicalized absolute pathname
        const realLsFile = fs.realpathSync(lsFileName);

        // If 'lsFileAsString' is a file, simply return 'realLsFile'
        if (fs.lstatSync(realLsFile).isFile()) {
            fn(true, {parent: realLsFile, childrenFiles: null});
            return;
        }

        // Build 'childrenFiles'
        const inDirFiles = await readdir(realLsFile, {withFileTypes: true});
        const childrenFiles = inDirFiles.map(function(f) { 
            return { name: f.name, isDirectory: f.isDirectory() };
        });

        // if 'realLsFile' is not a root directory, prepend '..' to 'childrenFiles'
        if (path.dirname(realLsFile) !== ".") {
            childrenFiles.splice(0, 0, { name: "..", isDirectory: true} );
        }

        fn(true, {parent: realLsFile, childrenFiles: childrenFiles});
    }

    /**
     * Change the launcher configuration to the given value.
     * @param {ImportConfsArgs} args object with two fields; 'useStoringDir' boolean and 'configAsText' JSON string corresponding to the new content of the launcher configuration.
     * @param {IoResponseCallback} fn
     */
    async importLauncherConfig(args: ImportConfsArgs, fn: IoResponseCallback) {
        if (typeof fn !== "function") {
            this.logger.error(`'importLauncherConfig' received a 'fn' which is not a function: ${fn}`);
            return;
        }
        const importConfsArgsCheck = SimulatorsConnection.checkImportConfsArgs("importLauncherConfig", args);
        if (importConfsArgsCheck.length !== 0) {
            this.logger.error(importConfsArgsCheck);
            fn(false, importConfsArgsCheck);
            return;
        }
        try {
            const parsedConfig = LauncherServer.parseLauncherConfigText(args.configAsText);

            if (args.useStoringDir) {
                this.launcherConfig.storing_dir = parsedConfig.storing_dir;
            }

            let lastId = (this.launcherConfig.simulator_configs.length) 
                ? this.launcherConfig.simulator_configs[this.launcherConfig.simulator_configs.length - 1].id + 1
                : 0;

            for (let index = 0; index < parsedConfig.simulator_configs.length; index++) {
                const s = parsedConfig.simulator_configs[index];
                s.config_name = this.newImportedConfName(s.config_name);
                s.id = lastId;
                lastId = lastId + 1;
                this.launcherConfig.simulator_configs.push(s);
            }
        
            await this.saveSimulatorConfigFile();
            
            this.logger.info(SIMULATOR_MESSAGE.c2s.importLauncherConfig + " succeeded.");
            fn(true);

            // Forward to all Launcher views
            this.emit(SIMULATOR_MESSAGE.s2c.launcherConfigImport, this.launcherConfig);
        }
        catch(error) {
            this.logger.error(SIMULATOR_MESSAGE.c2s.importLauncherConfig + " failed. Cause: " + error);
            this.logger.trace(error);
            fn(false, "" + error);
        }
    }

    private static checkImportConfsArgs(checkedMessage: string, args: ImportConfsArgs) {
        if (args === null) {
            return `'${checkedMessage}' received a 'args' which is null`;
        }
        if (typeof args.configAsText !== "string") {
            return `'${checkedMessage}' received a 'args' which has an invalid 'configAsText': ${args.configAsText}`;
        }
        if (typeof args.useStoringDir !== "boolean") {
            return `'${checkedMessage}' received a 'args' which has an invalid 'useStoringDir': ${args.useStoringDir}`;
        }
        return "";
    }

    private newImportedConfName(confName: string) {
        const existingConfNames = this.launcherConfig.simulator_configs.map(conf => conf.config_name);
        let newName = confName;
        let used = true;
        let index = 1;
        while (used) {
            used = existingConfNames.includes(newName);
            if (used) {
                index = index + 1;
                newName = `${confName} (${index})`;
            }
        }
        return newName;
    }

    saveSimulatorConfigFile() {
        return writeFile(LauncherServer.LAUNCHER_SERVER_JSON, JSON.stringify(this.launcherConfig, null, 4));
    }

    /**
     * @param {*} _data 
     * @param {IoResponseCallback2} fn
     */
    getLauncherConfig(_data: any, fn: IoResponseCallback2) {
        if (typeof fn !== "function") {
            this.logger.error(`'getLauncherConfig' received a 'fn' which is not a function: ${fn}`);
            return;
        }
        fn(this.launcherConfig);
    }

    /**
     * @param {*} _data 
     * @param {IoResponseCallback2} fn
     */
     async getRunSets(_data: any, fn: IoResponseCallback2) {
        if (typeof fn !== "function") {
            this.logger.error(`'getRunSets' received a 'fn' which is not a function: ${fn}`);
            return;
        }
        const runSets = new Set([...this.launcherServer.runsSets.keys()]);

        // Add runSets which are detected scanning 'storingDir'
        try {
            if (fs.existsSync(this.launcherConfig.storing_dir)) {
                const runDirNames = await readdir(this.launcherConfig.storing_dir);
                for (const runDirName of runDirNames) {
                    const fullRunDirName = path.join(this.launcherConfig.storing_dir, runDirName);
                    const stat = fs.lstatSync(fullRunDirName);
                    if (stat.isDirectory()) {
                        runSets.add(runDirName);
                    }
                }
            }
        }
        catch (error) {
            this.logger.trace(error);
        }

        // Return 'runSets' as a sorted array
        fn([...runSets].sort());
    }

    /**
     * Ask to the server to retrieve descriptions of optimizers that can be used for the given problem definition.
     */
    async getOptimDescrs(optimPbDef: OptimPbDef, fn: IoResponseCallback) {
        if (typeof fn !== "function") {
            this.logger.error(`'getOptimDescrs' received a 'fn' which is not a function: ${fn}`);
            return;
        }
        const optimPbDefCheck = SimulatorsConnection.checkOptimPbDef("getOptimDescrs", optimPbDef);
        if (optimPbDefCheck.length !== 0) {
            this.logger.warning(optimPbDefCheck);
            fn(false, optimPbDefCheck);
            return;
        }
        try {
            const optimDescrsAsJson = await Optimizer.runGetOptimDescrs(optimPbDef, this.logger);
            const optimDescrs = JSON.parse(optimDescrsAsJson);
            fn(true, optimDescrs);
        }
        catch (error) {
            fn(false, "" + error);
        }
    }

    private static checkOptimPbDef(checkedMessage: string, optimPbDef: OptimPbDef) {
        if (optimPbDef === null) {
            return `'${checkedMessage}' received a 'optimPbDef' which is null`;
        }
        if (!Object.prototype.hasOwnProperty.call(optimPbDef, "tags")) {
            return `'${checkedMessage}' received a 'optimPbDef' which doesn't have a 'tags' property`;
        }
        if (typeof optimPbDef.tags.constraints !== "boolean") {
            return `'${checkedMessage}' received a 'optimPbDef.tags' which has an invalid 'constraints': ${optimPbDef.tags.constraints}`;
        }
        if (typeof optimPbDef.tags.monoobj !== "boolean") {
            return `'${checkedMessage}' received a 'optimPbDef.tags' which has an invalid 'monoobj': ${optimPbDef.tags.monoobj}`;
        }
        if (typeof optimPbDef.tags.multiobj !== "boolean") {
            return `'${checkedMessage}' received a 'optimPbDef.tags' which has an invalid 'multiobj': ${optimPbDef.tags.multiobj}`;
        }
        if (typeof optimPbDef.tags.derivative !== "boolean") {
            return `'${checkedMessage}' received a 'optimPbDef.tags' which has an invalid 'derivative': ${optimPbDef.tags.derivative}`;
        }
        return "";
    }

    /**
     * Ask to the server its protocol version.
     */
    async getProtocolVersion(_data: any, fn: IoResponseCallback2) {
        if (typeof fn !== "function") {
            this.logger.error(`'addSimulatorConfig' received a 'fn' which is not a function: ${fn}`);
            return;
        }
        fn(LauncherServer.PROTOCOL_VERSION);
    }

}
export {SimulatorsConnection};