//
// This file is subject to the terms and conditions defined in
// the file 'LICENSE', which is part of this source code.
//
type Socket = import("socket.io").Socket;

// Types of message sent through sockets
import { SIMULATION_MESSAGE } from "../common/launcherConst.js";

import { Logger } from "../common/logger.js";
import { LauncherServer } from "../launcherServer.js";
import { RunsSet } from "./runs/runsSet.js";

type Doe = import("../launcherServer.js").Doe;
type Run = import("../launcherServer.js").Run;
type IoResponseCallback = import("../launcherServer.js").IoResponseCallback;
type IoResponseCallback2 = import("../launcherServer.js").IoResponseCallback2;
type OptimArgs = import("../launcherServer.js").OptimArgs;
type TellYArgs = import("../launcherServer.js").TellYArgs;
type StoreBinFileArgs = import("../launcherServer.js").StoreBinFileArgs;
type RunActionArg = import("./runs/runsSet.js").RunActionArg;
type SetRunSimulatorEvent = import("./runs/runsSet.js").SetRunSimulatorEvent;

class SimulationsConnection {

    logger: Logger;

    launcherServer: LauncherServer

    socket: Socket

    setId: string | null = null;

    /** 
     * Object in charge of scheduling the execution of runs.
     * @type {RunsSet} 
     **/ 
    runsSet: RunsSet | undefined = undefined;

    // eslint-disable-next-line max-lines-per-function
    constructor(launcherServer: LauncherServer, socket: Socket) {
        this.launcherServer = launcherServer;
        this.socket = socket;
        this.logger = new Logger(socket.id);
        
        const simulationsConnection = this;
        this.logger.info("A new socket " + socket.id + " connected for Simulations");
        socket.on("disconnect", function() {
            simulationsConnection.logger.info("Disconnecting the socket " + socket.id + " from Simulations");
        });

        // React to messages received from a Launcher view
        socket.on(
            SIMULATION_MESSAGE.c2s.attachSet,
            SimulationsConnection.prototype.attachSet.bind(simulationsConnection)
        );
        socket.on(
            SIMULATION_MESSAGE.c2s.getRuns,
            SimulationsConnection.prototype.getRuns.bind(simulationsConnection)
        );
        socket.on(
            SIMULATION_MESSAGE.c2s.submitAction,
            SimulationsConnection.prototype.submitDOE.bind(simulationsConnection)
        );
        socket.on(
            SIMULATION_MESSAGE.c2s.getOptimList,
            SimulationsConnection.prototype.getOptimList.bind(simulationsConnection)
        );
        socket.on(
            SIMULATION_MESSAGE.c2s.startOptim,
            SimulationsConnection.prototype.startOptim.bind(simulationsConnection)
        );
        socket.on(
            SIMULATION_MESSAGE.c2s.stopOptim,
            SimulationsConnection.prototype.stopOptim.bind(simulationsConnection)
        );
        socket.on(
            SIMULATION_MESSAGE.c2s.storeBinFile,
            SimulationsConnection.prototype.storeBinFile.bind(simulationsConnection)
        );
        socket.on(
            SIMULATION_MESSAGE.c2s.tellY,
            SimulationsConnection.prototype.tellY.bind(simulationsConnection)
        );
        socket.on(
            SIMULATION_MESSAGE.c2s.addRuns,
            SimulationsConnection.prototype.addRuns.bind(simulationsConnection)
        );
        socket.on(
            SIMULATION_MESSAGE.c2s.runAction,
            SimulationsConnection.prototype.runAction.bind(simulationsConnection)
        );
        socket.on(
            SIMULATION_MESSAGE.c2s.searchMustacheInputs,
            LauncherServer.prototype.searchMustacheInputs.bind(launcherServer)
        );
        socket.on(
            SIMULATION_MESSAGE.c2s.setRunSimulator,
            SimulationsConnection.prototype.setRunSimulator.bind(simulationsConnection)
        );
        socket.on(
            SIMULATION_MESSAGE.c2s.loginPasswordNeeded,
            LauncherServer.prototype.loginPasswordNeeded.bind(launcherServer)
        );
        socket.on(
            SIMULATION_MESSAGE.c2s.addLoginPassword,
            LauncherServer.prototype.addLoginPassword.bind(launcherServer)
        );
        socket.conn.on("message", (msg: string) => {
            if (msg.split("/")[1].startsWith("simulations")) {
                const msgName = msg.split('"')[1];
                const supportedNames = Object.values(SIMULATION_MESSAGE.c2s);
                if(!supportedNames.includes(msgName)) {
                    this.logger.info(`WARNING: message '${msgName}' is not one of: ${supportedNames.toString()}`);
                }
            }
        });
    }

    emit(event: string, ...args: any[]) {
        if (this.setId) {
            this.launcherServer.io.of("/simulations").to(this.setId).emit(event, ...args);
        }
        else {
            this.logger.warning(event.toString() + " received but ignored ('setId' is null)");
        }
    }

    // *******************************
    // Socket.io callbacks definitions
    // *******************************

    /**
     * Assigns the given simulator to given runs.
     * @param {SetRunSimulatorEvent} setRunSimulatorEvent specifies a simulator and the runs to assign.
     */
    setRunSimulator(setRunSimulatorEvent: SetRunSimulatorEvent) {
        const check = SimulationsConnection.checkSetRunSimulatorEvent(setRunSimulatorEvent)
        if (check.length !== 0) {
            this.logger.warning(`${SIMULATION_MESSAGE.c2s.setRunSimulator} received but ignored (${check})`);
            return;
        }
        if (!this.runsSet) {
            this.logger.warning(SIMULATION_MESSAGE.c2s.setRunSimulator + " received but ignored (maybe previous launcher action is not yet completed)");
            return;
        }
        const runs = this.runsSet.runs;
        if (setRunSimulatorEvent.runIds.map(runId => runs.get(runId)).filter(r => r && r.queueState).length === 0) {
            this.runsSet.setSimulator(setRunSimulatorEvent.runIds, setRunSimulatorEvent.simulatorId);

            // Forward to all Launcher views
            this.emit(SIMULATION_MESSAGE.s2c.runSimulatorSet, setRunSimulatorEvent);
        }
        else {
            this.logger.warning(SIMULATION_MESSAGE.c2s.setRunSimulator + " received but ignored (some selected simulations are running)");
        }
    }

    private static checkSetRunSimulatorEvent(setRunSimulatorEvent: SetRunSimulatorEvent) {
        if (typeof setRunSimulatorEvent.simulatorId === "undefined") {
            return "No 'simulatorId' specified";
        }
        if (typeof setRunSimulatorEvent.runIds === "undefined") {
            return "No 'runIds' specified";
        }
        if (!Array.isArray(setRunSimulatorEvent.runIds)) {
            return "Bad format for runIds: " + setRunSimulatorEvent.runIds;
        }

        return "";
    }

    /**
     * Submit the given `doe` to the server.  If operation succeeds, a `init runs` message is emitted.
     * @param {Doe} doe Submitted doe.
     */
    submitDOE(doe: Doe) {
        this.checkRunsSet();
        if (!this.runsSet) {
            this.logger.warning("submit action received but ignored (maybe previous launcher action is not yet completed)");
        }
        else if (this.runsSet.isRunning()) {
            this.logger.warning("submit action received but ignored (some simulations are running)");
        }
        else {
            this.runsSet.initRuns(doe);
            this.emit(SIMULATION_MESSAGE.s2c.initRuns, [...this.runsSet.getOrderedRuns()]);
        }
    }

    /**
     * @param {*} _data 
     * @param {IoResponseCallback2} fn
     */
     async getOptimList(_data: any, fn: IoResponseCallback2) {
        if (typeof fn !== "function") {
            this.logger.error(`'getOptimList' received a 'fn' which is not a function: ${fn}`);
            return;
        }
        if (!this.runsSet) {
            this.logger.warning("'getOptimList' action received but ignored (maybe previous launcher action is not yet completed)");
            fn([]);
            return;
        }
        fn(await this.runsSet.getOptimInfoList());
    }

    /**
     * Ask to the server to start an optimisation.
     */
    async startOptim(args: OptimArgs, fn: IoResponseCallback) {
        if (typeof fn !== "function") {
            this.logger.error(`'startOptim' received a 'fn' which is not a function: ${fn}`);
            return;
        }
        this.checkRunsSet();
        if (!this.runsSet) {
            const err = "optim action received but ignored (maybe previous launcher action is not yet completed)";
            this.logger.error(err);
            fn(false, {
                result: err, 
                optimId: args.optimId
            });
            return;
        }
        const optimArgsCheck = SimulationsConnection.checkOptimArgs("startOptim", args);
        if (optimArgsCheck.length !== 0) {
            this.logger.error(optimArgsCheck);
            fn(false, {
                result: optimArgsCheck, 
                optimId: args.optimId
            });
            return;
        }
        this.runsSet.startOptim(args, fn);
    }

    private static checkOptimArgs(checkedMessage: string, args: OptimArgs) {
        if (args === null) {
            return `'${checkedMessage}' received a 'args' which is null`;
        }
        const optimIdType = typeof args.optimId;
        if (optimIdType !== "string" && optimIdType !== "number") {
            return `'${checkedMessage}' received a 'args' which has an invalid 'optimId': ${args.optimId}`;
        }
        const wfTypeType = typeof args.workflowType;
        if (wfTypeType !== "string") {
            return `'${checkedMessage}' received a 'args' which has an invalid 'workflowType': ${args.workflowType}`;
        }
        const simulatorNameType = typeof args.simulatorName;
        if (simulatorNameType !== "string") {
            return `'${checkedMessage}' received a 'args' which has an invalid 'simulatorName': ${args.simulatorName}`;
        }
        const optimFileNameType = typeof args.optimFileName;
        if (optimFileNameType !== "string") {
            return `'${checkedMessage}' received a 'args' which has an invalid 'optimFileName': ${args.optimFileName}`;
        }
        const optimFuncNameType = typeof args.optimFuncName;
        if (optimFuncNameType !== "string") {
            return `'${checkedMessage}' received a 'args' which has an invalid 'optimFuncName': ${args.optimFuncName}`;
        }
        if (!Object.prototype.hasOwnProperty.call(args, "optimFuncArgs")) {
            return `'${checkedMessage}' received a 'args' which doesn't have an 'optimFuncArgs' property`;
        }
        if (!Object.prototype.hasOwnProperty.call(args.optimFuncArgs, "PbDefinition")) {
            return `'${checkedMessage}' received a 'args' which doesn't have an 'optimFuncArgs.PbDefinition' property`;
        }
        const pbDefinitionProps = [ "nd", "COformulation", "inputflag", "x0", "lb_orig", "ub_orig", "lb", "ub", "paramNames"];
        for (let i = 0; i < pbDefinitionProps.length; i++) {
            if (!Object.prototype.hasOwnProperty.call(args.optimFuncArgs.PbDefinition, pbDefinitionProps[i])) {
                return `'${checkedMessage}' received a 'args' which doesn't have an 'optimFuncArgs.PbDefinition.${pbDefinitionProps[i]}' property`;
            }
        }

        return "";
    }

    /**
     * Ask to the server to stop running optimisation.
     */
    async stopOptim(optimId: string | number) {
        if (!this.runsSet) {
            this.logger.warning("'stopOptim' received but ignored (maybe previous launcher action is not yet completed)");
            return;
        }
        if (typeof optimId !== "string" && typeof optimId !== "number") {
            this.logger.error(`'stopOptim' received an invalid 'optimId': ${optimId}`);
            return;
        }
        this.runsSet.stopOptim(optimId);
    }

    /**
     * Ask to the server to store a binary file.
     */
     async storeBinFile(args: StoreBinFileArgs) {
        if (!this.runsSet) {
            this.logger.warning("'storeBinFile' received but ignored (maybe previous launcher action is not yet completed)");
            return;
        }
        const storeBinFileArgsCheck = SimulationsConnection.checkStoreBinFileArgs("storeBinFile", args);
        if (storeBinFileArgsCheck.length !== 0) {
            this.logger.error(storeBinFileArgsCheck);
            return;
        }
        this.runsSet.storeBinFile(args);
    }

    private static checkStoreBinFileArgs(checkedMessage: string, args: StoreBinFileArgs) {
        if (args === null) {
            return `'${checkedMessage}' received a 'args' which is null`;
        }
        const optimIdType = typeof args.optimId;
        if (optimIdType !== "string" && optimIdType !== "number") {
            return `'${checkedMessage}' received a 'args' which has an invalid 'optimId': ${args.optimId}`;
        }
        if (typeof args.fileName !== "string") {
            return `'${checkedMessage}' received a 'args' which has an invalid 'fileName': ${args.fileName}`;
        }
        if (typeof args.content !== "string") {
            return `'${checkedMessage}' received a 'args' which has an invalid 'content': ${args.content}`;
        }
        return "";
    }

    /**
     * Ask to the server to y results for running optimisation.
     */
    async tellY(tellYArgs: TellYArgs) {
        if (!this.runsSet) {
            this.logger.warning("'tellY' received but ignored (maybe previous launcher action is not yet completed)");
            return;
        }
        const tellYArgsCheck = SimulationsConnection.checkTellYArgs("tellY", tellYArgs);
        if (tellYArgsCheck.length !== 0) {
            this.logger.error(tellYArgsCheck);
            return;
        }
        this.runsSet.tellY(tellYArgs);
    }

    private static checkTellYArgs(checkedMessage: string, tellYArgs: TellYArgs) {
        if (tellYArgs === null) {
            return `'${checkedMessage}' received a 'tellYArgs' which is null`;
        }
        const optimIdType = typeof tellYArgs.optimId;
        if (optimIdType !== "string" && optimIdType !== "number") {
            return `'${checkedMessage}' received a 'tellYArgs' which has an invalid 'optimId': ${tellYArgs.optimId}`;
        }
        if (!Object.prototype.hasOwnProperty.call(tellYArgs, "y")) {
            return `'${checkedMessage}' received a 'tellYArgs' which doesn't have a 'y' property`;
        }
        return "";
    }

    /**
     * Submit additional `doe` to the server.
     * @param {Doe} doe Submitted doe.
     * @param {IoResponseCallback2} fn
     */
    addRuns(doe: Doe, fn: IoResponseCallback2) {
        if (typeof fn !== "function") {
            this.logger.error(`'addRuns' received a 'fn' which is not a function: ${fn}`);
            return;
        }
        if (!this.runsSet) {
            this.logger.warning(SIMULATION_MESSAGE.c2s.addRuns + " received but ignored (maybe previous initialisation has failed)");
            fn([]);
            return;
        }
        const doeCheck = SimulationsConnection.checkDOE("addRuns", doe);
        if (doeCheck.length !== 0) {
            this.logger.error(doeCheck);
            fn([]);
            return;
        }
        const addedRuns: Run[] = this.runsSet.addRuns(doe);
        this.emit(SIMULATION_MESSAGE.s2c.runsAdd, addedRuns);
        fn(addedRuns);
    }

    private static checkDOE(checkedMessage: string, doe: Doe) {
        if (doe === null) {
            return `'${checkedMessage}' received a 'doe' which is null`;
        }
        if (!Array.isArray(doe.paramNames)) {
            return `'${checkedMessage}' received a 'doe' which has an invalid 'paramNames': ${doe.paramNames}`;
        }
        if (!Array.isArray(doe.mat) || doe.mat.length === 0 || !Array.isArray(doe.mat[0])) {
            return `'${checkedMessage}' received a 'doe' which has an invalid 'mat': ${doe.mat}`;
        }
        return "";
    }

    /**
     * @param {*} _data 
     * @param {IoResponseCallback2} fn
     */
    getRuns(_data: any, fn: IoResponseCallback2) {
        if (typeof fn !== "function") {
            this.logger.error(`'getRuns' received a 'fn' which is not a function: ${fn}`);
            return;
        }
        if (!this.runsSet) {
            this.logger.warning("'getRuns' action received but ignored (maybe previous launcher action is not yet completed)");
            fn([])
            return;
        }
        fn([...this.runsSet.getOrderedRuns()]);
    }

    /**
     * Execute the specified action on the specified runs.
     * @param {RunActionArg} runActionArg Specifiy which action is to apply to which runs. 
     */
    runAction(runActionArg: RunActionArg) {
        if (!this.runsSet) {
            this.logger.info(`Launcher action '${runActionArg.actionId}' received but ignored ('runsSet' is not initialized)`);
            return;
        }
        const runActionsArgCheck = SimulationsConnection.checkRunActionsArg("runAction", runActionArg);
        if (runActionsArgCheck.length !== 0) {
            this.logger.error(runActionsArgCheck);
            return;
        }
        this.runsSet.execute(runActionArg);
    }

    private static checkRunActionsArg(checkedMessage: string, runActionArg: RunActionArg) {
        if (runActionArg === null) {
            return `'${checkedMessage}' received a 'runActionArg' which is null`;
        }
        if (typeof runActionArg.actionId !== "string") {
            return `'${checkedMessage}' received a 'runActionArg' which has an invalid 'actionIdType': ${runActionArg.actionId}`;
        }
        if (!Array.isArray(runActionArg.runIds) || runActionArg.runIds.length === 0 || typeof runActionArg.runIds[0] !== "number") {
            return `'${checkedMessage}' received a 'runActionArg' which has an invalid 'runIds': ${runActionArg.runIds}`;
        }
        return "";
    }

    /**
     * @param {string} setId 
     * @param {IoResponseCallback2} fn
     */
    async attachSet(setId: string, fn: IoResponseCallback2) {
        if (typeof fn !== "function") {
            this.logger.error(`'attachSet' received a 'fn' which is not a function: ${fn}`);
            return;
        }
        if (typeof setId !== "string") {
            this.logger.error(`'attachSet' received a 'setId' which is not a string: ${setId}`);
            fn(false);
            return;
        }
        this.logger.info("SimulationsConnection.attachSet:", setId, " socket id:", this.socket.id);
        this.socket.join(setId);
        await this.initRunsSet(setId);
        fn(true);
    }
    
    checkRunsSet() {
        if (!this.runsSet) {
            this.initRunsSet("default");
        }
    }
    
    private async initRunsSet(setId: string) {
        this.setId = setId;
        let runsSet = this.launcherServer.runsSets.get(setId);
        if (!runsSet) {
            runsSet = new RunsSet(this, this.launcherServer.launcherConfig);
            this.launcherServer.runsSets.set(setId, runsSet);
            try {
                runsSet.initFromStoringDir();
            }
            catch (error) {
                this.logger.trace(error);
            }
        }
        this.runsSet = runsSet;
    }
    
}
export {SimulationsConnection};