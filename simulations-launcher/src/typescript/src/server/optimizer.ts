type RunsSet = import("./runs/runsSet.js").RunsSet;
type OptimArgs = import("../launcherServer.js").OptimArgs;
type OptimPbDef = import("../launcherServer.js").OptimPbDef;
type Doe = import("../launcherServer.js").Doe;
type ParamValue = import("../launcherServer.js").ParamValue;
type Run = import("../launcherServer.js").Run;
type IoResponseCallback = import("../launcherServer.js").IoResponseCallback;
type SimulatorConfig = import("../launcherServer.js").SimulatorConfig;

// Types of message sent through sockets
import { RUN_ACTION, STATUS, SIMULATION_MESSAGE } from "../common/launcherConst.js";
import { Logger } from "../common/logger.js";
import { AskTell } from "./asktell.js";

// File system module
import * as fs from "fs";
// Utilities for working with file and directory paths
import * as path from "path";
// Retrieve the 'child_process.exec' method providing the ability to exec child processes
import * as child_process from "child_process";
const exec = child_process.exec;
// OS module to have access to temp directory path
import * as os from "os";
import * as d3 from "d3";

class Optimizer {
    static readonly LAUNCHER_DIR = "../";

    static readonly TMP_DIR = os.tmpdir();

    static readonly NO_RESULTS = "No results";

    static readonly LAGUN_WF = "LagunWf";

    static readonly LAUNCHER_WF = "LauncherWf";

    static readonly LAUNCHER_WF_ASKY_BY_LAGUN = "LauncherWfAskyByLagun";
    
    logger: Logger;

    runsSet: RunsSet;

    optimId: string | number;

    simulatorConfig: SimulatorConfig | undefined;

    optimArgs: OptimArgs;

    iterationInProgress: boolean = false; // Tells if an iteration is in progress

    iterations: {runId: number, position: number }[][] = [] // List of list containing run refs (i.e. couple (runId, position)) for each iteration

    askTell: AskTell;

    constructor(args: OptimArgs, runsSet: RunsSet) {
        this.logger = runsSet.logger;
        this.runsSet = runsSet;
        this.optimId = args.optimId;
        this.optimArgs = args;
        this.simulatorConfig = this.runsSet.simulationsConnection.launcherServer.launcherConfig.simulator_configs.find(s => s.config_name === args.simulatorName);
        this.askTell = new AskTell(this.optimId.toString(), Optimizer.TMP_DIR, this.logger);
        this.logger.info("Build Optim ", this.optimId, ", TMP_DIR:", Optimizer.TMP_DIR);
    }

    async start(fn: IoResponseCallback) {
        this.createOptimInfoFile();

        if (this.optimArgs.workflowType === Optimizer.LAUNCHER_WF || this.optimArgs.workflowType === Optimizer.LAUNCHER_WF_ASKY_BY_LAGUN) {
            this.optimArgs.optimFuncArgs.PbDefinition.savepath = path.join(Optimizer.TMP_DIR, "Saved_Optimizations", this.optimId.toString());
            this.logger.info("start, args:", this.optimArgs)
            fs.writeFileSync(this.argsFileName(), JSON.stringify(this.optimArgs) + "\n");

            let runOptimScriptErr = null;
            this.runOptimScript().catch(reason => {
                runOptimScriptErr = reason;
                this.logger.error("'runOptimScript' failed, reason:", reason);
                this.askTell.askX_unlock();
            });
            await this.startCommunicationLoop();
            
            this.removeFile(this.argsFileName());
    
            if (runOptimScriptErr) {
                fn(false, {
                    result: runOptimScriptErr, 
                    optimId: this.optimId
                });
            }
            else {
                const results = await this.readResults();
                const isWithoutResults = (
                    results === Optimizer.NO_RESULTS ||
                    !Array.isArray(results) ||
                    results.length === 0 ||
                    (typeof results[0] === "string" && results[0].indexOf("Error") !== -1)
                );
        
                const done = !isWithoutResults;
                this.logger.info("done: ", done, " results: ", results);
                fn(done, {
                    result: results, 
                    optimId: this.optimId
                });
                this.storeOptimLogs(this.optimArgs.optimFuncArgs.PbDefinition.savepath)
            }
            this.runsSet.optimizers.delete(this.optimId.toString());
        }
    }

    private storeOptimLogs(savepath: string) {
        const srcFileName = savepath + ".zip";
        if (fs.existsSync(srcFileName)) {
            const storingDir = this.runsSet.getStoringDir();
            const destFileName = path.join(storingDir, "Optim" + path.basename(srcFileName));
            fs.copyFileSync(srcFileName, destFileName);
        }
    }

    /**
     * Retrieve descriptions of optimizers (as json) that can be used for the given problem definition.
     * @returns {Promise<string>} a promise with an error description if rejected, or optimizers descriptions if fulfilled
     */
    public static async runGetOptimDescrs(optimPbDef: OptimPbDef, logger: Logger): Promise<string> {
          const constraints = optimPbDef.tags.constraints ? "T" : "F";
          const categorical = optimPbDef.tags.categorical ? "T" : "F";
          const monoobj = optimPbDef.tags.monoobj ? "T" : "F";
          const multiobj = optimPbDef.tags.multiobj ? "T" : "F";
          const derivative = optimPbDef.tags.derivative ? "T" : "F";
          const exeWithAbsolutePath = `Rscript -e "sink(nullfile()); setwd('./optimizations'); suppressWarnings(suppressMessages(library(jsonlite))); source('getOptimDescr.R'); optimDescr <- toJSON(getOptimDescr(list(tags = list(constraints = ${constraints}, categorical = ${categorical}, monoobj = ${monoobj}, multiobj = ${multiobj}, derivative = ${derivative})))); sink(); optimDescr"`;

        return new Promise(function (resolve, reject) {
            exec(exeWithAbsolutePath, { cwd: Optimizer.LAUNCHER_DIR }, (err, stdout, stderr) => {
                if (err) {
                    logger.error("runDir: " + Optimizer.LAUNCHER_DIR);
                    logger.error("exeWithAbsolutePath: " + exeWithAbsolutePath);
                    logger.error(stderr);
                    reject(err); // promise is rejected with an error
                }
                else {
                    resolve(stdout); // promise is fulfilled with 'optimDescrs' as json
                }
            });
        });
    }

    /**
     * Execute the optimizer script.
     * @returns {Promise<void>} a promise with an error description if rejected, or no data if fulfilled
     */
    private async runOptimScript(): Promise<void> {
        const thisOptimizer = this;
        const exeWithAbsolutePath = `Rscript optimizations/optim.R "${this.argsFileName()}"`;

        return new Promise(function (resolve, reject) {
            exec(exeWithAbsolutePath, { cwd: Optimizer.LAUNCHER_DIR }, (err, stdout, stderr) => {
                if (err) {
                    thisOptimizer.logger.error("runDir: " + Optimizer.LAUNCHER_DIR);
                    thisOptimizer.logger.error("exeWithAbsolutePath: " + exeWithAbsolutePath);
                    thisOptimizer.logger.error(stderr);
                    reject(stderr); // promise is rejected with an error
                }
                else {
                    thisOptimizer.logger.info(stdout);
                    resolve(); // promise is fulfilled with no data
                }
            });
        });
    }

    private async startCommunicationLoop() {
        let loop = true;
        while (loop) {
            this.logger.info("askX...");
            const xReceived = await this.askTell.askX() as { [s: string]: string | number; }[];
            this.logger.info("...from askX, received:", xReceived);
            if (xReceived !== null) {
                // eslint-disable-next-line no-mixed-operators
                this.runsSet.simulationsConnection.emit(SIMULATION_MESSAGE.s2c.askYEvent, {
                    xTodo: xReceived, 
                    optimId: this.optimId
                });
                if (this.optimArgs.workflowType === Optimizer.LAUNCHER_WF) {
                    this.launchXTodo(xReceived);
                }
            }
            loop = (xReceived !== null);
        }
    }

    /**
     * Adds a new file containing optim informations.
     */
     createOptimInfoFile(): void {
        this.runsSet.mkRunsSetDir();
        const storingDir = this.runsSet.getStoringDir();
        const infoFileName = path.join(storingDir, "Optim" + this.optimId + ".json");

        fs.writeFileSync(
            infoFileName,
            JSON.stringify(this.optimArgs, null, 4)
        );
    }

    // eslint-disable-next-line max-lines-per-function
    private launchXTodo(xReceived: { [s: string]: string | number; }[]) {
        const simulatorConfig = this.simulatorConfig;
        if (!simulatorConfig) {
            this.logger.warning("'askX' received but simulator is not set");
            return;
        }

        // Call simulations launcher to run received points
        const isNewIteration = xReceived.every((row: { [s: string]: string | number; }) => row.isNewIteration);
        // 'isNewIteration' is 'FALSE' when optimizer asks again same points to check constraints
        const paramNames = this.optimArgs.optimFuncArgs.PbDefinition.paramNames;

        const xTodo = xReceived.map((row: { [s: string]: string | number; }) => {
            return paramNames.map(paramName => {
                return row[paramName];
            });
        });

        const runTodoRefs = [];
        let newRunId = this.runsSet.nextRunId;
        const doeMat = []; // Contains paramValues for each run which doesn't have an associated run and is to launch
        const runToRelaunchIds = []; // References runs which already have an associated run and are to launch

        let paramValuesBlock: (string | number)[][] = [];
        for (let i = 0; i < xTodo.length; i++) {
            const paramValues = xTodo[i];
            const foundRunRef = this.searchRunRef(paramNames, paramValues);
            if (foundRunRef === null) {
                const newRunRef = { runId: newRunId, position: paramValuesBlock.length };
                paramValuesBlock.push(paramValues);
                runTodoRefs.push(newRunRef);
                if (
                    typeof simulatorConfig.vector_size === "undefined" ||
                    paramValuesBlock.length === simulatorConfig.vector_size ||
                    i === xTodo.length - 1
                ) {
                    if (simulatorConfig.vector_support) {
                        const xValuesBlock = paramValuesBlock;
                        doeMat.push(paramNames.map((_paramName, paramIndex) => {
                            return xValuesBlock.map(xValues => {
                                return xValues[paramIndex];
                            });
                        }));
                    }
                    else {
                        doeMat.push(paramValuesBlock[0]);
                    }
                    newRunId = newRunId + 1
                    paramValuesBlock = [];
                }
            }
            else {
                const launcherRun = this.runsSet.runs.get(foundRunRef.runId);
                if (launcherRun && launcherRun.status && launcherRun.status !== STATUS.ended && launcherRun.status !== STATUS.disabled) {
                    runToRelaunchIds.push(foundRunRef.runId);
                }
                runTodoRefs.push(foundRunRef);
            }
        }
        if (isNewIteration) {
            this.iterations.push(runTodoRefs);
            this.iterationInProgress = true;
        }
        this.checkIfTodoCompleted();

        if (runToRelaunchIds.length !== 0) {
            this.runsSet.simulationsConnection.setRunSimulator({
                simulatorId: simulatorConfig.id,
                runIds: runToRelaunchIds
            });
            this.runsSet.simulationsConnection.runAction({
                actionId: RUN_ACTION.configureLaunchLoad,
                runIds: runToRelaunchIds
            });
        }
        
        if (doeMat.length !== 0) {
            const doe: Doe = {
                mat: doeMat,
                paramNames: paramNames
            }
            this.runsSet.simulationsConnection.addRuns(doe, (addedRuns: Run[]) => {
                const runToLaunchIds = addedRuns.map(r => r.id);
                this.runsSet.simulationsConnection.setRunSimulator({
                    simulatorId: simulatorConfig.id,
                    runIds: runToLaunchIds
                });
                this.runsSet.simulationsConnection.runAction({
                    actionId: RUN_ACTION.configureLaunchLoad,
                    runIds: runToLaunchIds
                });
            });
        }
    }

    private static nearEqual(xValues1: (string | number)[], xValuesBlock2: ParamValue[], position2: number): boolean {
        const precision = 10;
        if (xValues1.length !== xValuesBlock2.length) {
            return false;
        }
        for (let i = 0; i < xValues1.length; i++) {
            const xValue1 = xValues1[i];
            const xValueBlock2 = xValuesBlock2[i];
            const xValue2 = Array.isArray(xValueBlock2) ? xValueBlock2[position2] : xValueBlock2;
            if (typeof xValue1 !== typeof xValue2) {
                return false;
            }
            if (typeof xValue1 === "string" && xValue1 !== xValue2) {
                return false;
            }
            if (typeof xValue1 === "number") {
                const fixed1 = (xValue1 as number).toFixed(precision);
                const fixed2 = (xValue2 as number).toFixed(precision);
                if (fixed1 !== fixed2) {
                    return false;
                }
            }
        }
        return true;
    }

    private searchRunRef(paramNames: string[], paramValues: (string | number)[]) {
        if (this.simulatorConfig) {
            for (const run of this.runsSet.runs.values()) {
                if (
                    paramValues.length === run.paramValues.length &&
                    paramNames.length === run.paramNames.length &&
                    paramNames.every((paramName, index) => paramName === run.paramNames[index]) &&
                    run.simulatorId === this.simulatorConfig.id
                ) {
                    const runRef = Optimizer.scanXRun(run, paramValues);
                    if (runRef) {
                        return runRef;
                    }
                }
            }
        }
        return null;
    }

    private static scanXRun(run: Run, paramValues: (string | number)[]) {
        if (Array.isArray(run.paramValues[0])) {
            for (let i = 0; i < run.paramValues[0].length; i++) {
                if (Optimizer.nearEqual(paramValues, run.paramValues, i)) {
                    return { runId: run.id, position: i };
                }
            }
        }
        else {
            if (Optimizer.nearEqual(paramValues, run.paramValues, 0)) {
                return { runId: run.id, position: 0 };
            }
        }
        return null;
    }

    checkIfTodoCompleted() {
        if (!this.iterationInProgress) {
            return;
        }
      
        const runTodoRefs = this.iterations[this.iterations.length - 1];
        const runsTodo = runTodoRefs.map(runRef => this.runsSet.runs.get(runRef.runId));
        // If all iteration points are completed
        if (runsTodo.every(run => {
            return (run && run.status && (run.status === STATUS.ended || run.status === STATUS.onerror));
        })) {
            // Extract Y values for this iteration and send them to optim 'ask.Y'
            const run2resultBlock: Map<number, string[][]> = new Map();
            const yValues = runTodoRefs.map(runRef => {
                const run = this.runsSet.runs.get(runRef.runId);
                if (!run || !run.status || run.status === STATUS.onerror || run.result === null) {
                    return null;
                }
                let resultBlock = run2resultBlock.get(run.id);
                if (!resultBlock) {
                    const xBlockSize = Array.isArray(run.paramValues[0]) ? run.paramValues[0].length : 1;
                    resultBlock = Optimizer.parseLauncherResults(run.result, xBlockSize);
                    run2resultBlock.set(run.id, resultBlock);
                }
                return resultBlock[runRef.position];
            });
            // Clean 'this.iterationInProgress' to avoid to send optim results if other 'runs' event occured
            this.iterationInProgress = false;
      
            this.askTell.tellY(yValues);
        }
    }

    private static parseLauncherResults(results: string, xBlockSize: number) {
        // separator for simulation result file has to be ';'. TO DO: add UI selector for separator
        const output = d3.dsvFormat(";").parseRows(results);
        return output.slice(output.length - xBlockSize, output.length);
    }
  
    tellYFromSocket(yValues: unknown) {
        this.logger.info("...fromSocket, tellY:", yValues);
        this.askTell.tellY(yValues);
    }

    stopFromSocket() {
        this.askTell.askX_unlock();
        this.askTell.askY_unlock();
    }
    
    storeBinFileFromSocket(fileName: string, content: string) {
        const storingDir = this.runsSet.getStoringDir();
        const binFileName = path.join(storingDir, fileName);
        this.logger.info("storeBinFileFromSocket:", binFileName);
        fs.writeFileSync(binFileName, Buffer.from(content, "base64"));
    }
    
    private async readResults() {
        const resFileName = this.resFileName();
        if (!fs.existsSync(resFileName)) {
            return Optimizer.NO_RESULTS;
        }
        const parsedRes = await AskTell.parseFile(resFileName);
        this.removeFile(resFileName);
        return parsedRes;
    }

    private argsFileName() {
        return path.join(Optimizer.TMP_DIR, "args" + this.optimId);
    }

    private resFileName() {
        return path.join(Optimizer.TMP_DIR, "res" + this.optimId);
    }

    private removeFile(fileName: string) {
        try {
            fs.unlinkSync(fileName);
        }
        catch(unlinkErr) {
            this.logger.error(unlinkErr);
        }
    }

}

export { Optimizer };