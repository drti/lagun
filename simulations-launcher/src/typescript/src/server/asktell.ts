// File system module
import * as fs from "fs";
// Utilities for working with file and directory paths
import * as path from "path";

import { Logger } from "../common/logger.js";

class AskTell {
    logger: Logger;

    tmpDir: string;

    optimId: string;

    timeout = 400000000 // about 1 week

    sleepStep = 500

    askXPrefix = "ask.X_";

    askYPrefix = "ask.Y_";

    xTodoPrefix = "X.todo_";

    yDonePrefix = "Y.done_";

    constructor(optimId: string, tmpDir: string, logger: Logger) {
        this.optimId = optimId;
        this.tmpDir = tmpDir;
        this.logger = logger;
    }

    askY() {
        throw new Error("Method not yet implemented - " + this.optimId);
    }

    askY_unlock() {
        this.removeFile(this.askYFileName());
    }

    /** Returns matrix which is read in 'X.todo' file. */
    async askX() {
        let ellapsed = 0;

        const lockFileName = this.askXLockFileName();
        fs.writeFileSync(lockFileName, "");

        const xTodoFileName = this.xTodoFileName();
        while (!fs.existsSync(xTodoFileName) && ellapsed < this.timeout) {
            await sleep(this.sleepStep);
            if (!fs.existsSync(lockFileName)) {
                this.removeFile(xTodoFileName);
                this.logger.info("askX break!");
                return null;
            }
            ellapsed = ellapsed + this.sleepStep;
        }

        this.removeFile(lockFileName);

        if (ellapsed >= this.timeout) {
            this.removeFile(xTodoFileName);
            this.logger.error("timeout, " + xTodoFileName + " not found");
            return null;
        }

        try {
            const parsedX = await AskTell.parseFile(xTodoFileName);
            fs.unlinkSync(xTodoFileName);
            return parsedX;
        }
        catch(err) {
            this.logger.error("error parsing: " + xTodoFileName);
            this.logger.error(err);
        }
        
        return null;
    }

    askX_unlock() {
        this.removeFile(this.askXLockFileName());
    }

    /** Writes a given matrix to a 'Y.done' file. */
    tellY(y: unknown) {
        fs.writeFileSync(this.yDoneFileName(), JSON.stringify(y));
    }

    static parseFile(fileName: string) {
        let count = 0;
        // eslint-disable-next-line no-constant-condition
        while (true) {
            // Try to parse file (maybe it exists but is not yet completed)
            try {
                const fileContent = fs.readFileSync(fileName, "utf8");
                return JSON.parse(fileContent);
            }
            catch(err) {
                sleep(50);
                count = count + 1;
                if (count > 10) {
                    throw err;
                }
            }
        }
    }

    private askXLockFileName() {
        return path.join(this.tmpDir, this.askXPrefix + this.optimId);
    }

    private askYFileName() {
        return path.join(this.tmpDir, this.askYPrefix + this.optimId);
    }

    private xTodoFileName() {
        return path.join(this.tmpDir, this.xTodoPrefix + this.optimId);
    }

    private yDoneFileName() {
        return path.join(this.tmpDir, this.yDonePrefix + this.optimId);
    }

    private removeFile(fileName: string) {
        try {
            if (fs.existsSync(fileName)) {
                fs.unlinkSync(fileName);
            }
        }
        catch(unlinkErr) {
            this.logger.error(unlinkErr);
        }
    }

}

function sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

export { AskTell };