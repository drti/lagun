//
// This file is subject to the terms and conditions defined in
// the file 'LICENSE', which is part of this source code.
//

import nunjucks from "nunjucks";
const env = nunjucks.configure({ autoescape: true });
env.addGlobal("ln", Math.log);
env.addGlobal("exp", Math.exp);

export {nunjucks};