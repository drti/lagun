//
// This file is subject to the terms and conditions defined in
// the file 'LICENSE', which is part of this source code.
//

// ******************************
// Requirements / Initializations 
// ******************************

// File system module
import * as fs from "fs";
// Utilities for working with file and directory paths
import * as path from "path";
// Utilities primarily designed to support the needs of Node.js' own internal APIs (for example, provides the 'promisify' function)
import * as util from "util";

// Initialize the templating language 'nunjucks'
import { nunjucks } from "../template/templateEngine";

// Retrieve the 'child_process.exec' method providing the ability to exec child processes
import * as child_process from "child_process";
const exec = child_process.exec;

import { Logger } from "../../common/logger.js";

// Convert functions following the common error-first callback style, i.e. taking a (err, value) => ... callback as the last argument, and returns a version that returns promises.
const mkdir = util.promisify(fs.mkdir);
const copyFile = util.promisify(fs.copyFile);
const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);
const readdir = util.promisify(fs.readdir);

// - available run actions (used as argument for the message of type 'run action');
// - name of the json file used to store input parameter values in a 'runX' directory (built during simulation configuration);
import { RUN_ACTION, SIMU_INFO_JSON } from "../../common/launcherConst.js";

type RunsSet = import("./runsSet.js").RunsSet;
type RunParamInfo = import("./runsSet.js").RunParamInfo;
type SimulatorConfig = import("../../launcherServer.js").SimulatorConfig;
type ParamValue = import("../../launcherServer.js").ParamValue;

// ***************************
// 'LocalRun' class definition
// ***************************

class LocalRun {
    logger: Logger;

    /** @type {import('./runsSet').RunsSet} */
    runsSet: RunsSet;

    /** @type {import('./runsSet').RunParamInfo} */
    runParamInfo: RunParamInfo;

    /** @type {number} */
    runId: number;

    /** @type {import('../launcherServer').SimulatorConfig} */
    simulatorConfig: SimulatorConfig;

    /** @type {string} */
    runningDir: string;

    /**
     * @param {import('./runsSet').RunsSet} runsSet
     * @param {number} runId
     * @param {import('../launcherServer').SimulatorConfig} simulatorConfig
     */
    constructor(runsSet: RunsSet, runId: number, simulatorConfig: SimulatorConfig) {
        this.logger = runsSet.logger;
        this.runsSet = runsSet;

        this.runParamInfo = runsSet.getRunParamInfo(runId);

        this.runId = runId;
        if (simulatorConfig) { 
            this.simulatorConfig = JSON.parse(JSON.stringify(simulatorConfig));
        }
        else {
            throw new Error("simulatorConfig must be specified");
        }

        this.runningDir = path.join(runsSet.getStoringDir(), "run" + (this.runId + 1));
    }

    /**
     * Execute a given action on the curent run.
     * @param {string} actionId references an action (available actions are defined in RUN_ACTION)
     * @returns {Promise<void>} a promise with no data (if rejected or fulfilled)
     */
    async execute(actionId?: string): Promise<void> {
        switch (actionId) {
            case RUN_ACTION.configureLaunchLoad:
                this.configureLaunchLoad();
                break;
        
            case RUN_ACTION.configure:
                this.configure();
                break;
        
            case RUN_ACTION.load:
                this.load();
                break;
        
            default:
                this.logger.error("Unknown actionId: " + actionId);
                break;
        }
    }

    /**
     * Configure the run (input files are processed), then launch the run (simulator is called), then load the run (results are retrieved).
     * @returns {Promise<void>} a promise with no data (if rejected or fulfilled)
     */
    async configureLaunchLoad(): Promise<void> {
        try {
            this.runsSet.setConfiguring(this.runId);
            await this.configSimulation();
            await this.createSimuInfoFile(this.simulatorConfig.config_name);

            this.runsSet.setRunning(this.runId);
            await this.launchSimulator();

            this.runsSet.setLoading(this.runId);
            const data = await this.loadResults();

            this.runsSet.unqueue(this.runId);
            // Send an 'ended' event containing the result for the current simulation
            this.runsSet.setEnded(this.runId, data);
        }
        catch (error) {
            this.runsSet.unqueue(this.runId);
            // Send a 'onerror' event containing an error description
            this.runsSet.setOnError(this.runId, "" + error);
        }
    }

    /**
     * Configure the run (input files are processed).
     * @returns {Promise<void>} a promise with no data (if rejected or fulfilled)
     */
    async configure(): Promise<void> {
        try {
            this.runsSet.setConfiguring(this.runId);
            await this.configSimulation();
            await this.createSimuInfoFile();

            this.runsSet.unqueue(this.runId);
            // Send a 'configured' event 
            this.runsSet.setConfigured(this.runId);
        }
        catch (error) {
            // Send a 'onerror' event containing an error description
            this.runsSet.setOnError(this.runId, "" + error);
        }
    }

    /**
     * Load the run (results are retrieved).
     * @returns {Promise<void>} a promise with no data (if rejected or fulfilled)
     */
    async load(): Promise<void> {
        try {
            this.runsSet.setLoading(this.runId);
            const data = await this.loadResults();

            this.runsSet.unqueue(this.runId);
            // Send a 'ended' event containing the result for the current simulation
            this.runsSet.setEnded(this.runId, data);
        }
        catch (error) {
            // Send a 'onerror' event containing an error description
            this.runsSet.setOnError(this.runId, "" + error);
        }
    }

    // ***************************
    // About Configuration process
    // ***************************

    /**
     * Creates the directory 'runN' for the simulation, then copies the input files which contain only data, then processes input files which contain parameters.
     * @returns {Promise<void>} a promise with an error description if rejected, or no data if fulfilled
     */
    async configSimulation(): Promise<void> {
        await this.mkRunningDir();
        await this.copyDataFiles();
        return this.processParamFiles();
    }

    /**
     * Creates a running directory dedicated to the simulation.
     * @returns {Promise<void>} a promise with an error description if rejected, or no data if fulfilled
     */
    async mkRunningDir(): Promise<string | void> {
        try {
            return await mkdir(this.runningDir, { recursive: true });
        }
        catch (err) {
            // if (but not only if) the directory already exists, then "err.code === 'EEXIST'"
            if (err instanceof Object) {
                for (const [key, value] of Object.entries(err)) {
                    if (key === "code" && value === "EEXIST") {
                        return undefined;
                    }
                }
            }
            throw err; // promise is rejected with an error
        }
    }

    /**
     * Copies the simulation data files in the directory dedicated to the simulation.
     * @returns {Promise<void>} a promise with an error description if rejected, or no data if fulfilled
     */
    async copyDataFiles(): Promise<void> {
        for (const dataFileName of this.simulatorConfig.simulation_data_files) {
            const fullDataFileName = path.join(this.simulatorConfig.simulation_dir, dataFileName);
            const runningFileName = path.join(this.runningDir, dataFileName);
            await copyFile(fullDataFileName, runningFileName);
        }
    }

    /**
     * Adds a new file containing the values used for each inputs.
     * @returns {Promise<void>} a promise with an error description if rejected, or no data if fulfilled
     */
     createSimuInfoFile(simulatorConfigName?: string): Promise<void> {
        const context = this.simuInfoFileContext();
        const infoFileName = path.join(this.runningDir, SIMU_INFO_JSON);

        const infoFileContent = simulatorConfigName ?
            { xValues: context, simulatorConfig: simulatorConfigName } :
            { xValues: context };
        return writeFile(
            infoFileName, 
            JSON.stringify(infoFileContent, null, 4)
        );
    }

    /**
     * @returns {Object.<string, ParamValue>} context for simuInfoFile
     */
    simuInfoFileContext(): { [s: string]: ParamValue; } {
        const context: { [s: string]: ParamValue; } = {};
        for (let paramIndex = 0; paramIndex < this.runParamInfo.paramNames.length; paramIndex++) {
            const contextValue = this.runParamInfo.paramValues[paramIndex];
            context[this.runParamInfo.paramNames[paramIndex]] = contextValue;
        }
        return context;
    }
    
    /**
     * Processes input files which contain parameters.
     * @returns {Promise<void>} a promise with an error description if rejected, or no data if fulfilled
     */
    async processParamFiles(): Promise<void> {
        const context = this.templateContext();

        for (const paramFileName of this.simulatorConfig.simulation_param_files) {
            await this.processParamFile(paramFileName, context);
        }
    }

    /**
     * @returns {Object.<string, string | number>} template Context
     */
    templateContext(): { [s: string]: (string | number); } {
        const context: { [s: string]: (string | number); } = {};
        for (let paramIndex = 0; paramIndex < this.runParamInfo.paramNames.length; paramIndex++) {
            const contextValue = this.runParamInfo.paramValues[paramIndex];
            if (Array.isArray(contextValue)) {
                context[this.runParamInfo.paramNames[paramIndex]] = JSON.stringify(contextValue);
            }
            else {
                context[this.runParamInfo.paramNames[paramIndex]] = contextValue;
            }
        }
        return context;
    }
    
    /**
     * Processes the given parameter input file.
     * @param {string} paramFileName is the name of the parameter file to process.
     * @param {Object.<string, string>} templateContext provides the value to use for each parameter.
     * @returns {Promise<void>} a promise with an error description if rejected, or no data if fulfilled
     */
    async processParamFile(paramFileName: string, templateContext: { [s: string]: (string | number); }): Promise<void> {
        const fullParamFileName = path.join(this.simulatorConfig.simulation_dir, paramFileName);
        const runningFileName = path.join(this.runningDir, paramFileName);

        const fileContent = await readFile(fullParamFileName, "utf8");
        const processedContent = nunjucks.renderString(fileContent, templateContext);
        return writeFile(runningFileName, processedContent);
    }

    // ********************
    // About Launch process
    // ********************

    /**
     * Execute the run with the simulator defined via 'simulatorConfig.simulator_exe'.
     * @returns {Promise<void>} a promise with an error description if rejected, or no data if fulfilled
     */
    launchSimulator(): Promise<void> {
        const runDirectory = this.runningDir;
        const exeWithAbsolutePath = '"' + path.resolve(this.simulatorConfig.simulator_exe) + '"'; // Escape 'space' characters
        const command = exeWithAbsolutePath + " " + this.simulatorConfig.argument;

        const thisLocalRun = this;
        return new Promise(function (resolve, reject) {
            exec(command, { cwd: runDirectory }, (err, _stdout, _stderr) => {
                if (err) {
                    thisLocalRun.logger.error("runDir: " + runDirectory);
                    thisLocalRun.logger.error("exeWithAbsolutePath: " + exeWithAbsolutePath);
                    reject(err); // promise is rejected with an error
                }
                else {
                    resolve(); // promise is fulfilled with no data
                }
            });
        });
    }

    // ******************
    // About Load process
    // ******************

    /**
     * Searches a 'csv' file which contains results for the simulation and retrieves the result (defined via 'config.result_file_name').
     * @returns {Promise<string>} a promise with an error description if rejected, or the requested result if fulfilled
     */
    async loadResults(): Promise<string> {
        // Retrieve the name of the files which are in the running directory
        const fileNames = await readdir(this.runningDir);

        const csvFileNames = fileNames.filter(f => f === this.simulatorConfig.result_file_name);
        if (csvFileNames.length === 0) {
            throw new Error("No 'csv' file found"); // promise is rejected with an error text
        }

        const runCsvFileName = path.join(this.runningDir, this.simulatorConfig.result_file_name);

        // Retrieve content of 'csvFile'
        const csvFileContent = await readFile(runCsvFileName, "utf8");
        if (csvFileContent.length === 0) {
            throw new Error("Empty result file"); // promise is rejected with an error text
        }
        return csvFileContent; // promise is fulfilled with the requested result
    }
}

export {LocalRun};