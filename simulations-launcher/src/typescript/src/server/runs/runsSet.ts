//
// This file is subject to the terms and conditions defined in
// the file 'LICENSE', which is part of this source code.
//

// Retrieve SSH client and server modules
import { NodeSSH } from "node-ssh";

// File system module
import * as fs from "fs";
import * as unzipper from "unzipper";
// Utilities for working with file and directory paths
import * as path from "path";

// - run statuses (also used as launcher event types);
// - types of message sent through sockets;
// - available run actions (used as argument for the message of type 'run action');
// - name of the json file used to store input parameter values in a 'runX' directory (built during simulation configuration);
import { STATUS, SIMULATION_MESSAGE, RUN_ACTION, SIMU_INFO_JSON } from "../../common/launcherConst.js";

import { Logger } from "../../common/logger.js";

import { RemoteRun } from "./remoteRun.js";
import { LocalRun } from "./localRun.js";
import { Optimizer } from "../optimizer.js";

type IoResponseCallback = import("../../launcherServer.js").IoResponseCallback;
type SimulatorConfig = import("../../launcherServer.js").SimulatorConfig;
type LauncherConfig = import("../../launcherServer.js").LauncherConfig;
type Doe = import("../../launcherServer.js").Doe;
type Run = import("../../launcherServer.js").Run;
type ParamValue = import("../../launcherServer.js").ParamValue;
type SimulationsConnection = import("../simulationsConnection.js").SimulationsConnection;
type OptimArgs = import("../../launcherServer.js").OptimArgs;
type StoreBinFileArgs = import("../../launcherServer.js").StoreBinFileArgs;
type TellYArgs = import("../../launcherServer.js").TellYArgs;
type LoginPassword = import("../../launcherServer.js").LoginPassword;

/**
 * Available internal states of the runs queued for an action.
 * @enum {string} 
 **/
const QUEUE_STATE = {
    waiting: "waiting", 
    inProgress: "inProgress", 
    out: "out"
}

export type RunParamInfo = {
    paramNames: string[],
    paramValues: ParamValue[]
};

export type LauncherEvent = {
    id?: number,
    ids?: number[],
    type: string,
    data?: any
};

export type SetRunSimulatorEvent = {
    runIds: number[],
    simulatorId: number
};

export type RunActionArg = {
    runIds: number[],
    actionId: string
};

// ***********************
// jsdoc types definitions
// ***********************

/**
 * @typedef RunParamInfo
 * @type {object}
 * @property {string[]} paramNames Array of string (one name for each input parameter).
 * @property {string[]} paramValues Array containing the value to use for each input parameter.
 */

/**
 * @typedef LauncherEvent
 * @type {object}
 * @property {number=} id Id of a run.
 * @property {number[]=} ids Array containing the id of some run.
 * @property {string} type See {@link STATUS}
 * @property {*=} data A value extracted from the location specified in the simulator configuration (or a string describing an error).
 */

// ********************************
// 'RunsSet' class definition
// ********************************

class RunsSet {
    
    logger: Logger;

    /** @type {import('../SimulationsConnection').SimulationsConnection} */
    simulationsConnection: SimulationsConnection;

    /** Contains the runs in a map which associates a runId to its run */
    runs: Map<number, Run>;

    /** Contains the runs ordered by id, 
     * or is null if not yet ordered since last modification of 'this.runs' */
    cachedOrderedRuns: Run[] | null = null;

    nextRunId = 0;

    /** @type {import('../launcherServer').LauncherConfig} */
    launcherConfig: LauncherConfig;

    optimizers: Map<string | number, Optimizer>;

    /** @type {Map<number, import('../launcherServer').LoginPassword>} */
    static simulator2loginPwdMap: Map<number, LoginPassword> = new Map();

    /**
     * @param {import('socket.io').Namespace} io 
     * @param {import('../launcherServer').LauncherConfig} launcherConfig 
     */
    constructor(simulationsConnection: SimulationsConnection, launcherConfig: LauncherConfig) {
        this.logger = simulationsConnection.logger;
        this.simulationsConnection = simulationsConnection;
        this.runs = new Map();
        this.launcherConfig = launcherConfig;
        this.optimizers = new Map();
    }

    getOrderedRuns() {
        if (this.cachedOrderedRuns === null) {
            this.cachedOrderedRuns = [...this.runs.values()].sort((run1, run2) => run1.id - run2.id);
        }
        return this.cachedOrderedRuns;
    }

    /**
     * @param {import('../launcherServer').Doe} doe
     */
    initRuns(doe: Doe) {
        this.runs.clear();
        this.nextRunId = 0;
        this.cachedOrderedRuns = null;
        for (let i = 0; i < doe.mat.length; i++) {
            /** @type {import('../launcherServer').Run} */
            const run: Run = {
                id: this.nextRunId,
                status: STATUS.ready,
                simulatorId: -1,
                paramNames: doe.paramNames,
                paramValues: doe.mat[i],
                result: ""
            }
            this.runs.set(run.id, run);
            this.nextRunId = this.nextRunId + 1;
        }
    }

    /**
     * This function searches and parses a 'SIMU_INFO_JSON' file to return names and values of input parameters.
     * @param {number} runDirName directory name of a run.
     */
    parseSimuInfoJson(runDirName: string): {
            paramNames: string[];
            paramValues: ParamValue[];
            simulatorConfigId: number;
            resultsFileName: string | undefined;
        } | null {
        try {
            const fullSimuInfoJson = path.join(runDirName, SIMU_INFO_JSON);
            if (!fs.existsSync(fullSimuInfoJson)) {
                return null;
            }
            const fileContent = fs.readFileSync(fullSimuInfoJson, "utf8");
            const simuInfo = JSON.parse(fileContent);
            if (simuInfo.simulatorConfig) {
                const simulatorConfig = this.launcherConfig.simulator_configs.find(conf => conf.config_name === simuInfo.simulatorConfig);
                if (simulatorConfig) {
                    return { 
                        paramNames: Object.keys(simuInfo.xValues), 
                        paramValues: Object.values(simuInfo.xValues),
                        simulatorConfigId: simulatorConfig.id,
                        resultsFileName: simulatorConfig.result_file_name
                    };
                }
            }
            return { 
                paramNames: Object.keys(simuInfo.xValues), 
                paramValues: Object.values(simuInfo.xValues),
                simulatorConfigId: -1,
                resultsFileName: undefined
            };
    
        }
        catch (error) {
            this.logger.trace(error);
            return null;
        }
    }

    /** 
     * Populate 'runs' field by scanning 'runX' files found in a storing directory.
     * @returns {Promise<void>} a promise with no data (if rejected or fulfilled)
     */
    initFromStoringDir() {
        this.runs.clear();
        this.nextRunId = 0;
        this.cachedOrderedRuns = null;

        try {
            const storingDir = this.getStoringDir();
            if (fs.existsSync(storingDir)) {
                const fileNames = fs.readdirSync(storingDir);
                const runDirNames = fileNames.filter(f => /^run\d+$/.test(f));
                for (const runDirName of runDirNames) {
                    this.initFromRunXDir(storingDir, runDirName);
                }
                if (this.nextRunId > 0) {
                    this.logger.info(`Number of run directories used in '${storingDir}' to populate runSet: ${this.runs.size}`);
                }
            }
        }
        catch(err) {
            this.logger.error("initFromStoringDir failed, ", err)
        }
    }

    private initFromRunXDir(storingDir: string, runDirName: string) {
        try {
            const fullRunDirName = path.join(storingDir, runDirName);
            // Deal with 'SIMU_INFO_JSON'
            const simuInfo = this.parseSimuInfoJson(fullRunDirName);
            if (!simuInfo) {
                // Process next 'runX' file
                return;
            }
    
            const runId = +runDirName.substring("run".length) - 1;
            const run: Run = {
                id: runId,
                status: STATUS.ready,
                simulatorId: simuInfo.simulatorConfigId,
                paramNames: simuInfo.paramNames,
                paramValues: simuInfo.paramValues,
                result: ""
            };

            run.status = STATUS.configured;

            // Deal with result file
            if (simuInfo.resultsFileName) {
                const fullResultFileName = path.join(fullRunDirName, simuInfo.resultsFileName);
                if (fs.existsSync(fullResultFileName)) {
                    run.result = fs.readFileSync(fullResultFileName, "utf8");
                    if (run.result) {
                        run.status = STATUS.ended;
                    }
                }
            }

            this.runs.set(run.id, run);
            this.nextRunId = this.nextRunId > run.id ? this.nextRunId : run.id + 1;
        }
        catch(err) {
            this.logger.error(`initFromStoringDir, '${runDirName}' raises an exception`, err)
        }
    }

    /**
     * @param {import('../launcherServer').Doe} doe identifies the action to execute.
     * @returns {import('../launcherServer').Run[]} Added runs.
     */
    addRuns(doe: Doe): Run[] {
        this.cachedOrderedRuns = null;
        /** @type import('../launcherServer').Run[] */ const addedRuns: Run[] = [];
        for (let i = 0; i < doe.mat.length; i++) {
             const run: Run = {
                id: this.nextRunId,
                status: STATUS.ready,
                simulatorId: -1,
                paramNames: doe.paramNames,
                paramValues: doe.mat[i],
                result: ""
            };
            this.runs.set(run.id, run);
            addedRuns.push(run);
            this.nextRunId = this.nextRunId + 1;
        }
        return addedRuns;
    }

    /**
     * Assigns the given simulator to given runs.
     * @param {number} runIds identify the runs to assign.
     * @param {number} simulatorId identify the simulator to use.
     */
    setSimulator(runIds: number[], simulatorId: number) {
        for (const runId of runIds) {
            const run = this.runs.get(runId);
            if (run) {
                run.simulatorId = simulatorId;
            }
            else {
                this.logger.error(`setSimulator, runId: ${runId}" not in ${[...this.runs.keys()]}`);
            }
        }
    }

    /**
     * @returns {boolean}
     */
    isRunning(): boolean {
        return [...this.runs.values()].filter(r => r.queueState).length !== 0;
    }

    /**
     * Executes the given action for given runs.
     * @param {RunActionArg} runActionArg identifies the action to execute and the affected runs.
     * @returns {Promise<void>} a promise with no data (if rejected or fulfilled)
     */
    async execute(runActionArg: RunActionArg): Promise<void> {
        if (runActionArg.actionId === RUN_ACTION.cancel) {
            this.cancel(runActionArg.runIds);
            return;
        }

        if (runActionArg.actionId === RUN_ACTION.enDisable) {
            this.enDisable(runActionArg.runIds);
            return;
        }

        // Determine which runs are to execute
        const runsToExecute = this.checkRunToExecute(runActionArg.runIds);

        // Set 'Waiting' status to runs which are to execute
        runsToExecute.forEach(r => {
            r.actionId = runActionArg.actionId;
            r.queueState = QUEUE_STATE.waiting;
            r.status = STATUS.waiting;
        });
        this.emitSimulationsEvent({
            ids: runsToExecute.map(r => r.id),
            type: STATUS.waiting,
        });

        this.processRunsQueues();
    }

    /**
     * @param {LauncherEvent} launcherEvent
     */
    emitSimulationsEvent(launcherEvent: LauncherEvent) {
        this.simulationsConnection.emit(SIMULATION_MESSAGE.s2c.launcherEvent, launcherEvent);
        this.optimizers.forEach(optimizer => {
            optimizer.checkIfTodoCompleted();
        });
    }

    /**
     * Cancels the given runs.
     * @param {number} runIds identify the runs to cancel.
     * @returns {Promise<void>} a promise with no data (if rejected or fulfilled)
     */
    async cancel(runIds: number[]): Promise<void> {
        const set = this;
        runIds.forEach(runId => {
            const run = this.runs.get(runId);
            if (run && run.status === STATUS.waiting) {
                try {
                    set.unqueue(run.id);
                    // Send a 'ready' event
                    set.setReady(run.id);
                }
                catch (error) {
                    // Send a 'onerror' event containing an error description
                    set.setOnError(run.id, "" + error);
                }
            }
        });

    }

    /**
     * Enables or disables the given runs; 
     * if status is `Ready`, `Ended` or `On Error`, set status to `Disabled`; 
     * if status is `Disabled`, set status to `Ready`.
     * @param {number} runIds identify the runs to enable/disable.
     * @returns {Promise<void>} a promise with no data (if rejected or fulfilled)
     */
     async enDisable(runIds: number[]): Promise<void> {
        // Determine which runs are to invalidate
        const runsToEnDisable = runIds.map(runId => this.runs.get(runId));

        const runsToDisable = runsToEnDisable.filter(r => r && [STATUS.ready, STATUS.ended, STATUS.onerror].includes(r.status)) as Run[];
        const runsToEnable = runsToEnDisable.filter(r => r && r.status === STATUS.disabled) as Run[];

        const set = this;
        runsToDisable.forEach(r => {
            set.setDisabled(r.id);
        });
        runsToEnable.forEach(r => {
            set.setReady(r.id);
        });

    }

    /**
     * Change status of the run to 'ready', and emit a 'ready launcher event' to all connected sockets.
     * @param {number} runId id of affected run
     * @returns {Promise<void>} a promise with no data (if rejected or fulfilled)
     */
    async setReady(runId: number): Promise<void> {
        this.setStatus(runId, STATUS.ready);
        // Emit an 'ready launcher event' to all connected sockets
        this.emitSimulationsEvent({
            id: runId,
            type: STATUS.ready
        });
    }

    /**
     * Change status of the run to 'configuring', and emit a 'configuring launcher event' to all connected sockets.
     * @param {number} runId id of affected run
     * @returns {Promise<void>} a promise with no data (if rejected or fulfilled)
     */
    async setConfiguring(runId: number): Promise<void> {
        this.setStatus(runId, STATUS.configuring);
        // Emit an 'configuring launcher event' to all connected sockets
        this.emitSimulationsEvent({
            id: runId,
            type: STATUS.configuring
        });
    }

    /**
     * Change status of the run to 'configured', and emit a 'configured launcher event' to all connected sockets.
     * @param {number} runId id of affected run
     * @returns {Promise<void>} a promise with no data (if rejected or fulfilled)
     */
    async setConfigured(runId: number): Promise<void> {
        this.setStatus(runId, STATUS.configured);
        // Emit an 'configured launcher event' to all connected sockets
        this.emitSimulationsEvent({
            id: runId,
            type: STATUS.configured
        });
    }

    /**
     * Change status of the run to 'running', and emit an 'running launcher event' to all connected sockets.
     * @param {number} runId id of affected run
     * @returns {Promise<void>} a promise with no data (if rejected or fulfilled)
     */
    async setRunning(runId: number): Promise<void> {
        this.setStatus(runId, STATUS.running);
        // Emit a 'running launcher event' to all connected sockets
        this.emitSimulationsEvent({
            id: runId,
            type: STATUS.running
        });
    }

    /**
     * Change status of the run to 'loading', and emit an 'loading launcher event' to all connected sockets.
     * @param {number} runId id of affected run
     * @returns {Promise<void>} a promise with no data (if rejected or fulfilled)
     */
    async setLoading(runId: number): Promise<void> {
        this.setStatus(runId, STATUS.loading);
        // Emit a 'loading launcher event' to all connected sockets
        this.emitSimulationsEvent({
            id: runId,
            type: STATUS.loading
        });
    }

    /**
     * Change status of the run to 'ended', store the requested result, and emit an 'ending launcher event' to all connected sockets.
     * @param {number} runId id of affected run
     * @param {string} result the requested result
     * @returns {Promise<void>} a promise with no data (if rejected or fulfilled)
     */
    async setEnded(runId: number, result: string): Promise<void> {
        this.setResult(runId, result);
        this.setStatus(runId, STATUS.ended);
        // Emit an 'ended launcher event' to all connected sockets
        this.emitSimulationsEvent({
            id: runId,
            type: STATUS.ended,
            data: result
        });
    }

    /**
     * Change status of the given run to 'onerror', store the associated error message, and emit an 'onerror launcher event' to all connected sockets.
     * @param {number} runId id of affected run
     * @param {Error} error the error
     * @returns {Promise<void>} a promise with no data (if rejected or fulfilled)
     */
    async setOnError(runId: number, error: string): Promise<void> {
        this.logger.error("Simulation ", runId, ": ", error);
        this.setResult(runId, error);
        this.setStatus(runId, STATUS.onerror);
        // Emit an 'onerror launcher event' to all connected sockets
        this.emitSimulationsEvent({
            id: runId,
            type: STATUS.onerror,
            data: error
        });
    }

    /**
     * Change status of the given run to 'disabled', and emit an 'disabled launcher event' to all connected sockets.
     * @param {number} runId id of affected run
     * @returns {Promise<void>} a promise with no data (if rejected or fulfilled)
     */
     async setDisabled(runId: number): Promise<void> {
        this.setStatus(runId, STATUS.disabled);
        // Emit an 'disabled launcher event' to all connected sockets
        this.emitSimulationsEvent({
            id: runId,
            type: STATUS.disabled
        });
    }

    async tryRun(run: Run) {
        const simulatorConfig = await this.checkSimulatorConfig(run.simulatorId);
        const clusterSize = (simulatorConfig.cluster_size > 0 ) ? simulatorConfig.cluster_size : 1;

        const inProgressRuns = [...this.runs.values()].filter(r => (r.queueState === QUEUE_STATE.inProgress && r.simulatorId === run.simulatorId && r.actionId === run.actionId));
        if (inProgressRuns.length < clusterSize && run.queueState === QUEUE_STATE.waiting) {
            try {
                run.queueState = QUEUE_STATE.inProgress; // eslint-disable-line require-atomic-updates
                const execRun = await this.executableRun(run.id);
                execRun.execute(run.actionId);
            }
            catch (err) {
                run.queueState = undefined; // eslint-disable-line require-atomic-updates
                this.setOnError(run.id, "" + err);
            }
        }
    }

    /**
     * @returns {Promise<void>} a promise with no data (if rejected or fulfilled)
     */
    async processRunsQueues(): Promise<void> {
        // Determine which runs are queued and waiting
        const queuedRuns = [...this.getOrderedRuns()].filter(r => r.queueState === QUEUE_STATE.waiting);

        for (let i = 0; i < queuedRuns.length; i++) {
            try {
                await this.tryRun(queuedRuns[i]);
            }
            catch (err) {
                this.setOnError(queuedRuns[i].id, "" + err);
            }
        }
    }

    /**
     * @param {number} runId
     */
    unqueue(runId: number) {
        const run = this.runs.get(runId);
        if (run) {
            run.queueState = undefined;
        }
        else {
            this.logger.error(`unqueue, runId: ${runId}" not in ${[...this.runs.keys()]}`);
        }
    }

    /**
     * @param {number} runIds identify the runs to check.
     * @returns {import('../launcherServer').Run[]} a promise with an error description if rejected, or with a run executor if fulfilled
     */
    checkRunToExecute(runIds: number[]): Run[] {
        /** @type {import('../launcherServer').Run[]} */ const givenRuns = runIds.map(runId => this.runs.get(runId));
        const runsToExecute = givenRuns.filter(r => 
            r && r.status !== STATUS.waiting && r.status !== STATUS.running && r.status !== STATUS.disabled
        ) as Run[];

        const runsWithNoSimulator = runsToExecute.filter(r => r.simulatorId === -1);
        runsWithNoSimulator.forEach(r => {
            r.queueState = undefined;
            this.setOnError(r.id, "Simulator not defined");
        });

        return runsToExecute.filter(r => r.simulatorId !== -1);
    }

    /**
     * Build a run executor (local or distant).
     * @param {number} runId
     * @returns {Promise<import('./remoteRun').RemoteRun|import('./localRun').LocalRun>} a promise with an error description if rejected, or with a run executor if fulfilled
     */
    async executableRun(runId: number): Promise<RemoteRun | LocalRun> {
        const run = this.runs.get(runId);
        if (!run) {
            throw new Error(`executableRun, runId: ${runId}" not in ${[...this.runs.keys()]}`); // promise is rejected with an error message
        }
        const simulatorConfId = run.simulatorId;
        const simulatorConfig = await this.checkSimulatorConfig(simulatorConfId);
        if (simulatorConfig.host === "localhost") {
            return new LocalRun(this, runId, simulatorConfig);
        }
        else {
            const loginPassword = RunsSet.simulator2loginPwdMap.get(simulatorConfig.id);
            if (!loginPassword) {
                throw new Error(`MissingLoginPassword, ${simulatorConfig.id} not found in [${Array.from(RunsSet.simulator2loginPwdMap.keys()).join()}]`);
            }
            const connConf = {
                host: simulatorConfig.host,
                port: simulatorConfig.port,
                username: loginPassword.login,
                password: loginPassword.password
            };
            const ssh = new NodeSSH();
            await ssh.connect(connConf);
            const sftp = await ssh.requestSFTP();
            return new RemoteRun(this, runId, simulatorConfig, {ssh: ssh, sftp: sftp});
        }
    }

    /**
     * Checks if the requested simulator conf id is known.
     * @param {number} simulatorConfId
     * @returns {Promise<import('../launcherServer').SimulatorConfig>} a promise with an error description if rejected, or a simulator conf if fulfilled
     */
    async checkSimulatorConfig(simulatorConfId: number): Promise<SimulatorConfig> {
        const simulatorConfig = this.launcherConfig.simulator_configs.find(conf => conf.id === simulatorConfId);
        if (!simulatorConfig) {
            throw new Error(simulatorConfId + " is an unknown simulator configuration"); // promise is rejected with an error message
        }
        return simulatorConfig;
    }

    getStoringDir() {
        if (this.simulationsConnection && this.simulationsConnection.setId) {
            return path.join(this.launcherConfig.storing_dir, this.simulationsConnection.setId);
        }
        else {
            return this.launcherConfig.storing_dir;
        }
    }

    /**
     * @param {number} runId
     * @returns {RunParamInfo} a RunParamInfo.
     */
    getRunParamInfo(runId: number): RunParamInfo {
        const run = this.runs.get(runId);
        if (!run) {
            throw new Error(`getRunParamInfo, runId: ${runId}" not in ${[...this.runs.keys()]}`);
        }
        return {
            paramNames: run.paramNames,
            paramValues: run.paramValues
        };
    }

    /**
     * @param {number} runId
     * @param {string} status
     */
    setStatus(runId: number, status: string) {
        const run = this.runs.get(runId);
        if (run) {
            run.status = status;
            this.processRunsQueues();
        }
        else {
            this.logger.error(`setStatus, runId: ${runId}" not in ${[...this.runs.keys()]}`);
        }
    }

    /**
     * @param {number} runId
     * @param {string} result
     */
    setResult(runId: number, result: string) {
        const run = this.runs.get(runId);
        if (run) {
            run.result = result;
        }
        else {
            this.logger.error(`setResult, runId: ${runId}" not in ${[...this.runs.keys()]}`);
        }
    }

    /**
     * Creates directory dedicated to RunsSet.
     * @returns {Promise<void>} a promise with an error description if rejected, or no data if fulfilled
     */
     async mkRunsSetDir(): Promise<string | void> {
        try {
            const storingDir = this.getStoringDir();
            return fs.mkdirSync(storingDir, { recursive: true });
        }
        catch (err) {
            // if (but not only if) the directory already exists, then "err.code === 'EEXIST'"
            if (err instanceof Object) {
                for (const [key, value] of Object.entries(err)) {
                    if (key === "code" && value === "EEXIST") {
                        return undefined;
                    }
                }
            }
            throw err; // promise is rejected with an error
        }
    }

    /**
     * Ask to the server to start an optimisation.
     */
    async startOptim(args: OptimArgs, fn: IoResponseCallback) {
        try {
            const optimizer = new Optimizer(args, this);
            optimizer.start(fn)
                .catch(err => { 
                    fn(false, {
                        result: err.message, 
                        optimId: args.optimId
                    }); 
                });
            this.optimizers.set(args.optimId.toString(), optimizer);
        }
        catch(err) {
            this.logger.error("startOptim failed, ", err)
        }
    }

    /**
     * Ask to the server to store a binary file.
     */
     async storeBinFile(args: StoreBinFileArgs) {
        const optimizer = this.optimizers.get(args.optimId.toString());
        if (optimizer) {
            optimizer.storeBinFileFromSocket(args.fileName, args.content);
        }
        else {
            this.logger.error(`'storeBinFile' received but provided 'optimId' is not recognized: ${args.optimId} not in ${[...this.optimizers.keys()]}`);
        }
    }

    /**
     * Ask to the server to stop running optimisation.
     */
    async stopOptim(optimId: string | number) {
        const optimizer = this.optimizers.get(optimId.toString());
        if (optimizer) {
            optimizer.stopFromSocket();
        }
        else {
            this.logger.error(`'stopOptim' received but provided 'optimId' is not recognized: ${optimId} not in ${[...this.optimizers.keys()]}`);
        }
    }

    /**
     * Ask to the server y results for running optimisation.
     */
    async tellY(tellYArgs: TellYArgs) {
        const optimizer = this.optimizers.get(tellYArgs.optimId.toString());
        if (optimizer) {
            optimizer.tellYFromSocket(tellYArgs.y);
        }
        else {
            this.logger.error(`'tellY' received but provided 'optimId' is not recognized: ${tellYArgs.optimId} != ${[...this.optimizers.keys()]}`);
        }
    }

    static async getIterationsFileContent(optimInfoFullfileName: string): Promise<string> {
        if (fs.existsSync(optimInfoFullfileName)) {
            const zip = fs.createReadStream(optimInfoFullfileName).pipe(unzipper.Parse({ forceStream: true }));
            for await (const entry of zip) {
                const fileName = entry.path;
                if (fileName === "iterations.json") {
                    const content = await entry.buffer();
                    return content.toString();
                } else {
                    entry.autodrain();
                }
            }
        }
        return "[]";
    }

    async getOptimInfo(optimArgsfileName: string, storingDir: string) {
        const optimInfofileName = optimArgsfileName.replace(".json", ".zip");
        const optimInfoFullfileName = path.join(storingDir, optimInfofileName);
        const iterationsFileContent = await RunsSet.getIterationsFileContent(optimInfoFullfileName);

        let iterations;
        try {
            iterations = JSON.parse(iterationsFileContent)
        }
        catch(err) {
            this.logger.error(`Parsing '${iterationsFileContent}' raises an exception`, err)
            iterations = [];
        }

        let optimArgs;
        try {
            const optimArgsFullfileName = path.join(storingDir, optimArgsfileName);
            const optimArgsFileContent = fs.readFileSync(optimArgsFullfileName, "utf8");
            optimArgs = JSON.parse(optimArgsFileContent);
        }
        catch(err) {
            this.logger.error(`'${optimArgsfileName}' raises an exception`, err)
            optimArgs = "";
        }

        return { optimArgs: optimArgs, iterations: iterations };
    }

    /**
     * Scans storing directory and returns the content of all 'optimArgs' files as json structures.
     */
    async getOptimInfoList() {
        try {
            const storingDir = this.getStoringDir();
            if (!fs.existsSync(storingDir)) {
                // If 'storingDir' doesn't exist, it means no simulation has yet been launched
                return [];
            }
            const fileNames = fs.readdirSync(storingDir);
            const optimArgsFileNames = fileNames.filter(f => /Optim[A-Za-z0-9_-]+.json/.test(f));
            const optimInfoList = await Promise.all(optimArgsFileNames.map(optimArgsfileName => {
                return this.getOptimInfo(optimArgsfileName, storingDir);
            }));
            return optimInfoList.filter(optimInfo => optimInfo.optimArgs.optimId !== undefined);
        }
        catch(err) {
            this.logger.error("getOptimArgsList failed, ", err)
            return [];
        }
    }
}

export { RunsSet };