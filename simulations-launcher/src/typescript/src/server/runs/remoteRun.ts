//
// This file is subject to the terms and conditions defined in
// the file 'LICENSE', which is part of this source code.
//

// ******************************
// Requirements / Initializations 
// ******************************

// File system module
import * as fs from "fs";
// Utilities for working with file and directory paths
import * as path from "path";
// Utilities primarily designed to support the needs of Node.js' own internal APIs (for example, provides the 'promisify' function)
import * as util from "util";

// Initialize the templating language 'nunjucks'
import { nunjucks } from "../template/templateEngine";

import { Logger } from "../../common/logger.js";

// Convert functions following the common error-first callback style, i.e. taking a (err, value) => ... callback as the last argument, and returns a version that returns promises.
const mkdir = util.promisify(fs.mkdir);
const readFile = util.promisify(fs.readFile);
const writeFile = util.promisify(fs.writeFile);

// - available run actions (used as argument for the message of type 'run action');
// - name of the json file used to store input parameter values in a 'runX' directory (built during simulation configuration);
import { RUN_ACTION, SIMU_INFO_JSON } from "../../common/launcherConst.js";

type RunsSet = import("./runsSet.js").RunsSet;
type RunParamInfo = import("./runsSet.js").RunParamInfo;
type SimulatorConfig = import("../../launcherServer.js").SimulatorConfig;
type ParamValue = import("../../launcherServer.js").ParamValue;

// ****************************
// 'RemoteRun' class definition
// ****************************

export class RemoteRun {
    logger: Logger;

    /** @type {import('./runsSet').RunsSet} */
    runsSet: RunsSet;

    /** @type {import('./runsSet').RunParamInfo} */
    runParamInfo: RunParamInfo;

    /** @type {number} */
    runId: number;

    /** @type {import('../launcherServer').SimulatorConfig} */
    simulatorConfig: SimulatorConfig;

    /** @type {string} */
    storingDir: string;

    /** @type {string} */
    runningDir: string;

    ssh: any;

    sftp: any;

    /**
     * @param {import('./runsSet').RunsSet} runsSet
     * @param {number} runId
     * @param {import('../launcherServer').SimulatorConfig} simulatorConfig
     */
    constructor(runsSet: RunsSet, runId: number, simulatorConfig: SimulatorConfig, ssh: any) {
        this.logger = runsSet.logger;
        this.runsSet = runsSet;

        this.runParamInfo = runsSet.getRunParamInfo(runId);

        this.runId = runId;
        this.simulatorConfig = JSON.parse(JSON.stringify(simulatorConfig));

        this.storingDir = path.join(runsSet.getStoringDir(), "run" + (this.runId + 1));
        if (this.simulatorConfig.running_dir) {
            if (runsSet.simulationsConnection && runsSet.simulationsConnection.setId) {
                this.runningDir = path.posix.join(this.simulatorConfig.running_dir, runsSet.simulationsConnection.setId, "run" + (this.runId + 1));
            }
            else {
                this.runningDir = path.posix.join(this.simulatorConfig.running_dir, "run" + (this.runId + 1));
            }
        }
        else {
            throw new Error("'running_dir' is not defined");
        }

        this.ssh = ssh.ssh;
        this.sftp = ssh.sftp;
    }

    /**
     * Execute a given action on the curent run.
     * @param {string} actionId
     * @returns {Promise<void>} a promise with no data (if rejected or fulfilled)
     */
    async execute(actionId?: string): Promise<void> {
        switch (actionId) {
            case RUN_ACTION.configureLaunchLoad:
                await this.configureLaunchLoad();
                break;
        
            case RUN_ACTION.configure:
                await this.configure();
                break;
        
            case RUN_ACTION.load:
                await this.load();
                break;
        
            default:
                this.logger.error("Unknown actionId: " + actionId);
                break;
        }
        this.ssh.dispose();
    }

    /**
     * Configure the run (input files are processed), then launch the run (simulator is called), then load the run (results are retrieved).
     * @returns {Promise<void>} a promise with no data (if rejected or fulfilled)
     */
    async configureLaunchLoad(): Promise<void> {
        try {
            this.runsSet.setConfiguring(this.runId);
            await this.configSimulation();
            await this.createSimuInfoFile(this.simulatorConfig.config_name);

            this.runsSet.setRunning(this.runId);
            const res = await this.launchSimulator();
            if (res.code !== 0) {
                this.logger.error("res.stdout:", res.stdout);
                this.logger.error("res.stderr:", res.stderr);
                const message = JSON.stringify({
                    exitCode: res.code, 
                    stderr: res.stderr
                });
                throw new Error(message);
            }

            this.runsSet.setLoading(this.runId);
            const data = await this.loadResults();

            this.runsSet.unqueue(this.runId);
            // Send an 'ended' event containing the result for the current simulation
            this.runsSet.setEnded(this.runId, data);
        }
        catch (error) {
            this.runsSet.unqueue(this.runId);
            // Send a 'onerror' event containing an error description
            this.runsSet.setOnError(this.runId, "" + error);
        }
    }

    /**
     * Configure the run (input files are processed).
     * @returns {Promise<void>} a promise with no data (if rejected or fulfilled)
     */
    async configure(): Promise<void> {
        try {
            this.runsSet.setConfiguring(this.runId);
            await this.configSimulation();
            await this.createSimuInfoFile();

            this.runsSet.unqueue(this.runId);
            // Send a 'configured' event 
            this.runsSet.setConfigured(this.runId);
        }
        catch (error) {
            // Send a 'onerror' event containing an error description
            this.runsSet.setOnError(this.runId, "" + error);
        }
    }

    /**
     * Load the run (results are retrieved).
     * @returns {Promise<void>} a promise with no data (if rejected or fulfilled)
     */
    async load(): Promise<void> {
        try {
            this.runsSet.setLoading(this.runId);
            const data = await this.loadResults();

            this.runsSet.unqueue(this.runId);
            // Send a 'ended' event containing the result for the current simulation
            this.runsSet.setEnded(this.runId, data);
        }
        catch (error) {
            // Send a 'onerror' event containing an error description
            this.runsSet.setOnError(this.runId, "" + error);
        }
    }

    // ***************************
    // About Configuration process
    // ***************************

    /**
     * Creates the directory 'runN' for the simulation, then copies the input files which contain only data, then processes input files which contain parameters.
     * @returns {Promise<void>} a promise with an error description if rejected, or no data if fulfilled
     */
    async configSimulation(): Promise<void> {
        await this.mkStoringDir();
        await this.mkRunningDir();
        await this.copyDataFiles();
        return this.processParamFiles();
    }

    /**
     * Creates a storing directory dedicated to the inputs and to the results of the simulation.
     * @returns {Promise<void>} a promise with an error description if rejected, or no data if fulfilled
     */
    async mkStoringDir(): Promise<string | void> {
        try {
            return await mkdir(this.storingDir, { recursive: true });
        }
        catch (err) {
            // if (but not only if) the directory already exists, then "err.code === 'EEXIST'"
            if (err instanceof Object) {
                for (const [key, value] of Object.entries(err)) {
                    if (key === "code" && value === "EEXIST") {
                        return undefined;
                    }
                }
            }
            throw err; // promise is rejected with an error
        }
    }

    /**
     * Creates a running directory dedicated to the execution of the simulation.
     * @returns {Promise<void>} a promise with an error description if rejected, or no data if fulfilled
     */
    mkRunningDir(): Promise<void> {
        // this.ssh.mkdir(this.runningDir, 'sftp', this.sftp);
        // this.ssh.mkdir(this.runningDir, 'exec');
        return this.ssh.execCommand("mkdir -p " + this.runningDir);
    }

    /**
     * Copies the simulation data files in the directory dedicated to the simulation.
     * @returns {Promise<void>} a promise with an error description if rejected, or no data if fulfilled
     */
    async copyDataFiles(): Promise<void> {
        for (const dataFileName of this.simulatorConfig.simulation_data_files) {
            const fullDataFileName = path.join(this.simulatorConfig.simulation_dir, dataFileName);
            const runningFileName = path.posix.join(this.runningDir, dataFileName);
            await this.ssh.putFile(fullDataFileName, runningFileName, this.sftp);
        }
    }

    /**
     * Adds a new file containing the values used for each inputs.
     * @returns {Promise<void>} a promise with an error description if rejected, or no data if fulfilled
     */
     createSimuInfoFile(simulatorConfigName?: string): Promise<void> {
        const context = this.simuInfoFileContext();
        const infoFileName = path.join(this.storingDir, SIMU_INFO_JSON);

        const infoFileContent = simulatorConfigName ?
            { xValues: context, simulatorConfig: simulatorConfigName } :
            { xValues: context };
        return writeFile(
            infoFileName, 
            JSON.stringify(infoFileContent, null, 4)
        );
    }

    /**
     * @returns {Object.<string, ParamValue>} context for simuInfoFile
     */
    simuInfoFileContext(): { [s: string]: ParamValue; } {
        const context: { [s: string]: ParamValue; } = {};
        for (let paramIndex = 0; paramIndex < this.runParamInfo.paramNames.length; paramIndex++) {
            const contextValue = this.runParamInfo.paramValues[paramIndex];
            context[this.runParamInfo.paramNames[paramIndex]] = contextValue;
        }
        return context;
    }
    
    /**
     * Processes input files which contain parameters.
     * @returns {Promise<void>} a promise with an error description if rejected, or no data if fulfilled
     */
    async processParamFiles(): Promise<void> {
        const context = this.templateContext();

        for (const paramFileName of this.simulatorConfig.simulation_param_files) {
            await this.processParamFile(paramFileName, context);
        }
    }

    /**
     * @returns {Object.<string, string | number>} template Context
     */
     templateContext(): { [s: string]: (string | number); } {
        const context: { [s: string]: (string | number); } = {};
        for (let paramIndex = 0; paramIndex < this.runParamInfo.paramNames.length; paramIndex++) {
            const contextValue = this.runParamInfo.paramValues[paramIndex];
            if (Array.isArray(contextValue)) {
                context[this.runParamInfo.paramNames[paramIndex]] = JSON.stringify(contextValue);
            }
            else {
                context[this.runParamInfo.paramNames[paramIndex]] = contextValue;
            }
        }
        return context;
    }
    
    /**
     * Processes the given parameter input file.
     * @param {string} paramFileName is the name of the parameter file to process.
     * @param {*} templateContext provides the value to use for each parameter.
     * @returns {Promise<void>} a promise with an error description if rejected, or no data if fulfilled
     */
    async processParamFile(paramFileName: string, templateContext: { [s: string]: (string | number); }): Promise<void> {
        const fullParamFileName = path.join(this.simulatorConfig.simulation_dir, paramFileName);
        const runningFileName = path.posix.join(this.runningDir, paramFileName);
        const storingFileName = path.join(this.storingDir, paramFileName);

        const fileContent = await readFile(fullParamFileName, "utf8");
        const processedContent = nunjucks.renderString(fileContent, templateContext);
        await writeFile(storingFileName, processedContent);
        return this.ssh.putFile(storingFileName, runningFileName, this.sftp);
    }

    // ********************
    // About Launch process
    // ********************

    /**
     * Execute the run with the simulator defined via 'simulatorConfig.simulator_exe'.
     * @returns Promise<{ stdout: string, options?: Object, stderr: string, signal: ?string, code: number }> a promise with an error description if rejected, or no data if fulfilled
     */
    launchSimulator() {
        const command = this.simulatorConfig.simulator_exe + " " + this.simulatorConfig.argument;
        return this.ssh.execCommand("cd " + this.runningDir + ";" + command);
    }

    // ******************
    // About Load process
    // ******************

    /**
     * Searches a 'csv' file which contains results for the simulation and retrieves the result (defined via 'config.result_file_name').
     * @returns {Promise<string>} a promise with an error description if rejected, or the requested result if fulfilled
     */
    async loadResults(): Promise<string> {
        // Retrieve the name of the files which are in the running directory
        const fileNames = await this.readRunningDir();

        const csvFileNames = fileNames.map(f => f.filename).filter(n => n.endsWith(this.simulatorConfig.result_file_name));
        if (csvFileNames.length === 0) {
            throw new Error("No 'csv' file found"); // promise is rejected with an error text
        }

        const runningFileName = path.posix.join(this.runningDir, this.simulatorConfig.result_file_name);
        const storingFileName = path.join(this.storingDir, this.simulatorConfig.result_file_name);
        await this.ssh.getFile(storingFileName, runningFileName, this.sftp);

        // Retrieve content of 'csvFile'
        const csvFileContent = await readFile(storingFileName, "utf8");
        if (csvFileContent.length === 0) {
            throw new Error("Empty result file"); // promise is rejected with an error text
        }
        return csvFileContent; // promise is fulfilled with the requested result
    }

    /**
     * @returns {Promise} a promise with an error description if rejected, or the requested result if fulfilled
     */
    readRunningDir() : Promise<any[]> {
        const sftp = this.sftp;
        const runningDir = this.runningDir;
        return new Promise(function (resolve, reject) {
            sftp.readdir(runningDir, (err: any, fileNames: any) => {
                if (err) {
                    reject(err); // promise is rejected with an error
                }
                else {
                    resolve(fileNames); // promise is fulfilled with the names of the files contained in the directory
                }
            });
        })
    }
}
