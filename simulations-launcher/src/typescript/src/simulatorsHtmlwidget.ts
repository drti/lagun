// @ts-nocheck
import { SimulatorsView } from "./view/simulatorsView";

// eslint-disable-next-line no-undef
HTMLWidgets.widget({

  name: "simulatorsView",

  type: "output",

  factory: function(el: HTMLElement, _width: number, _height: number) {

    // TODO: define shared variables for this instance

    const simulatorsView = new SimulatorsView(el.id);

    return {

      renderValue: function(config) {

        // TODO: code to render the widget, e.g.
        el.innerHTML = config.rawHtml;
        simulatorsView.init(config);

      },

      resize: function(_newWidth: number, _newHeight: number) {

        // TODO: code to re-render the widget with a new size

      }

    };
  }
});