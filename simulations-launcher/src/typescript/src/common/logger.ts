class Logger {

    name: string;

    constructor(name: string) {
        this.name = name;
    }

    public info(...messages: any) {
        // eslint-disable-next-line no-console
        console.info(`[${new Date().toLocaleString()}] <${this.name}> ${messages.join("")}`);
    }

    public warning(...messages: any) {
        // eslint-disable-next-line no-console
        console.log(`[${new Date().toLocaleString()}] <${this.name}> ${messages.join("")}`);
    }

    public error(...messages: any) {
        // eslint-disable-next-line no-console
        console.error(`[${new Date().toLocaleString()}] <${this.name}> ${messages.join("")}`);
    }

    // eslint-disable-next-line class-methods-use-this
    public trace(error: unknown) {
        console.trace(error);
    }
}

export { Logger };