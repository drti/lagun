//
// This file is subject to the terms and conditions defined in
// the file 'LICENSE', which is part of this source code.
//

/**
 * Run statuses (also used as launcher event types) 
 * @enum {string} 
 **/
const STATUS = {
    ready: "ready", 
    configuring: "configuring", 
    configured: "configured", 
    waiting: "waiting", 
    running: "running", 
    loading: "loading", 
    ended: "ended", 
    onerror: "onerror",
    disabled: "disabled"
}

/**
 * Types of simulator messages sent through sockets
 * @enum {string} 
 **/
const SIMULATOR_MESSAGE = {
    c2s: {
        getProtocolVersion: "get protocol version", 
        getRunSets: "get run sets", 
        getLauncherConfig: "get launcher config", 
        updateStoringDir: "update storing dir",
        addSimulatorConfig: "add simulator config",
        updateSimulatorConfig: "update simulator config",
        removeSimulatorConfig: "remove simulator config",
        listFiles: "list files",
        importLauncherConfig: "import launcher config",
        getOptimDescrs: "get optim descrs", 
    },
    s2c: {
        storingDirUpdate: "storing dir update",
        simulatorConfigAdd: "simulator config add",
        simulatorConfigUpdate: "simulator config update",
        simulatorConfigRemove: "simulator config remove",
        launcherConfigImport: "launcher config import"
    }
}

/**
 * Types of simulation messages sent through sockets
 * @enum {string} 
 **/
const SIMULATION_MESSAGE = {
    c2s: {
        attachSet: "attach set", 
        submitAction: "submit action", 
        getOptimList: "get optim list", 
        startOptim: "start optim", 
        stopOptim: "stop optim", 
        storeBinFile: "store bin file", 
        tellY: "tell Y",
        runAction: "run action", 
        getRuns: "get runs", 
        addRuns: "add runs", 
        searchMustacheInputs: "search mustache inputs",
        setRunSimulator: "set run simulator", 
        loginPasswordNeeded: "login password needed",
        addLoginPassword: "add login password"
    },
    s2c: {
        initRuns: "init runs", 
        runsAdd: "runs add", 
        runSimulatorSet: "run simulator set", 
        launcherEvent: "launcher event",
        askYEvent: "askY event"
    }
}

/**
 * Launcher actions (used as argument of SIMULATION_MESSAGE.c2s.runAction) 
 * @enum {string} 
 **/
const RUN_ACTION = {
    configureLaunchLoad: "ConfigureLaunchLoad", 
    configure: "Configure", 
    load: "Load",
    cancel: "Cancel",
    enDisable: "EnDisable"
}

/** 
 * Name of the file used to store input parameter values in a 'runX' directory (built during simulation configuration).
 * @type {string} 
 **/
const SIMU_INFO_JSON = "SimulationsLauncherInfo.json";

export {STATUS, SIMULATOR_MESSAGE, SIMULATION_MESSAGE, RUN_ACTION, SIMU_INFO_JSON};