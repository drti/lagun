<img src="documentation/illustrations/lagun_front.png">

# Lagun

Lagun is a R/Shiny platform providing a user-friendly interface to methods and algorithms dedicated to the exploration and the analysis of datasets.<br>
Guided workflows are provided to help non-expert users to apply safely the proposed methodologies.<br>

## Install & run

The developer documentation is available [here](https://lagun.readthedocs.io/).

### Install sources and execute manually

- Requirements: R (4.4.1) available from [CRAN](https://cran.r-project.org/), git, ([Rtools44](https://cran.r-project.org/bin/windows/Rtools/) for windows)

- Clone the current repository: `git clone https://gitlab.com/drti/lagun.git`

- Go in the main directory of Lagun Shiny app: `cd lagun/lagun`

- To execute the following R commands, make sure that R is added to the PATH variable or use R console (using [Rstudio](https://rstudio.com/) is recommended for windows).

- Install the package manager renv: `R -e "install.packages('renv')"`

- Download and install all the required packages: `R -e "options(timeout = 600); options(renv.config.connect.timeout = 600); renv::load(); renv::restore()"`

- Start Lagun app with `R -e "renv::load(); shiny::runApp('app.R', port=6023)"`

- Open your browser at returned local address: http://127.0.0.1:6023

**Warning**<br/>
Lagun initially requires that all the necessary [packages](./lagun/loadPackages.R) are installed using [renv](https://cran.r-project.org/package=renv), a package manager which freezes the versions defined in the file [renv.lock](./lagun/renv.lock). Notice the following three details:

- Before launching Lagun app, always run `R -e "renv::load()"` to load the required packages. To restore your R session after closing Lagun, stop renv with a restart of R.

- renv downloads package sources from CRAN, it can fail for network instability issues. To retry and continue installation after a failure, make sure you are located in the appropriate directory `cd lagun/lagun` and rerun `R -e "renv::load(); renv::restore()"`.

- renv recompiles packages from source. If your system fails to recompile any package, precompiled binaries are available from [CRAN](https://cran.r-project.org/web/packages/).
However, be aware that the appropriate precompiled versions may not be available anymore. Consequently, such manual install may disable some features of Lagun.

To install Lagun on a machine with no internet access, see the following [instructions](documentation/INSTALLATION.md).

### Run with docker

We provide a docker install to make the execution more smooth, on a machine on which you have docker installed:

`docker run -p 6023:6023 registry.gitlab.com/drti/lagun/lagun`

Wait for the container to start and simply open your browser at http://127.0.0.1:6023

### Hugging face demonstrator
A demonstrator is available at https://lagun-saf-ifp-lagun.hf.space. For a frequent use, you can create a hugging face account and duplicate the space https://huggingface.co/spaces/lagun-saf-ifp/lagun for your own usage.

## Details

These tools are commonly used in the numerical uncertainty community ([RT-UQ](https://uq.math.cnrs.fr/) for example) but are also widely applicable to experimental problems. The main functionalities are the following:

1. **Optimized design of experiments**<br/>
If you control the inputs/parameters of the system generating the dataset (numerical simulations, settings of the experiments, ...), you can benefit from a better spatial repartition of the experiments.
<br/>
<div align="center"><img src="documentation/illustrations/doe.png"></div><br/>

2. **Visual exploration tools**<br/>
When the complete dataset with inputs/parameters and outputs/responses is available, you can load it to perform insightful visual analyses and identify the main trends and the most influential parameters.
<br/>
<div align="center"><img src="documentation/illustrations/Visu.png"></div><br/>

3. **Going further with surrogate models**<br/>
A next common step is to use the dataset to infer a predictive relationship between the inputs/parameters and the outputs. This estimated relationship, the surrogate model, can help push forward the analysis with its ability to predict the responses for any new combination of the inputs. In particular it can be extensively used for uncertainty quantification, sensitivity analysis, deterministic optimization, optimization under uncertainty (robust and reliability based) or more intensive graphical studies.
<br/>
<div align="center"><img src="documentation/illustrations/Surrogate.png"></div><br/>

4. **Numerical simulations**<br/>
In the specific case of numerical simulations, you can use the [simulation launcher](./simulations-launcher) to directly connect Lagun to your simulation scripts, and perform automatic and sequential optimizations with the surrogate models.
<br/>
<div align="center"><img src="documentation/illustrations/simulations-launcher.png" width="795px" height="250px"></div><br/>

You can learn more through tutorials and test cases available in the [documentation folder](./documentation).


Contacts: <br>
&nbsp;&nbsp;&nbsp;&nbsp;[Email us !](mailto:saf.lagun@safrangroup.com;ifpen.lagun@ifpen.fr) <br>
&nbsp;&nbsp;&nbsp;&nbsp;[Link to Discourse forum](https://ifpen-discourse.appcollaboratif.fr/c/lagun/)

## History

The first version of Lagun was initiated at Safran Tech (under a different name), the corporate research center of [Safran](https://www.safran-group.com/).
Its goal was to give an easy access to methods and algorithms to all Safran engineers with a user-friendly interface.
A collaboration was later launched with [IFPEN](https://www.ifpenergiesnouvelles.com/) in 2019 to share algorithms and developments in a common platform now named Lagun ("Assistance" in [Basque language](https://en.wikipedia.org/wiki/Basque_language)).

**Version 0.9.10 (December 2020)**

- First release

**Version 0.9.11 (April 2021)**

- *[General]* Developer documentation
- *[General]* Package manager "renv"
- *[General]* Handling categorical outputs (classification surrogate models)
- *[Uncertainty Quantification]* Adding distribution fitting for modeling uncertainty on inputs & dependence modeling via copulas
- *[Sensitivity Analysis]* Adding Shapley effects for test cases with dependent inputs

**Version 0.10.0 (September 2022)**

- *[General]* Calibration of numerical codes with experimental data
- *[General]* Composite outputs from analytical formulas
- *[Sequential Optimization]* Improve numerical stability and robustness
- *[General]* Increasing test coverage with tests for server functions
- *[Design of Experiments]* Improve speed and functionalities of DOE visualization
- *[Preliminary Exploration]* Add rule-based models (SIRUS)
- *[Optimization]* Plugin to use an external optimizer directly interacting with a numerical simulator

**Version 1.0.0 (July 2023)**

- *[General]* Save and load projects
- *[General]* Export surrogate models to Python
- *[General]* Plugin to perform external calibration of numerical simulator with experimental data

**Version 1.0.1 (February 2025)**
- *[General]* Docker files (for Lagun and simulations Launcher)
- *[General]* Save and load projects (extension to similations launcher + performance improvements)
- *[General]* Each parallel coordinate plot is accompanied by a scatter plot matrix, and they are synchronized with each other
- *[Direct optimization]* 'Use a previous optimization' improved (restore not only defintion, but also results)
- *[Simulations Launcher]* In simulator definition, new attributes 'Vector support' and 'Vector size'
- *[Simulations Launcher]* Ability to use the query string from the Lagun URL to specify how to connect the simulations launcher
- *[Surrogate model]* Add 'surrogate model pluggin system'
- *[Tests]* Upgraded from shinytest to shinytest2. Improved pipeline with speed up


## List of contributors

- Sébastien Da Veiga (Safran Tech, now ENSAI)
- Clément Bénard (Safran Tech)
- David Chazalviel (TECH’advantage, funded by IFPEN and ANR)
- Ayoub El Bachiri (Safran Engineering Services, funded by Safran Tech)
- Delphine Sinoquet (IFP Energies Nouvelles)
- Thierry Gonon (Safran Tech)
- Yann Richet (IRSN, funded by Safran Tech) – Testing environment
- Antoine Baker (former Safran Analytics, now Ecole Normale Supérieure)  – App modularization
- Morgane Menz (IFP Energies Nouvelles) - Surrogate model pluggin system
